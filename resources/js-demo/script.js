
// Atribuição simples de variáveis
var foo = "Olá Mundo!";
var foo =               "Oi!";

// Monstrando o valor da variável foo no console do chrome
console.log(foo);

// Operações
console.log( 2 * 3 + 5 );
console.log( 2 * (3 + 5) );

// Função simples
function foo2() {
    console.log('teste');
}

// Atribuinda uma função anônima a uma variável
var foo3 = function() {
    console.log('olá');
};

// Concatenação
var ola = 'olá';
var mundo = 'mundo';
console.log(ola + ' ' + mundo); // 'olá mundo'

// Multiplicação e divisão
console.log(2 * 3);
console.log(2 / 3);

var i = 1;
var j = ++i; // pré-incremento: j é igual a 2; i é igual a 2
var k = i++; // pós-incremento: k é igual a 2; i é igual a 3

console.log("i: " + i);
console.log("j: " + j);
console.log("k: " + k);

var foca = function(){
    var nome = document.getElementById("nome");
    nome.focus();
};

var envia = function() {
    alert("enviar!");

    var test = document.getElementById("test");
    test.innerHTML = "Test";
}


var foo = 1;
var bar = '2';
console.log(foo + bar); // 12. uh oh

var foo = 1;
var bar = '2';
// converte string para numero
console.log(foo + Number(bar));

// Faz o mesmo que Number(bar)
console.log(foo + +bar);


// Operadores lógicos
var foo = 1;
var bar = 0;
var baz = 2;
console.log(foo || bar); // retorna 1, que é verdadeiro
console.log(bar || foo); // retorna 1, que é verdadeiro
console.log(foo && bar); // retorna 0, que é falso
console.log(foo && baz); // retorna 2, que é verdadeiro
console.log(baz && foo); // retorna 1, que é verdadeiro

// Operadores de comparação
var foo = 1;
var bar = 0;
var baz = '1';
var bim = 2;
foo == bar; // retorna false
foo != bar; // retorna true
foo == baz; // retorna true; cuidado!
foo === baz; // retorna false
foo !== baz; // retorna true
foo === parseInt(baz); // retorna true
foo > bim; // retorna false
bim > baz; // retorna true
foo <= baz; // retorna true

// Exemplo 2.14. Valores que são avaliados como true
/*
‘0’;
‘qualquer string’;
[]; // um array vazio
{}; // um objeto vazio 
1; // qualquer número diferente de zero
*/
//Exemplo 2.15. Valores que são avaliados como false

/*
0; 
‘’; // uma string vazia 
NaN; // Variável “not-a-number” do JavaScript 
null; 
undefined; // seja cuidadoso – undefined pode ser redefinido!
*/

var meuArray = [ 'ola', 'mundo', 'foo', 'bar' ];
console.log(meuArray[3]); // loga 'bar'

var meuArray = [ 'olá', 'mundo' ];
console.log(meuArray.length); // loga 2

var meuArray = [ 'olá', 'mundo' ];
meuArray[1] = 'mudado';

console.log(meuArray);


var meuArray = [ 'olá', 'mundo' ];
meuArray.push('novo');

console.log(meuArray);


var meuArray = [ 'h', 'e', 'l', 'l', 'o' ];
var minhaString = meuArray.join(''); // 'hello'
var meuSplit = minhaString.split(''); // [ 'h', 'e', 'l', 'l', 'o' ]

console.log(minhaString);
console.log(meuSplit);

// Objetos

var myObject = {
    myName : 'Rebecca',
    sayHello : function() {
        console.log('olá');
    }
};

myObject.sayHello(); // loga 'olá'
console.log(myObject.myName); // loga 'Rebecca'

// alternative to DOMContentLoaded
document.onreadystatechange = function () {
  if (document.readyState == "interactive") {
    var nome_var = document.getElementById('nome');
    nome_var.value = myObject.myName;
  }
}

var lista1 = [];
function cria_array() {
    var nome_var = document.getElementById('nome');
    lista1.push(nome_var.value);
    console.log(lista1);
}

// Funções

// Função comum
// function foo() { /* faz alguma coisa */ }

// Expressão de Função Nomeada
// var foo = function() { /* faz alguma coisa */ }


// Usando funções

// Função simples
var greet = function(person, greeting) {
    var text = greeting + ', ' + person;
    console.log(text);
};

greet('Rebecca', 'Olá');

// Função que retorna valor
var greet = function(person, greeting) {
    var text = greeting + ', ' + person;
    return text;
};
console.log(greet('Rebecca','Olá'));

// Função que retorna outra função

var greet = function(person, greeting) {
    var text = greeting + ', ' + person;
    return function() { console.log(text); };
};

var greeting = greet('Rebecca', 'Olá');

greeting();

// Funções anônimas autoexecutáveis
(function(){
    var foo4 = 'Hello world';
})();
console.log(foo4); // undefined!

// Funções como argumento
var myFn = function(fn) {
    var result = fn();
    console.log(result);
};

myFn(function() { return 'olá mundo'; }); // loga 'olá mundo'

// Passando função nomeada como argumento
var myFn = function(fn) {
    var result = fn();
    console.log(result);
};

var myOtherFn = function() {
    return 'olá mundo';
};

myFn(myOtherFn); // loga 'olá mundo'



