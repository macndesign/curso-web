$(document).ready(function() {

	// Soma
    $("#soma").click(function(){
        var campo_a = $("#a").val();
        var campo_b = $("#b").val();
        $("#resultado").html(Number(campo_a) + Number(campo_b));
        $(".resultado_estilo").css("color", "red");
    });

    // Inserir texto
    $("#insere_texto").click(function(){
    	$(".texto")
    	.html("Texto adicionado pelo botão insere_texto")
    	.css({"background": "red", "color": "white"});
    });
    
    $("#mostra_texto").click(function(){
    	$(".texto:first").show();
    });

    // Selecionando pelo atributo
    $("input[type=button]").css({"background": "navy", "color": "white"});

    // Selecionando elementos através da composição de seletores CSS
    $("#composicao .carros li").css("border-bottom", "solid 1px");

    // Pseudo-seletores
    $('a.external:first').html("Novo external").css("background", "yellow");
	$('tr:odd').css("background", "#aaa");
	$('form :input').css("border", "dotted 1px green"); // selecione todos os elementos input num formulário
	$(".texto:first").hide();
	$('div:visible').css("border", "dotted 3px orange");
	$('div:gt(2)'); // todos exceto as três primeiras divs
	$('div:animated'); // todas as divs com animação


    // Refinando & Filtrando Seleções
    $('div.foo').has('p').css('background', 'purple'); // o elemento div.foo que contém <p>'s
    $('div').not('.foo').css('background', 'green'); // elementos h1 que não têm a classe bar
    $('ul.menu li').filter('.current').css('background', 'yellow'); // itens de listas não-ordenadas com a classe current
    $('ul.menu li').first().css('background', 'blue'); // somente o primeiro item da lista não ordenada
    $('ul.menu li').eq(1).css('background', 'orange'); // o sexto item da lista

    // Seletores de formulário
    //$(":button").click(function(){
    //    $(this).html("Novo");
    //});

    $(":checkbox").closest("label").css("color", "red");

    $(":checked").closest("label").css("color", "green");

    $(":disabled").css({
        "border": "solid 5px #bbb",
        "background": "#eee"
    });

    $(":enabled").css({
        "border": "solid 5px green",
        "background": "yellow"
    });

    $(":file").css({
        "border": "solid 5px red",
        "background": "yellow"
    });

    $(":password").css({
        "border": "solid 2px blue",
        "background": "#efefef"
    });







});