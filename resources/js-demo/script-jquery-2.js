$(document).ready(function() {

	$('#content')
        .find('h3')
        .eq(2)
        .html('o novo texto do terceiro h3!');

    console.log($('#content h3:first').html());

    var $h1 = $('h1');
    $h1.addClass('big');
    $h1.removeClass('big');
    $h1.toggleClass('big');

    $("#muda-bg").click(function(){
        $("body").addClass("outra-cor");
    });

    if ($h1.hasClass('big')) {
        console.log("Tem a class big!");
    }

    $("#mais_largo").click(function(){
        $("#redimensiona").width("+=50");
    });

    $("#mais_estreito").click(function(){
        $("#redimensiona").width("-=50");
    });

    $("#mais_alto").click(function(){
        $("#redimensiona").animate({height: "+=50"}, 'slow');
    });

    $("#mais_baixo").click(function(){
        $("#redimensiona").animate({height: "-=50"}, 'fast');
    });

    // manipulando-atributos
    var val_prop = $("#manipulando-atributos")
        .find("input").prop("disabled");

    var val_attr = $("#manipulando-atributos")
        .find("input").attr("disabled");

    $("#val_prop").text(val_prop);
    $("#val_attr").text(val_attr);

    // Travessia
    $('h1').next('p').css('background', 'yellow');
    $('#travessia p:visible').parent().css('border', 'solid 3px red');
    $('input[name=first_name]').closest('form').css('border', 'solid green');
    $('#myList').children().css('background', '#0f0');
    $('li.selected').siblings().css('background', '#ff0000');


});