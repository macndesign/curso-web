Estrutura básica
================

.. code-block:: html

    <!DOCTYPE html>
    <html>
    <head>
        <title>Blog do Mário</title>
    </head>
    <body>

    </body>
    </html>


Metas
=====

.. code-block:: html

    <meta charset="UTF-8">
    <meta name="author" content="Mário Chaves">
    <meta name="description" content="Blog complexo em html5 do Mário">
    <meta name="keywords" content="html5, blog, mario">


Título principal
================

.. code-block:: html

    <header>
        <h1><a href="#">Blog do Mário</a> <small>Slogan do blog</small></h1>
    </header>


Menu princial
=============

.. code-block:: html

    <nav class="menu">
        <a href="#">Home</a>
        <a href="#">Sobre</a>
        <a href="#">Artigos</a>
        <a href="#">Tags</a>
        <a href="#">Contato</a>
    </nav>


Tag do conteúdo principal
=========================

.. code-block:: html

    <main></main>


Seção de postagens com artigo básico
====================================

.. code-block:: html

    <section class="posts">
        <article>
            <hgroup>
                <h2><a href="#">Primeiro artigo do blog</a></h2>

                <h3>Publicado por <a href="#">Mário Chaves</a> em
                <time datetime="2013-02-15T20:00Z">15/02/2013 as 20h</time></h3>

            </hgroup>

            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mollis urna vitae nulla porta eu varius purus consectetur. Suspendisse ut sapien elit, ut vehicula enim. Sed et magna eu felis elementum mollis. Suspendisse fringilla nisl ac augue dignissim pulvinar. Sed dapibus, purus quis convallis congue, erat orci aliquet justo, a tempus sapien mauris sit amet ante. Etiam molestie accumsan erat eu sodales. Aliquam molestie blandit nibh eu adipiscing. Nulla facilisi.</p>

            <nav class="tags">
                <strong>Tags: </strong>
                <a href="#">tag-1</a>
                <a href="#">tag-2</a>
                <a href="#">tag-3</a>
                <a href="#">tag-4</a>
            </nav>
        </article>
    </section>


Seção de postagens com artigo complexo
======================================

.. code-block:: html

    <section class="posts">
        <article>
            <hgroup>
                <h2><a href="#">Primeiro artigo do blog</a></h2>

                <h3>Publicado por <a href="#">Mário Chaves</a> em
                <time pubdate datetime="2013-02-15T20:00Z">15/02/2013 as 20h</time></h3>

            </hgroup>

            <p><img src="surf.jpeg" alt="Foto do campeão cearense de surf" title="Foto do campeão cearense de surf"></p>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce mollis urna vitae nulla porta eu varius purus consectetur. Suspendisse ut sapien elit, ut vehicula enim. Sed et magna eu felis elementum mollis. Suspendisse fringilla nisl ac augue dignissim pulvinar. Sed dapibus, purus quis convallis congue, erat orci aliquet justo, a tempus sapien mauris sit amet ante. Etiam molestie accumsan erat eu sodales. Aliquam molestie blandit nibh eu adipiscing. Nulla facilisi.</p>

            <section class="comments">

                <h3>Comentários:</h3>

                <article>
                    <h3><a href="#">Andréa Negreiros</a> em

                    <time pubdate datetime="2013-02-15T20:00Z">15/02/2013 as 20h</time></h3>
                    <p>Adorei seu artigo, está muito bem escrito.</p>
                </article>

                <article>
                    <h3><a href="#">Milson Feitosa</a> em

                    <time pubdate datetime="2013-02-15T20:00Z">15/02/2013 as 20h</time></h3>
                    <p>Adorei seu artigo, está muito bem escrito.</p>
                </article>

                <fieldset>
                    <legend>Deixe seu comentário aqui</legend>

                    <form action="." method="post">
                        <label>Nome: <input type="text" maxlength="35" required></label>

                        <label>EMail: <input type="email" maxlength="100" required></label>

                        <label>Comentário: <textarea rows="5" maxlength="140" required></textarea></label>

                        <input type="submit" value="Enviar">
                    </form>
                </fieldset>

            </section>

            <nav class="tags">
                <strong>Tags: </strong>
                <a href="#">tag-1</a>
                <a href="#">tag-2</a>
                <a href="#">tag-3</a>
                <a href="#">tag-4</a>
            </nav>

        </article>
    </section>


Inserindo sidebar
=================

.. code-block:: html

    <aside class="sidebar"></aside>


Dentro da sidebar as seções de tags e blog roll
===============================================

.. code-block:: html

        <section>
            <nav>
                <ul class="links-sidebar">
                    <li>Tags</li>
                    <li><a href="#">Tag 1</a></li>
                    <li><a href="#">Tag 2</a></li>
                    <li><a href="#">Tag 3</a></li>
                    <li><a href="#">Tag 4</a></li>
                    <li><a href="#">Tag 5</a></li>
                </ul>
            </nav>
        </section>

        <section>
            <nav>
                <ul class="links-sidebar">
                    <li>Blog roll</li>
                    <li><a href="#">Blog 1</a></li>
                    <li><a href="#">Blog 2</a></li>
                    <li><a href="#">Blog 3</a></li>
                    <li><a href="#">Blog 4</a></li>
                    <li><a href="#">Blog 5</a></li>
                </ul>
            </nav>
        </section>


E logo abaixo da tag main, a tag footer
=======================================

.. code-block:: html

    <footer>&copy; Copyright 2013</footer>


Criação do arquivo style.css para o estilo do blog
==================================================

.. code-block:: html

    <link type="text/css" rel="stylesheet" href="style.css">


Não esquecer de incluir o import do html5shiv
=============================================

.. code-block:: html

    <script type="text/javascript" src="html5shiv.js"></script>


Começando a escrever no arquivo style.css, zerando os espaçamentos das tags
===========================================================================

.. code-block:: css

    * { margin: 0; padding: 0 }

Estilo do corpo do documento
============================

.. code-block:: css

    body { width: 80%; margin: 20px auto }


Espaçamento interno padrão para diversas tags ao mesmo tempo
============================================================

.. code-block:: css

    header, nav, article, aside, footer, label { padding: 5px }


Aplicando estilo no cabeçalho
=============================

.. code-block:: css

    header {
        background: #ccc;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-bottom: 8px;
    }


Estilo nas tags de navegação do menu principal e das tags
=========================================================

.. code-block:: css

    body > nav {
        background: #777;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-bottom: 8px;
    }

.. code-block:: css

    nav.menu > a { color: #fff; text-decoration: none }
    nav.menu > a:after { content: " | " }
    nav.menu > a:last-child:after { content: "" }

    article > nav {
        background: #bbb;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        margin-top: 5px
    }

    nav.tags > a { text-decoration: none }
    nav.tags > a:after { content: ", " }
    nav.tags > a:last-child:after { content: "" }


Espaçamento externo dos coemntários apenas para separação visual
================================================================

.. code-block:: css

    section.comments {
        margin-top: 10px;
    }


Estilização em componentes do formulário e tags de organização
==============================================================

.. code-block:: css

    fieldset {
        width: 350px;
        padding: 10px;
        border: solid 1px #777;
        background: #f2f2f2;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
        text-align: right;
    }
    fieldset > legend {
        padding: 2px 5px;
        border: solid 1px #777;
        margin-left: 10px;
        background: #f2f2f2;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }

    fieldset input[type=text], fieldset input[type=email], fieldset textarea { width: 250px }

    label { display: block }


Aplicando box flexível na tag principal <main>
==============================================

.. code-block:: css

    main {
        display: -webkit-flex;
        display: -moz-flex;
        display: -ms-flex;
        display: -o-flex;
        display: flex;
    }


Estilizando as regiões de postagens e sidebar
=============================================

.. code-block:: css

    section.posts {
        background: #ddd;
        -webkit-border-top-left-radius: 4px;
        -moz-border-radius-topleft: 4px;
        border-top-left-radius: 4px;
        /*-webkit-order: 2;
        -moz-order: 2;
        -ms-order: 2;
        -o-order: 2;
        order: 2;*/
    }
    aside.sidebar {
        background: #eee;
        width: 200px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topright: 4px;
        border-top-right-radius: 4px;
        /*-webkit-order: 1;
        -moz-order: 1;
        -ms-order: 1;
        -o-order: 1;
        order: 1;*/
    }


Criando um estilo para os menus (tags e blog roll) baseados em listas
=====================================================================

.. code-block:: css

    ul.links-sidebar {
        list-style: none;
    }
    ul.links-sidebar li a {
        display: block;
        background: #ddd;
        text-decoration: none;
        color: #777;
        padding: 8px;
        border-bottom: solid 1px #ccc;
    }
    ul.links-sidebar li a:hover {
        background: #fff;
    }
    ul.links-sidebar li:first-child {
        background: #ccc;
        padding: 8px;
        font-weight: bold;
        color: #555;
        cursor: default;
        -webkit-border-top-left-radius: 4px;
        -moz-border-radius-topleft: 4px;
        -webkit-border-top-right-radius: 4px;
        -moz-border-radius-topright: 4px;
    }
    ul.links-sidebar li:last-child a {
        -webkit-border-bottom-left-radius: 4px;
        -moz-border-radius-bottomleft: 4px;
        -webkit-border-bottom-right-radius: 4px;
        -moz-border-radius-bottomright: 4px;
        border-bottom: none;
    }


Finalizando o blog com o estilo da tag footer
=============================================

.. code-block:: css

    footer {
        background: #999;
        color: white;
        -webkit-border-bottom-left-radius: 4px;
        -moz-border-radius-bottomleft: 4px;
        -webkit-border-bottom-right-radius: 4px;
        -moz-border-radius-bottomright: 4px;
    }


Tabela simples
==============

.. code-block:: html

    <table>
        <thead>
        <tr>
            <th>Título 1</th>
            <th>Título 2</th>
            <th>Título 3</th>
            <th>Título 4</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>Cont. 1</td>
            <td>Cont. 2</td>
            <td>Cont. 3</td>
            <td>Cont. 4</td>
        </tr>
        <tr>
            <td>Cont. 1</td>
            <td>Cont. 2</td>
            <td>Cont. 3</td>
            <td>Cont. 4</td>
        </tr>
        </tbody>
        <tfoot>
        <tr>
            <td colspan="4">Rodapé da tabela</td>
        </tr>
        </tfoot>
    </table>


Tabela complexa
===============

.. code-block:: html

    <table>
        <caption>Comparativo de automóveis</caption>
        <thead>
        <tr>
            <th rowspan="2">#</th>
            <th colspan="3">Volkswagen</th>
            <th colspan="3">Fiat</th>
            <th colspan="3">Ford</th>
        </tr>
        <tr>
            <th>Gol</th>
            <th>Fox</th>
            <th>Saveiro</th>
            <th>Uno</th>
            <th>Palio</th>
            <th>Siena</th>
            <th>Ka</th>
            <th>Fiesta</th>
            <th>EcoSport</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <th rowspan="2">Básico</th>
            <td>Motor 1.0<br>R$ 24 Mil</td>
            <td>Motor 1.0<br>R$ 26 Mil</td>
            <td>Motor 1.0<br>R$ 28 Mil</td>
            <td>Motor 1.0<br>R$ 24 Mil</td>
            <td>Motor 1.0<br>R$ 26 Mil</td>
            <td>Motor 1.0<br>R$ 28 Mil</td>
            <td>Motor 1.0<br>R$ 24 Mil</td>
            <td>Motor 1.0<br>R$ 26 Mil</td>
            <td>Motor 1.0<br>R$ 28 Mil</td>
        </tr>
        <tr>
            <td>Motor 1.6<br>R$ 26 Mil</td>
            <td>Motor 1.6<br>R$ 28 Mil</td>
            <td>Motor 1.6<br>R$ 32 Mil</td>
            <td>Motor 1.6<br>R$ 26 Mil</td>
            <td>Motor 1.6<br>R$ 28 Mil</td>
            <td>Motor 1.6<br>R$ 32 Mil</td>
            <td>Motor 1.6<br>R$ 26 Mil</td>
            <td>Motor 1.6<br>R$ 28 Mil</td>
            <td>Motor 1.6<br>R$ 32 Mil</td>
        </tr>
        <tr>
            <th rowspan="2">Completo</th>
            <td>Motor 1.0<br>R$ 28 Mil</td>
            <td>Motor 1.0<br>R$ 32 Mil</td>
            <td>Motor 1.0<br>R$ 38 Mil</td>
            <td>Motor 1.0<br>R$ 28 Mil</td>
            <td>Motor 1.0<br>R$ 32 Mil</td>
            <td>Motor 1.0<br>R$ 38 Mil</td>
            <td>Motor 1.0<br>R$ 28 Mil</td>
            <td>Motor 1.0<br>R$ 32 Mil</td>
            <td>Motor 1.0<br>R$ 38 Mil</td>
        </tr>
        <tr>
            <td>Motor 1.6<br>R$ 32 Mil</td>
            <td>Motor 1.6<br>R$ 38 Mil</td>
            <td>Motor 1.6<br>R$ 42 Mil</td>
            <td>Motor 1.6<br>R$ 32 Mil</td>
            <td>Motor 1.6<br>R$ 38 Mil</td>
            <td>Motor 1.6<br>R$ 42 Mil</td>
            <td>Motor 1.6<br>R$ 32 Mil</td>
            <td>Motor 1.6<br>R$ 38 Mil</td>
            <td>Motor 1.6<br>R$ 42 Mil</td>
        </tr>
        </tbody>
    </table>


Estilo para a tabela
====================

.. code-block:: css

    table {
        width: 100%;
        background: #777;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    caption {
        background: #555;
        color: #fff; padding: 5px;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    th { background: #ddd }
    th, td {
        border: solid 1px #777;
        padding: 3px 6px;
        margin: 0;
        white-space: nowrap;
        -webkit-border-radius: 4px;
        -moz-border-radius: 4px;
        border-radius: 4px;
    }
    td { font-size: 12px; background: #fff }
