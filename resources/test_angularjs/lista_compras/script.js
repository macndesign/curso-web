function compras_controller($scope) {
    $scope.compras = [];
    var $produto = document.getElementById("produto");

    $scope.adicionarProduto = function() {
        $scope.compras.push({
            produto: $scope.produto,
            quantidade: $scope.quantidade,
            comprado: false
        });
        $scope.produto = "";
        $scope.quantidade = "";
        $produto.focus();
    };

    $scope.apagarSelecionados = function() {
        var comprasAtuais = $scope.compras;
        $scope.compras = [];
        angular.forEach(comprasAtuais, function(compra){
            if (compra.comprado == false) {
                $scope.compras.push(compra);
            }
        });
        $produto.focus();
    };

    $scope.faltantes = function() {
        var contador = 0;
        angular.forEach($scope.compras, function(compra){
            if (compra.comprado == false) {
                contador += 1;
            }
        });
        return contador;
    };
}