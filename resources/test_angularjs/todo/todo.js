function TodoCtrl($scope) {
    $scope.todos = [
        // Preenchendo a listagem com 2 itens
        {text:'learn angular', done:true},
        {text:'build an angular app', done:false}
    ];

    $scope.addTodo = function() {
        /* Insere na listagem de todos uma linha
        com o texto do ng-model todoText */
        $scope.todos.push({text:$scope.todoText, done:false});
        $scope.todoText = '';
    };

    $scope.remaining = function() {
        /* Calcula todos que não foram marcados, com o auxilio de um
        contador, percorrendo cada objeto da listagem de todos */
        var count = 0;
        angular.forEach($scope.todos, function(todo) {
            // O count é incrementado com +1 se done for false
            count += todo.done ? 0 : 1;
        });
        return count;
    };

    $scope.archive = function() {
        // Seleciona todos os "todos"
        var oldTodos = $scope.todos;
        // Limpa toda a listagem
        $scope.todos = [];
        /* Percorre toda a listagem em memória e insere no objeto "todos"
        os itens que done for igual a false */
        angular.forEach(oldTodos, function(todo) {
            if (!todo.done) $scope.todos.push(todo);
        });
    };
}