************
CSS / 3 (4h)
************

O que é CSS
===========

O CSS formata a informação entregue pelo HTML. Essa informação pode ser qualquer coisa: imagem, texto, vídeo, áudio ou
qualquer outro elemento criado. Grave isso: CSS formata a informação. Essa formatação na maioria das vezes é visual, mas
não necessariamente. No CSS Aural, nós manipulamos o áudio entregue ao visitante pelo sistema de leitura de tela. Nós
controlamos volume, profundidade, tipo da voz ou em qual das caixas de som a voz sairá. De certa forma você está
formatando a informação que está em formato de áudio e que o visitante está consumindo ao entrar no site utilizando um
dispositivo ou um sistema de leitura de tela. O CSS prepara essa informação para que ela seja consumida da melhor
maneira possível.

O HTML5 trouxe poucas novidades para os desenvolvedores client-side. Basicamente foram criadas novas tags, o significado
de algumas foi modificado e outras tags foram descontinuadas. As novidades interessantes mesmo ficaram para o pessoal
que conhece Javascript. As APIs que o HTML5 disponibilizou são sem dúvida uma das features mais aguardadas por todos
estes desenvolvedores. Diferentemente do CSS3 que trouxe mudanças drásticas para a manipulação visual dos elementos do
HTML.

Com o CSS que nós conhecemos podemos formatar algumas características básicas: cores, background, características de
font, margins, paddings, posição e controlamos de uma maneira muito artesanal e simples a estrutura do site com a
propriedade float.

Você deve pensar: “mas com todas as características nós conseguimos fazer sites fantásticos, com design atraente e com a
manutenção relativamente simples”. E eu concordo com você, mas outras características que nós não temos controles e
dependemos de:

1. Algum programa visual como o Photoshop para criarmos detalhes de layout;
2. Javascript para tratarmos comportamentos ou manipularmos elementos específicos na estrutura do HTML;
3. Estrutura e controle dos elementos para melhorarmos acessibilidade e diversos aspectos do SEO;

Com as atualizações do CSS3 e com os browsers atualizando o suporte do CSS2.1, nós entramos em patamar onde sem dúvida o
CSS é a arma mais poderosa para o designer web. Segue uma pequena lista dos principais pontos que podemos controlar
nesse novo patamar:

1. selecionar primeiro e último elemento;
2. selecionar elementos pares ou ímpares;
3. selecionarmos elementos específicos de um determinado grupo de elementos;
4. gradiente em textos e elementos;
5. bordas arredondadas;
6. sombras em texto e elementos;
7. manipulação de opacidade;
8. controle de rotação;
9. controle de perspectiva;
10. animação;
11. estruturação independente da posição no código HTML;

É agora que começa diversão ao formatar sites com CSS.

A sintaxe do CSS é simples:

.. code-block:: css

    seletor {
        propriedade: valor;
    }



A propriedade é a característica que você deseja modificar no elemento. O valor é o valor referente a esta
característica. Se você quer modificar a cor do texto, o valor é um Hexadecimal, RGBA ou até mesmo o nome da cor por
extenso. Até aqui, nada diferente. Muitas vezes você não precisa aprender do que se trata a propriedade, basta saber que
existe e se quiser decorar, decore. Propriedades são criadas todos os dias e não é um ato de heroísmo você saber todas
as propriedades do CSS e seus respectivos valores.

Os seletores são a alma do CSS e você precisa dominá-los. É com os seletores que você irá escolher um determinado
elemento dentro todos os outros elementos do site para formatá-lo. Boa parte da inteligência do CSS está em saber
utilizar os seletores de uma maneira eficaz, escalável e inteligente.



O que é um seletor?
===================

O seletor representa uma estrutura. Essa estrutura é usada como uma condição para determinar quais elementos de um grupo
de elementos serão formatados.

Seletores encadeados e seletores agrupados são a base do CSS. Você os aprende por osmose durante o dia a dia. Para você
lembrar o que são seletores encadeados e agrupados segue um exemplo abaixo:

Exemplo de seletor encadeado:

.. code-block:: css

    div p strong a {
        color: red;
    }



Este seletor formata o link (a), que está dentro de um strong, que está dentro de P e que por sua vez está dentro de um
DIV.

Exemplo de seletor agrupado:

.. code-block:: css

    strong, em, span {
        color: red;
    }



Você agrupa elementos separados por vírgula para que herdem a mesma formatação.

Estes seletores são para cobrir suas necessidades básicas de formatação de elementos. Eles fazem o simples. O que vai
fazer você trabalhar menos, escrever menos código CSS e também menos código Javascript são os seletores complexos.

Os seletores complexos selecionam elementos que talvez você precisaria fazer algum script em Javascript para poder
marcá-lo com uma CLASS ou um ID para então você formatá-lo. Com os seletores complexos você consegue formatar elementos
que antes eram inalcançáveis.



Exemplo de funcionamento de um seletor complexo
-----------------------------------------------

Imagine que você tenha um título (h1) seguido de um parágrafo (p). Você precisa selecionar todos os parágrafos que vem
depois de um título h1. Com os seletores complexos você fará assim:

.. code-block:: css

    h1 + p {
        color: red;
    }



Esses seletores é um dos mais simples e mais úteis que eu já utilizei em projetos por aí. Não precisei adicionar uma
classe com JQuery, não precisei de manipular o CMS para marcar esse parágrafo, não precisei de nada. Apenas apliquei o
seletor.

Abaixo, veja uma lista de seletores complexos e quais as suas funções. Não colocarei todas as possibilidades aqui porque
podem haver modificações, novos formatos ou outros formatos que podem ser descontinuados. Para ter uma lista sempre
atualizada, siga o link no final da tabela.

===========================  ====================================================================================  =====
Padrão                       Significado                                                                           CSS
===========================  ====================================================================================  =====
elemento[atr]                Elemento com um atributo específico.                                                  2
elemento[atr=”x”]            Elemento que tenha um atributo com um valor específico igual a “x”                    2
elemento[atr~=”x”]           Elemento com um atributo cujo valor é uma lista separada por espa- ços, sendo que um  2
                             dos valores é “x”.
elemento[atr^=”x”]           Elemento com um atributo cujo valor comece exatamente com string “x”.                 3
elemento[atr$=”x”]           Elemento com um atributo cujo valor termina exatamente com string “x”.                3
elemento[atr*=”x”]           Elemento com um atributo cujo valor contenha a string “x”.                            3
elemento[atr|=”en”]          Um elemento que tem o atributo específico com o valor que é separado por hífen        2
                             começando com EN (da esquerda para direita).
elemento:root                Elemento root do documento. Normalmente o HTML.                                       3
elemento:nth- child(n)       Selecione um objeto N de um determinado elemento.                                     3
elemento:nth-last- child(n)  Seleciona um objeto N começando pelo último objeto do elemento.                       3
elemento:empty               Seleciona um elemento vazio, sem filhos. Incluindo elementos de texto.                3
elemento:enabled             Seleciona um elemento de interface que esteja habilitado, como selects, checkbox,     3
                             radio button etc.
elemento:disabled            Seleciona um elemento de interface que esteja habilitado, como selects, checkbox,     3
                             radio button etc.
elemento:checked             Seleciona elementos que estão checados, como radio buttons e checkboxes.              3
E>F                          Seleciona os elementos E que são filhos diretos de F.                                 2
E+F                          Seleciona um elemento F que precede imediatamente o elemento E.                       2
===========================  ====================================================================================  =====



Gradiente
=========

Uma das features mais interessantes é a criação de gradientes apenas utilizando CSS. Todos os browsers mais novos como
Safari, Opera, Firefox e Chrome já aceitam essa feature e você pode utilizá-la hoje. Os Internet Explorer atuais (8 e 9)
não reconhecem ainda, contudo você poderá utilizar imagens para estes browsers que não aceitam essa feature. Você pode
perguntar: “Mas já que terei o trabalho de produzir a imagem do gradiente, porque não utilizar imagens para todos os
browsers?“ Lembre-se que se utilizar uma imagem, o browser fará uma requisição no servidor buscando essa imagem, sem
imagem, teremos uma requisição a menos, logo o site fica um pouquinho mais rápido. Multiplique isso para todas as
imagens de gradiente que você fizer e tudo realmente fará mais sentido.

Deixe para que os browsers não adeptos baixem imagens e façam mais requisições.

Veja abaixo um exemplo de código, juntamente com o fallback de imagem:

.. code-block:: css

    div {
        width:200px; height:200px;
        background-color: #FFF;
        /* imagem caso o browser não aceite a feature */
        background-image: url(images/gradiente.png);
        /* Firefox 3.6+ */
        background-image: -moz-linear-gradient(green, red);
        /* Safari 5.1+, Chrome 10+ */
        background-image: -webkit-linear-gradient(green, red);
        /* Opera 11.10+ */
        background-image: -o-linear-gradient(green, red);
    }

Veja o resultado:

.. figure:: ../_images/gradient-1.png

.. warning::

    Até que os browsers implementem de vez essa feature, iremos utilizar seus prefixos.



“Stops” ou definindo o tamanho do seu gradiente
-----------------------------------------------

O padrão é que o gradiente ocupe 100% do elemento como vimos no exemplo anterior, mas muitas vezes queremos fazer apenas
um detalhe.

Nesse caso nós temos que definir um STOP, para que o browser saiba onde uma cor deve terminar para começar a outra.
Perceba o 20% ao lado da cor vermelha. O browser calcula quanto é 20% da altura (ou largura dependendo do caso) do
elemento, e começa o gradiente da cor exatamente ali. O código de exemplo segue abaixo:

.. code-block:: css

    div {
        /* Firefox 3.6+ */
        background-image: -moz-linear-gradient(green, red 20%);
        /* Safari 5.1+, Chrome 10+ */
        background-image: -webkit-linear-gradient(green, red 20%);
        /* Opera 11.10+ */
        background-image: -o-linear-gradient(green, red 20%);
    }



Como ficou:

.. figure:: ../_images/gradient-2.png



Se colocarmos um valor para o verde, nós iremos conseguir efeitos que antes só conseguiríamos no Illustrator ou no
Photoshop. Segue o código e o resultado logo após:

.. code-block:: css

    div {
        /* Firefox 3.6+ */
        background-image: -moz-linear-gradient(green 10%, red 20%);
        /* Safari 5.1+, Chrome 10+ */
        background-image: -webkit-linear-gradient(green 10%, red 20%);
        /* Opera 11.10+ */
        background-image: -o-linear-gradient(green 10%, red 20%);
    }

.. figure:: ../_images/gradient-3.png

Perceba que o tamanho da transição vai ficando menor à medida que vamos aumentando as por- centagens. Muito, mas muito
útil.




Colunas
=======

Com o controle de colunas no CSS, podemos definir colunas de texto de forma automática. Até hoje não havia maneira de
fazer isso de maneira inteligente com CSS e o grupo de propriedades columns pode fazer isso de maneira livre de
gambiarras.



column-count
------------

A propriedade column-count define a quantidade de colunas terá o bloco de textos.

.. code-block:: css

    div {
        /* Define a quantidade de colunas, a largura é definida uniformemente. */
        -moz-column-count: 2; -webkit-column-count: 2;
    }



column-width
------------

Com a propriedade column-width definimos a largura destas colunas.

.. code-block:: css

    div {
        /* Define qual a largura mínima para as colunas. Se as colunas forem espremidas, fazendo com que a largura
        delas fique menor que este valor, elas se transformam em 1 coluna automaticamente */
        -moz-column-width: 400px;
        -webkit-column-width: 400px;
    }



Fiz alguns testes aqui e há uma diferença entre o Firefox 3.5 e o Safari 4 (Public Beta).

O column-width define a largura mínima das colunas. Na documentação do W3C é a seguinte: imagine que você tenha uma área
disponível para as colunas de 100px. Ou seja, você tem um div com 100px de largura (width). E você define que as
larguras destas colunas (column-width) sejam de 45px. Logo, haverá 10px de sobra, e as colunas irão automaticamente ter
50px de largura, preenchendo este espaço disponível. É esse o comportamento que o Firefox adota.

Já no Safari, acontece o seguinte: se você define um column-width, as colunas obedecem a esse valor e não preenchem o
espaço disponível, como acontece na explicação do W3C e como acontece também no Firefox. Dessa forma faz mais sentido
para mim.

Como a propriedade não está 100% aprovada ainda, há tempo para que isso seja modificado novamente. Talvez a mudança da
nomenclatura da classe para column-min-width ou algo parecida venha a calhar, assim, ficamos com os dois resultados
citados, que são bem úteis para nós de qualquer maneira.



column-gap
----------

A propriedade column-gap cria um espaço entre as colunas, um gap.

.. code-block:: css

    div {
        /* Define o espaço entre as colunas. */
        -moz-column-gap: 50px;
        -webkit-column-gap: 50px;
    }



Utilizamos aqui os prefixos -moz- e -webkit-, estas propriedades não funcionam oficialmente em nenhum browser. Mas já
podem ser usados em browsers como Firefox e Safari.



Bordas
======

Definir imagem para as bordas é uma daquelas propriedades da CSS que você se pergunta como vivíamos antes de conhecê-la.
É muito mais fácil entender testando na prática, por isso sugiro que se você estiver perto de um computador, faça testes
enquanto lê este texto. A explicação pode não ser suficiente em algumas partes, mas a prática irá ajudá-lo a entender.

Esta propriedade ainda está em fase de testes pelos browsers, por isso utilizaremos os prefixos para ver os resultados.
Utilizarei apenas o prefixo do Safari, mas o Firefox já entende essa propriedade muito bem.

Segue um exemplo da sintaxe abaixo:

.. code-block:: css

    a {
        display: block;
        width: 100px;
        -webkit-border-image: url(border.gif) 10 10 10 10 stretch;
    }



Acima definimos uma imagem com o nome de border.gif, logo depois definimos o width de cada uma das bordas do elemento.
A sintaxe é igual a outras propriedades com 4 valores: top, right, bottom, left. E logo depois colocamos qual o tipo
de tratamento que a imagem vai receber.



Dividindo a imagem
------------------

Para posicionar a imagem devidamente em seu objeto o browser divide a imagem em 9 seções:

.. figure:: ../_images/dividindo-imagem.png

Quando a imagem é colocada no elemento, o browser posiciona os cantos da imagem juntamente com os cantos correspondentes
do elemento. Ou seja, o bloco 1 da imagem é colocado no canto superior esquerdo, o 3 no canto superior direito e assim
por diante. Se você fizer o teste, a imagem aparecerá no elemento como se estivesse desproporcional. Isso é normal
porque a imagem deve seguir as proporções do elemento e não as suas próprias.



Comportamento da imagem
-----------------------

O comportamento da imagem é a parte mais importante porque define como o centro da imagem (no caso do nosso exemplo a
seção de número 5), irá ser tratada. Há vários valores, mas que é mais simples de se entender é a stretch, que estica e
escala a imagem para o tamanho do elemento aplicado. Há outros valores como round e repeat. Mas hoje alguns browsers
não tem tanto suporte e acabam ou ignorando esses valores ou como no caso do Safari e o Chrome, interpretam o round
como o repeat. Vamos nos concentrar com o stretch e você entenderá como funciona a propriedade.



Aplicação
---------

Vamos utilizar a imagem abaixo para aplicar a propriedade. Iremos fazer um botão ao estilo iPhone. A coisa é simples e
sugiro que você faça testes individualmente brincando com os valores das bordas e com diversas outras imagens para ver
como funciona o recurso.

.. figure:: ../_images/botao-iphone.png

Irei aplicar o estilo em um link. O código que irei aplicar segue abaixo:

.. code-block:: css

    a {
        display: block;
        width: 100px;
        text-align: center;
        font: bold 13px verdana, arial, tahoma, sans-serif;
        text-decoration: none;
        color: black;
    }



Para inserir a imagem, colocamos as duas linhas abaixo:

.. code-block:: css

    border-width: 10px;
    -webkit-border-image: url(button.png) 10 stretch;



Defini com a primeira linha que a borda teria 10px de largura com a propriedade border-width. Na segunda linha define
que a imagem utilizada seria a button.png, que as áreas da imagem teriam que se estender por 10px, e o valor stretch
define que a imagem cobrirá o elemento inteiro, o resultado segue abaixo:

.. figure:: ../_images/botao-iphone-2.png

Para você ver como tudo ali é meio estranho. Se eu diminuir o valor de 10 do border-image, que é o valor que define
quanto a imagem deve se estender nas bordas, veja como fica:

.. figure:: ../_images/botao-iphone-3.png

Temos o border-width definindo a largura da borda, mas não temos nenhum valor dizendo quanto dessa largura a imagem
deve tomar.

Os efeitos são bem estranhos quando esses valores estão mal formatados. Por isso, teste na prática essa propriedade para
que não haja problemas a implementar em seus projetos. O pulo da gato, para mim, é a propriedade border-width.



Cores
=====

Normalmente em web trabalhamos com cores na forma de hexadecimal. É a forma mais comum e mais utilizada desde os
primórdios do desenvolvimento web. Mesmo assim, há outros formatos menos comuns que funcionam sem problemas, um destes
formatos é o RGB. O RGB são 3 conjuntos de números que começam no 0 e vão até 255 (0% até 100%), onde o primeiro bloco
define a quantidade de vermelho (Red), o segundo bloco a quantidade de verde (Green) e o último bloco a quantidade de
azul (Blue). A combinação destes números formam todas as cores que você pode imaginar.

No HTML o RGB pode ser usado em qualquer propriedade que tenha a necessidade de cor, como:
color, background, border etc. Exemplo:

.. code-block:: css

    p {
        background: rgb(255,255,0);
        padding: 10px;
        font: 13px verdana;
    }



Este código RGB define que o background o elemento P será amarelo.

RGBA e a diferença da propriedade OPACITY
-----------------------------------------

Até então nós só podíamos escrever cores sólidas, sem nem ao menos escolhermos a opacidade dessa cor. O CSS3 nos trouxe
a possibilidade de modificar a opacidade dos elementos via propriedade opacity. Lembrando que quando modificamos a
opacidade do elemento, tudo o que está contido nele também fica opaco e não apenas o background ou a cor dele. Veja o
exemplo abaixo e compare as imagens.

A primeira é a imagem normal, sem a aplicação de opacidade:

.. figure:: ../_images/opacidade-1.png

Agora com o bloco branco, marcado com um P, com opacidade definida. Perceba que o background e também a cor do texto
ficaram transparentes.

Isso é útil mas dificulta muito quando queremos que apenas a cor de fundo de um determinado elemento tenha a opacidade
modificada. É aí que entra o RGBA. O RGBA funciona da mesma forma que o RGB, ou seja, definindo uma cor para a
propriedade. No caso do RGBA, além dos 3 canais RGB (Red, Green e Blue) há um quarto canal, A (Alpha) que controla a
opacidade da cor. Nesse caso, podemos controlar a opacidade da cor do background sem afetar a opacidade dos outros
elementos:

.. figure:: ../_images/opacidade-2.png

Veja um exemplo de código aplicado abaixo:

.. code-block:: css

    p {
        background: rgba(255,255,0, 0.5);
        padding: 10px;
        font: 13px verdana;
    }


O último valor é referente ao canal Alpha, onde 1 é totalmente visível e 0 é totalmente invisível. No exemplo acima
está com uma opacidade de 50%.



currentColor
------------

O valor currentColor é muito simples de se entender e pode ser muito útil para diminuirmos o retrabalho em alguns
momentos da produção. Imagine que o currentColor é uma variável cujo seu valor é definido pelo valor da propriedade
color do seletor. Veja o código abaixo:

.. code-block:: css

    p {
        background: red;
        padding: 10px;
        font: 13px verdana;
        color: green;
        border: 1px solid green;
    }



Note que o valor da propriedade color é igual ao valor da cor da propriedade border.

Há uma redundância de código, que nesse caso é irrelevante, mas quando falamos de dezenas de arquivos de CSS modulados,
com centenas de linhas cada, essa redundância pode incomodar a produtividade. A função do currentColor é simples: ele
copia para outras propriedades do mesmo seletor o valor da propriedade color. Veja o código abaixo para entender melhor:

.. code-block:: css

    p {
        background: red;
        padding: 10px;
        font: 13px verdana;
        color: green;
        border: 1px solid currentColor;
    }



Veja que apliquei o valor currentColor onde deveria estar o valor de cor da propriedade border. Agora, toda vez que o
valor da propriedade color for modificado, o currentColor aplicará o mesmo valor para a propriedade onde ela está sendo
aplicada.

Isso funciona em qualquer propriedade que faça utilização de cor como background, border, etc. Obviamente não funciona
para a propriedade color. O currentColor também não funciona em seletores separados, ou seja, você não consegue
atrelar o valor da propriedade color ao currentColor de outro seletor.


Font-face
=========

A regra @font-face é utilizada para que você utilize fontes fora do padrão do sistema em seus sites.

Para que isso funcione, nós disponibilizamos as fontes necessárias em seu servidor e linkamos estas fontes no arquivo
CSS. A sintaxe é bem simples e tem suporte a todos os navegadores, com algumas ressalvas.

.. code-block:: css

    @font-face {
        font-family: helveticaneue;
        src: url(helveticaNeueLTStd-UltLt.otf);
    }


Na primeira linha você customiza o nome da font que você usará durante todo o projeto. Na segunda linha definimos a
URL onde o browser procurará o arquivo da font para baixar e aplicar no site.

Você aplica a fonte como abaixo:

.. code-block:: css

    p {
        font: 36px helveticaneue, Arial, Tahoma, Sans-serif;
    }



Suponha que o usuário tenha a font instalada, logo ele não precisa baixar a font, assim podemos indicar para que o
browser possa utilizar o arquivo da própria máquina do usuário. Menos requisições, mais velocidade. Veja o código abaixo:

.. code-block:: css

    @font-face {
        font-family: helveticaneue;
        src: local(HelveticaNeueLTStd-UltLt.otf), url(HelveticaNeueLTStd-UltLt.otf);
    }


Media queries
=============



Novos recursos (Border-radius, Gradients, Box-shadow, etc)
==========================================================
