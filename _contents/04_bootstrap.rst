***************
Bootstrap (16h)
***************

Visão geral
===========



Obtendo e instalando
====================



Estrutura de arquivos
=====================



Template básico
===============



Sistema de grids (básico, offset, aninhamento e fluido)
=======================================================



Layouts (fixo e fluido)
=======================



Layout responsive (media queries)
=================================



Tabelas, formulários, botões, imagens e ícones
----------------------------------------------



Componentes (grupos de botões, navegação, paginação, alertas e etc)
-------------------------------------------------------------------



Plugins JavaScript (janelas, dropdown, abas, tooltips, painel de colapso, slide)
--------------------------------------------------------------------------------



Customizando o template
-----------------------
