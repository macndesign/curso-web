*************
Back end (8h)
*************

Introdução a tecnologias back-end
=================================



Preparando conteúdo pra tecnologia server-side
==============================================



Instalação e configurações do ambiente
======================================

Instalando o Python
-------------------

Para Windows vamos instalar o ActivePython da ActiveState:

http://www.activestate.com/activepython/downloads/

Até o momento a versão recomendada é o ActivePython 2.7.2 (x86 for Windows)

.. note::

    Para instalar o ActivePython é bem simples, basta ir avançando na instalação até que seja concluída.
    Como o de costume em instaladores Windows

    Já para a grande maioria das distribuições Linux e MacOS o Python já vem instalado.



Instalando o PyCharm (IDE para projetos Python em geral)
--------------------------------------------------------

Na seguinta página vamos baixar o ultimo PyCharm que atualmente está na versão 2.7

http://www.jetbrains.com/pycharm/

.. note::

    Para instalar o PyCharm é bem simples, basta ir avançando na instalação até que seja concluída.
    Na hora de escolher o interpretador Python, basta selecionar o padrão da máquina.


Iniciando um projeto django
---------------------------

Ao abrir o PyCharm pela primeira vez é iniciada uma tela com algumas ações iniciais, vamos clicar em Create New Project

.. figure:: ../_images/pycharm-welcome.png



Ao entrar no wizard de criação de um novo projeto, iserimos um nome para o projeto, em nosso caso "blog", o local onde o
projeto será salvo em seu HD (que no meu caso eu coloquei em c:\works\blog), para o tipo do projeto, vamos escolher
Django Project e o interpretador padrão do Python instalado em sua máquina.

.. figure:: ../_images/pycharm-create-new-project.png



Agora algumas configurações opcionais para finlizar o processo de criação do projeto. O nome já está selecionado como
blog, para o nome da nossa primeira aplicação, vamos chamar de core (será uma aplicação que guardará as principais
funcionalidades de um blog) e por enquanto vamnos deixar os templates do projeto nessa pasta que ele indica e pra
finalizar as configurações opcionais, vamos logo habilitar o admin do django selecionando Enabled Django Admin.

.. figure:: ../_images/pycharm-project-settings.png



Após a criação do projeto, a tela do PyCharm já é iniciada com o projeto configurado

.. figure:: ../_images/pycharm-primeira-tela.png



Criando ambientes virtuais isolados com o PyCharm
-------------------------------------------------

Para não poluir o Python que está instalado como padrão na máquina com a instalação de bibliotecas desnecessárias.

Para isso vamos as ferramentas que ficam no topo da IDE, clicaremos em Settings que o ícone é uma Chave mais uma
engrenagem, como na figura:

.. figure:: ../_images/pycharm-my-settings.png



No lado esquerdo clique em Project Interpreter, ao abrir a tela de configuração do interpretador do projeto, clique em
Configure interpreters (frase sublinhada em azul)

.. figure:: ../_images/pycharm-project-iterpreter.png



Clique no ícone do Python (com um "v") ao lado direito da tela denominado de Create virtual environment, como na imagem
a seguir:

.. figure:: ../_images/pycharm-create-virtual-environment.png



Nomeie seu ambiente virtual do nome que quiser, mas para o nosso caso eu dei o nome de venv (abreviação para virtual
environment) e coloquei o ambiente virtual (venv) dentro do próprio projeto: c:\works\blog\venv

Como está na imagem abaixo:

.. figure:: ../_images/pycharm-create-virtual-environment-dialog.png



Aguarde o PyCharm terminar de criar seu ambiente virtual...

.. figure:: ../_images/pycharm-create-virtual-environment-creating.png



Clique em Ok para finalizar o processo de criação de seu ambiente virtual

.. figure:: ../_images/pycharm-create-virtual-environment-ok.png



Vamos instalar as bibliotecas de uma forma mais rápida e eficiente criando um arquivo requirements.txt dentro do nosso
projeto, logo na raiz da pasta blog.

.. line-block::

    Django==1.5.1
    South==0.7.6
    Unipath==0.2.1
    distribute==0.6.35
    django-taggit==0.9.3
    wsgiref==0.1.2



Descrição rápida dos principais pacotes que serão instalados:

* Django: Framework web que abstrai muito do trabalho que teríamos para codificar uma aplicação web do "zero"
* South: Solução para que o desenvolvedor tenha o mínimo de esforço na hora de fazer migrações e esquema de dados
* Unipath: Trabalha de forma cross-plataforma com caminhos do sistema operacional
* django-taggit: Aplicação pronta para "plugarmos" em nosso projeto e ter facilmente tags relacionadas às postagens

Feche e abra novamente o projeto blog com o PyCharm, veja que no topo aparecem os pacotes necessários para nosso
projeto do blog, então vamos clicando em ``Install requirements`` que está de azul (sublinhado) ao lado do nome dos
pacotes a serem instalados, até que todos tenham sido instalados.

.. figure:: ../_images/pycharm-package-requirements.png



Exemplo de tela com os ultimos pacotes instalados:

.. figure:: ../_images/pycharm-package-requirements-success.png



Agora vamos apenas executar nosso projeto, apenas pra ver se está funcionando. Clique no ícone de "Play" que fica no
posicionado nas ferramentas do topo do PyCharm como na imagem abaixo:

.. figure:: ../_images/pycharm-project-run.png



Agora vamos executar no navegador acessando o endereço http://localhost:8000 veremos que dar um erro com o código 404
de página não encontrada

.. figure:: ../_images/pycharm-project-run-page-not-found.png



O motivo desse erro é que não temos nenhuma url apontando para nosso endereço de raiz do projeto, podemos conferiri isso
verificando o arquivo urls.py que está dentro de c:\works\blog\blog\urls.py

.. code-block:: python

    # Resumindo o código que temos lá, temos apenas 2 urls
    urlpatterns = patterns('',

        # Essa que aponta para a documentação do django admin
        url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

        # E essa que aponta para o próprio django admin
        url(r'^admin/', include(admin.site.urls)),
    )


E se apontarmos o endereço do navegador para http://localhost:8000/admin ?

Então vamos experimentar! Teremos o seguinte erro:

.. figure:: ../_images/pycharm-project-run-improperly-configured-at-admin.png



O django reclama que temos que configurar o banco de dados lá no arquivo settings.py que está em c:\works\blog\blog

Para configurar o banco de dados vamos fazer o seguinte:

Logo abaixo do comentário

.. code-block:: python

    # Django settings for blog project.



Importe o módulo Unipath e crie uma variável que representará a raiz do projeto

.. code-block:: python

    from unipath import Path
    PROJECT_DIR = Path(__file__).parent



E por fim vamos configurar o banco de dados para o SQLite3 e colocá-lo para criar um arquivo do db na pasta \blog\blog

.. code-block:: python

    DATABASES = {
        'default': {
            'ENGINE': 'django.db.backends.sqlite3',  # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
            'NAME': PROJECT_DIR.child('dev.db'),  # Or path to database file if using sqlite3.
            'USER': '',                      # Not used with sqlite3.
            'PASSWORD': '',                  # Not used with sqlite3.
            'HOST': '',                      # Set to empty string for localhost. Not used with sqlite3.
            'PORT': '',                      # Set to empty string for default. Not used with sqlite3.
        }
    }



Configuramos o banco no settings, e agora é só rodar novamente? Vamos experimentar!

.. figure:: ../_images/pycharm-project-run-database-error-at-admin.png



Dessa acho que você já deve imaginar só pelo nome do erro, DatabaseError at /admin/

Isso mesmo! Ainda não criamos tabelas necessárias para o aplicativo de administração do django funcionar corretamente.

Mas podemos criar o banco de dados facilmente indo em ``Tools > Run manage.py task`` selecione ``syncdb`` e tecle enter

.. figure:: ../_images/pycharm-project-run-syncdb.png



Após rodar o syncdb (comando que cria as tabelas necessárias do projeto), comece a preencher o wizard:

.. figure:: ../_images/pycharm-project-run-syncdb-wizard.png



Perguntas com as respostas:

.. line-block::

    You just installed Django's auth system, which means you don't have any superusers defined.
    Would you like to create one now? (yes/no): ``yes``
    Username (leave blank to use 'mario'): ``admin``
    Email address: ``admin@admin.com``
    Warning: Password input may be echoed.
    Password: ``admin``
    Warning: Password input may be echoed.
    Password (again): ``admin``
    Superuser created successfully.
    Installing custom SQL ...
    Installing indexes ...
    Installed 0 object(s) from 0 fixture(s)



Ao rodar novamente a aplicação clicando em Rerun

.. figure:: ../_images/pycharm-project-run-re-run.png



Construindo o backend da aplicação
==================================

Continuar configurando o projeto a partir do arquivo settings.py
----------------------------------------------------------------

Em ``TIME_ZONE = 'America/Chicago'`` mudar para ``TIME_ZONE = 'America/Fortaleza'``

Em ``LANGUAGE_CODE = 'en-us'`` mudar para ``LANGUAGE_CODE = 'pt-br'``

Em ``MEDIA_ROOT = ''`` alterar para ``MEDIA_ROOT = PROJECT_DIR.child('media')``

Em ``MEDIA_URL = ''`` alterar para ``MEDIA_URL = '/media/'``

Em ``STATIC_ROOT = ''`` alterar para ``STATIC_ROOT = PROJECT_DIR.child('public')``

Em ``STATIC_URL = ''`` alterar para ``STATIC_URL = '/static/'``

Remover o ``import os`` que está acima do ``TEMPLATE_DIRS``

Alterar o ``TEMPLATE_DIRS = (os.path.join(os.path.dirname(__file__), '..', 'templates').replace('\\','/'),)`` para ``TEMPLATE_DIRS = ()``



Na em ``INSTALLED_APPS`` adicione as apps ``core``, ``taggit`` e ``south``

.. code-block:: python

    INSTALLED_APPS = (
        'django.contrib.auth',
        'django.contrib.contenttypes',
        'django.contrib.sessions',
        'django.contrib.sites',
        'django.contrib.messages',
        'django.contrib.staticfiles',
        # Uncomment the next line to enable the admin:
        'django.contrib.admin',
        # Uncomment the next line to enable admin documentation:
        # 'django.contrib.admindocs',
        'core',
        'taggit',
        'south',
    )



Após adicionar as aplicações ao ``INSTALLED_APPS`` rode o comando syncdb novamente para que as tabelas do banco sejam
atualizadas com as três novas apps.

Agora vamos criar a classe de modelo do nosso blog
==================================================

Definindo a class Post
----------------------

Vamos abrir o arquivo ``models.py`` que está dentro da pasta ``core``para codificar nossa classe de modelo

.. code-block:: python

    # coding: utf-8
    from django.db import models
    from taggit.managers import TaggableManager


    class Post(models.Model):
        autor = models.ForeignKey(
            to='auth.User',
            help_text='Selecione o autor da postagem'
        )
        titulo = models.CharField(
            verbose_name=u'Título',
            max_length=120,
            help_text=u'Digite um título adequado para a postagem'
        )
        sub_titulo = models.CharField(
            verbose_name=u'Sub-título',
            max_length=120,
            help_text=u'Digite um sub-título adequado para a postagem'
        )
        data_publicacao = models.DateTimeField(
            verbose_name=u'Data de publicação',
            auto_now_add=True,
            editable=True
        )
        conteudo = models.TextField(
            verbose_name=u'Conteúdo'
        )
        tags = TaggableManager()

        def get_absolute_url(self):
            return '/%s' % self.pk

        def __unicode__(self):
            return self.titulo



.. note::

    Note que assim como o arquivo html, um arquivo Python 2.x também precisa da codificação utf-8 declarada no topo do
    documento ``# coding: utf-8`` mas a partir do Python 3.x isso não é mais necessário, pois UTF-8 virou a codificação
    padrão para os arquivos Python.

    Veja os dados que nossa tabela Post vai guardar: Autor, Título, Sub-Título, Data de publicação, Conteúdo e tags



Ao criar nossa classe Post, precisamos sincronizar com o banco de dados, para que ele crie nossa tabela Post com as
colunas que especificamos nela.

Agora vamos fazer com quê possamos incluir, alterar, excluir, buscar e visualizar os dados das nossas postagens através
da área administrativa do django



Habilitando nossa app Core com o admin do django
------------------------------------------------

Crie um arquivo chamado ``admin.py`` na raiz da pasta ``core`` e nesse arquivo digite o seguinte:

.. code-block:: python

    from django.contrib import admin
    from core.models import Post


    class PostAdmin(admin.ModelAdmin):
        list_display = ['autor', 'titulo', 'data_publicacao']

    admin.site.register(Post, PostAdmin)



.. note::

    Veja que criamos uma class PostAdmin para alterar um pouco a forma como a postagem é visualizada na listagem.
    Por padrão a listagem tem apenas a representação do objeto que no caso é o que aparece no método ``__unicode__``
    da class Post que está no ``models.py`` da app ``core``, nesse caso eu quis que aparece no display da listagem
    o nome do autor, título e data de publicação. Existe muito mais configurações para o admin do django, praticamente
    você pode deixar ele da forma que você desejar.



Agora vamos acessar novamente http://localhost:8000/admin e ver como está nossa página administrativa para o blog.


Noção de como integrar o front-end com o back-end
=================================================
