******************************************************
Tradução sobre o elemento article do site HTML5 Doctor
******************************************************



O elemento ARTICLE da HTML5
===========================

Nós já apresentamos uma série dos novos elementos da HTML5 aqui no site HTML5Doctor, mas o elemento article escapou do
nosso microscópio… até agora! Esse é um dos novos elementos para marcar seções. É por isso mesmo frequentemente
confundido com os elementos section e div, mas não se preocupe, iremos mostrar as diferenças entre eles.



O que diz a especificação
-------------------------

.. note::

    O elemento ``article`` destina-se a marcar um conteúdo que se constitua em um componente **autossuficiente** em uma
    página, aplicação ou site e que possa ser distribuido ou reusado de forma **independente (isolada)**, como por exemplo
    via sindicalização. Tal conteúdo pode ser um post em um fórum, um artigo de uma revista ou jornal, uma matéria em um
    blog, um comentário de um usuário, um widget ou gadget interativo ou qualquer outro item independente de conteúdo.
    .. _Especificação: http://www.w3.org/TR/html5/semantics.html#the-article-element



Além do seu conteúdo principal o elemento <article> tipicamente contém um cabeçalho (em geral dentro do elemento header)
e algumas vezes um rodapé. A maneira mais fácil de conceituar <article> é imaginando seu uso em um weblog, tal como
mencionado nos exemplos contidos na especificação: “uma matéria em um blog” e “um comentário de um usuário”. No site HTML5
Doctor nós marcamos cada matéria com o elemento <article>. Também usamos o elemento <article> para marcar os conteúdos
das páginas “estáticas”, tais como, as páginas About e Contact uma vez que o elemento <article> popde ser usado para
marcar “qualquer outro item independente de conteúdo”. A questão é saber o que exatamente significa qualquer outro item
independente de conteúdo”?



Teste para saber o que é conteúdo independente
----------------------------------------------

Conteúdo independente próprio para ser marcado com uso do elemento <article> é todo conteúdo que faça sentido
isoladamente. A interpretação desse conceito é com você, contudo uma maneira fácil de chegar a uma conclusão é
respondendo à pergunta: esse conteúdo é apropriado para se publicar em um feed RSS? Sem dúvida uma matéria em um weblog
ou páginas estáticas são uma boa opção para um leitor de feed, assim como weblogs costumam disponibilizar feeds para
comentários. Por outro lado, colocar em um feed cada parágrafo de uma matéria não teria utilidade alguma. A chave aqui é
que o conteúdo deverá fazer sentido independentemente do contexto no qual foi inserido, ou seja, fazer sentido ainda que
todo o conteúdo emvolta tenha sido removido.



Exemplos de uso de <article>
----------------------------



Uso básico de <article>
+++++++++++++++++++++++

Conteúdo com um cabeçalho e algum texto que faça sentido isoladamente (considere a existência de muito mais texto
sobre apples.

.. code-block:: html

    <article>
        <h1>Apple</h1>
        <p>The <b>apple</b> is the pomaceous fruit of the apple tree...</p>
                ...
    </article>



<article> em um weblog
++++++++++++++++++++++

A presença da data de publicação da matéria sugere o uso do elemento <header> e nesse encontramos, também, conteúdo para
ser marcado com o elemento ``<footer>``.

.. code-block:: html

    <article>
        <header>
            <h1>Apple</h1>
            <p>Published: <time pubdate="pubdate">2009-10-09</time></p>
        </header>
        <p>The <b>apple</b> is the pomaceous fruit of the apple tree...</p>
        ...
        <footer>
            <p><small>Creative Commons Attribution-ShareAlike License</small></p>
        </footer>
    </article>



Uso de <article> com área de comentários marcada com <article>s aninhados
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

Nesse exemplo mostramos uma matéria com comentários em um weblog. Cada comentário é marcado com uso de um elemento
``<article>`` aninhado com ``<article>``.

.. code-block:: html

    <article>
        <header>
            <h1>Apple</h1>
            <p>Published: <time pubdate datetime="2009-10-09">9th October, 2009</time></p>
        </header>
        <p>The <b>apple</b> is the pomaceous fruit of the apple tree...</p>
        ...

        <section>
            <h2>Comments</h2>
            <article>
                <header>
                    <h3>Posted by: Apple Lover</h3>
                    <p><time pubdate datetime="2009-10-10T19:10-08:00">~1 hour ago</time></p>
                </header>
                <p>I love apples, my favourite kind are Granny Smiths</p>
            </article>

            <article>
                <header>
                    <h3>Posted by: Oranges are king</h3>
                    <p><time pubdate datetime="2009-10-10T19:15-08:00">~1 hour ago</time></p>
                </header>
                <p>Urgh, apples!? you should write about ORANGES instead!!1!</p>
            </article>
        </section>
    </article>



Uso de <article> com <section>s
+++++++++++++++++++++++++++++++

Você pode usar o elemento ``<section>`` para dividir o conteúdo de ``<article>`` em grupos contendo cabeçalhos próprios:

.. code-block:: html

    <article>
        <h1>Apple varieties</h1>
        <p>The apple is the pomaceous fruit of the apple tree...</p>

        <section>
            <h2>Red Delicious</h2>
            <p>These bright red apples are the most common found in many supermarkets...</p>
        </section>

        <section>
            <h2>Granny Smith</h2>
            <p>These juicy, green apples make a great filling for apple pies...</p>
        </section>

    </article>



Uso de <section> contendo <article>s
++++++++++++++++++++++++++++++++++++

Quando apropriado um elemento ``<section>`` poderá conter elementos ``<article>``. Já vimos esse caso no exemplo para
comentários mostrado anteriormente. Um outro uso comum para esse é a categotização de páginas em um weblog:

.. code-block:: html

    <section>
        <h1>Articles on: Fruit</h1>

        <article>
            <h2>Apple</h2>
            <p>The apple is the pomaceous fruit of the apple tree...</p>
        </article>

        <article>
            <h2>Orange</h2>
            <p>The orange is a hybrid of ancient cultivated origin, possibly between pomelo and tangerine...</p>
        </article>

        <article>
            <h2>Banana</h2>
            <p>Bananas come in a variety of sizes and colors when ripe, including yellow, purple, and red...</p>
        </article>

    </section>



Uso <article> para marcar um widget
+++++++++++++++++++++++++++++++++++

A especificação menciona que um widget interativo deve ser marcado com ``<article>``. O exemplo a seguir mostra a marcação
para um widget incorporado à página, tal como em .. _Widgetbox: http://www.widgetbox.com/

.. code-block:: html

    <article>
        <h1>My Fruit Spinner</h1>
        <object>
            <param name="allowFullScreen" value="true">
            <embed src="#" width="600" height="395"></embed>
        </object>
    </article>



O atributo pubdate
------------------

Usamos o atributo ``pubdate`` em exemplos anteriores. Trata-se de um atributo booleano a ser adicionado a um elemento
time contido em ``<article>``. Se presente, esse atributo indica que no
.. _elemento_time: http://html5doctor.com/the-time-element a data em que ``<article>`` foi publicado. Ele admite
sintaxes alternativas, como mostradas a seguir:

    pubdate
    pubdate="pubdate"

As sintaxes são compatíveis com as linguagens HTML e XHTML respectivamente — o resultado é o mesmo, portando use a
sintaxe que lhe convier. Convém ressaltar que o atributo pubdate aplica-se somente ao elemento <article> pai ou então à
pagina como um todo.



Diferenças entre <article> e <section>
--------------------------------------

Tem havido muita confusão com relação a diferença (ou diferença perceptível) entre os elementos ``<article>`` e
``<section>`` da HTML5. O elemento ``<article>`` é uma espécie de ``<section>`` especializada; ele possui um valor
semântico  mais específico do que o valor semântico de ``<section>`` no sentido de ser um bloco de conteúdos
relacionados **independente** e **autossuficiente**. Podemos usar ``<section>``, mas usando ``<article>`` estamos
reforçando a semântica do conteúdo.

Por outro lado ``<section>`` é tão somente um bloco de conteúdos relacionados e ``<div>`` um simples bloco de conteúdos.
Como mencionado anteriormente o atributo pubdate não se aplica a ``<section>``. Para decidir qual elemento usar escolha
a primeira resposta para as seguintes perguntas:

1. O conteúdo faz sentido se inserido em um leitor de feed? Se a resposta for sim, use ``<article>``
2. O conteúdo é relacionado? Se a resposta for sim, use ``<section>``
3. Existe relacionamento do conteúdo com a semântica? Se a resposta for não, use ``<div>``

