**************************
JavaScript com jQuery (8h)
**************************

Introdução
==========

jQuery é construído sobre JavaScript, uma linguagem rica e expressiva por si só. Esta sessão cobre os
conceitos básicos de JavaScript, assim como algumas pegadinhas que aparecem com freqüência para
pessoas que ainda não usaram JavaScript. Por enquanto, para pessoas sem experiência em programação,
até mesmo para aquelas que já programam em outras linguagens, podem se beneficiar ao aprender um
pouco sobre algumas particularidades de JavaScript.

Se você está interessado em aprender mais sobre a linguagem JavaScript, eu recomento fortemente o livro
O Melhor do JavaScript do Douglas Crockford.


O Básico de sintaxe
===================

Entendendo declarações, nomes de variáveis, espaço, e outras sintaxes básicas do JavaScript
-------------------------------------------------------------------------------------------

Exemplo 2.1. Uma simples declaração de variável

.. code-block:: javascript

    var foo = 'ola mundo';



Exemplo 2.2. Espaços em branco não interferem fora das aspas.

.. code-block:: javascript

    var foo = 'olá mundo';



Exemplo 2.3. Parênteses indicam precedência

.. code-block:: javascript

    2 * 3 + 5; // retorna 11; multiplicação acontece antes
    2 * (3 + 5); // retorna 16; adição acontece primeiro



Exemplo 2.4. Tabulação melhora a leitura, mas não tem significado especial

.. code-block:: javascript

    var foo = function() {
        console.log('olá');
    };



Operadores
==========

Operadores Básicos
------------------

Operadores básicos permitem que você manipule valores

Exemplo 2.5. Concatenação

.. code-block:: javascript

    var foo = 'olá';
    var bar = 'mundo';
    console.log(foo + ' ' + bar); // 'olá mundo'



Exemplo 2.6. Multiplicação e divisão

.. code-block:: javascript

    2 * 3;
    2 / 3;


Exemplo 2.7. Incrementando e decrementando

.. code-block:: javascript

    var i = 1;
    var j = ++i; // pré-incremento: j é igual a 2; i é igual a 2
    var k = i++; // pós-incremento: k é igual a 2; i é igual a 3



Operações sobre números & Strings
---------------------------------

Em JavaScript, números e strings ocasionalmente irão se comportar de formas inesperadas.

Exemplo 2.8. Adição vs. Concatenação

.. code-block:: javascript

    var foo = 1;
    var bar = '2';
    console.log(foo + bar); // 12. uh oh



Exemplo 2.9. Forçando uma string atuar como um número

.. code-block:: javascript

    var foo = 1;
    var bar = '2';
    // converte string para numero
    console.log(foo + Number(bar));



O construtor Number, quando chamado como função (como no exemplo acima), terá o efeito de mudar seu
argumento para um número. Você pode também usar o operador unário de adição, que faz a mesma coisa:

Exemplo 2.10. Forçando uma string a atuar como um número (usando o operador unário de adição)

.. code-block:: javascript

    console.log(foo + +bar);



Operadores lógicos
------------------

Operadores lógicos permitem que você teste uma série de operadores usando as operações lógicas AND e OR.

Exemplo 2.11. Os operadores lógicos AND e OR

.. code-block:: javascript

    var foo = 1;
    var bar = 0;
    var baz = 2;
    foo || bar; // retorna 1, que é verdadeiro
    bar || foo; // retorna 1, que é verdadeiro
    foo && bar; // retorna 0, que é falso
    foo && baz; // retorna 2, que é verdadeiro
    baz && foo; // retorna 1, que é verdadeiro


Apesar de não estar claro pelo exemplo, o operador || retorna o valor do primeiro operador verdadeiro,
ou, em casos em que nenhum operador seja verdadeiro, ele irá retornar o último dos operandos. O operador
&& retorna o valor do primeiro operando falso, ou o valor do último operador se ambos operadores forem
verdadeiros.

Tenha certeza de consultar “Coisas Verdadeiras e Falsas” para maiores detalhes sobre quais valores são
avaliados como true e quais são avaliados como false.

.. note::

    Algumas vezes, você verá alguns desenvolvedores usando estes operadores lógicos para controle
    de fluxo ao invés de utilizar o if. Por exemplo:



.. code-block:: javascript

    // faz alguma coisa com foo se foo for verdadeiro
    foo && doSomething(foo);
    // seta bar para baz se baz é verdadeiro;
    // senão for verdadeiro, seta-o para retornar
    // o valor de createBar()
    var bar = baz || createBar();



Este estilo é elegante e agradevelmente curto; por isso, pode ser difícil ler, especialmente por
iniciantes. Eu o mostro aqui para que você possa reconhece-lo no código que você lê, mas eu não
recomendo o uso até que você esteja extremamente confortável com o que isto significa e como
você espera que se comporte.



Operadores de comparação
------------------------

Operadores de comparação premite testar quando valores são equivalentes ou quando são idênticos.

Exemplo 2.12. Operadores de comparação

.. code-block:: javascript

    var foo = 1;
    var bar = 0;
    var baz = '1';
    var bim = 2;
    foo == bar; // retorna false
    foo != bar; // retorna true
    foo == baz; // retorna true; cuidado!
    foo === baz; // retorna false
    foo !== baz; // retorna true
    foo === parseInt(baz); // retorna true
    foo > bim; // retorna false
    bim > baz; // retorna true
    foo <= baz; // retorna true



Código condicional
==================

Algumas vezes você só irá querer executar um bloco de código sob certas condições. Controle de fluxo -
através dos blocos if e else - permite que você execute uma parte do código em certas condições.

Exemplo 2.13. Controle de fluxo

.. code-block:: javascript

    var foo = true;
    var bar = false;
    if (bar) {
        // este código nunca será executado
        console.log('hello!');
    }
    if (bar) {
        // este código não irá executar
    } else {
        if (foo) {
            // este código irá executar
        } else {
            // este código poderá ser executado se foo e bar forem ambos falsos.
        }
    }



.. note::

    As chaves não são estritamente necessárias quando você tem blocos if com uma só linha, porém,
    se forem usados consistentemente, mesmo que não sejam estritamente requeridos, eles podem
    fazer seu código ficar mais legível.

    Esteja atento para não definir funções com o mesmo nome várias vezes dentro de blocos de código if/
    else, senão pode haver resultados inesperados.



Coisas Verdadeiras e Falsas
---------------------------

De forma a usar o controle de fluxo com sucesso, é importante entender quais tipos de valores são
"verdadeiros" e quais tipos de valores são "falsos". Algumas vezes, valores que parecem ser avaliados de
uma forma, são avaliados de outra.

Exemplo 2.14. Valores que são avaliados como true

    '0';
    'qualquer string';
    []; // um array vazio
    {}; // um objeto vazio
    1; // qualquer número diferente de zero

Exemplo 2.15. Valores que são avaliados como false

    0;
    ''; // uma string vazia
    NaN; // Variável "not-a-number" do JavaScript
    null;
    undefined; // seja cuidadoso -- undefined pode ser redefinido!



Associação Condicional de Variáveis Com o Operador Ternário
-----------------------------------------------------------

Algumas vezes você precisará colocar um valor numa variável dependendo de uma condição. Você poderia
usar uma declaração if/else, mas em muitos casos, o operador ternário é mais conveniente.

.. note::

    O operador ternário testa uma condição; se a condição for verdadeira, ele retorna um valor correto, senão
    retorna um valor diferente.



Exemplo 2.16. O operador ternário

.. code-block:: javascript

    // seta foo pra 1 se bar for verdadeiro;
    // senão, seta foo pra 0
    var foo = bar ? 1 : 0;



Enquanto o operador ternário pode ser usado sem associação do valor de retorno para uma variável, isto é geralmente desencorajado.



Declaração Switch
-----------------

Ao invés de usar uma série de blocos if/else if/else, algumas vezes pode ser útil usar a declaração switch.

.. note::

    Declarações Switch olham o valor da variável ou expressão e executa blocos de código
    diferentes dependendo do valor.



Exemplo 2.17. Uma declaração switch

.. code-block:: javascript

    switch (foo) {
        case 'bar':
            alert('o valor é bar -- yay!');
            break;
        case 'baz':
            alert('boo baz :(');
            break;
        default:
            alert('todo o resto está ok');
            break;
    }

As declarações Switch de alguma forma favorecem o JavaScript, pois frequentemente o mesmo
comportamento pode ser obtido através da criação de um objeto que tem um maior potencial para reuso,
teste, etc. Por exemplo:

.. code-block:: javascript

    var stuffToDo = {
        'bar' : function() {
            alert('o valor é bar -- yay!');
        },
        'baz' : function() {
            alert('boo baz :(');
        },
        'default' : function() {
            alert('todo o resto está ok');
        }
    };

    if (stuffToDo[foo]) {
        stuffToDo[foo]();
    } else {
        stuffToDo['default']();
    }



Nós daremos uma olhada mais profunda em objetos mais a frente neste capítulo.


Laços
=====

Laços permitem que você execute um bloco de código por um certo número de vezes.

Exemplo 2.18. Laços

.. code-block:: javascript

    // loga 'tentativa 0', 'tentativa 1', ..., 'tentativa 4'
    for (var i=0; i<5; i++) {
        console.log('tentativa ' + i);
    }



Note que em Exemplo 2.18, “Laços” mesmo que usemos a palavra chave var antes do nome da variável
i, o escopo da variável i não é alterado para o bloco do laço. Nós iremos discutir sobre escopo com mais
profundidade mais a frente neste capítulo.



O laço for
----------

O laço for é constituído de quatro declarações e possui a seguinte estrutura:

    for ([inicialização]; [condição]; [incremento])
        [corpoDoLaço]



A declaração inicialização é executada somente uma vez, antes do loop iniciar. Ela lhe dá uma
oportunidade de preparar ou declarar qualquer variável.

A declaração condição é executada antes de cada iteração, e o seu valor de retorno decide se o loop continua
ou não. Se a declaração condicional for interpretada como um valor falso, o laço para de ser executado.

A declaração incremento é executada no fim de cada iteração e lhe dá a oportunidade de mudar o estado
de variáveis importantes. Tipicamente, isso irá envolver o incremento ou decremento de um contador e
assim fazer com que o loop sempre chegue ao seu fim.

A declaração corpoDoLaço é o que é executado em cada iteração. Ele pode conter o que você quiser.

Tipicamente, você terá múltiplas declarações que precisam ser executadas e portanto você irá colocá-las
dentro de um bloco de código ({...}).

Este é um uso típico para o laço for:

Exemplo 2.19. Um laço for típico

.. code-block:: javascript

    for (var i = 0, limit = 100; i < limit; i++) {
        // Este bloco será executado 100 vezes
        console.log('Estou em ' + i);
        // Nota: o último log será: "Estou em 99"
    }



O laço while
------------

O laço while é similar a uma declação if, exceto que seu corpo continuará executando até que a condição
seja interpretada como falso.

    while ([condição]) [corpoDoLaço]



Este é um laço while típico:

Exemplo 2.20. Um laço while típico

.. code-block:: javascript

    var i = 0;
    while (i < 100) {
        // Este bloco será executado 100 vezes
        console.log('Estou em ' + i);
        i++; // incrementa i
    }



Você irá perceber que nós temos que incrementar o contador dentro do corpo do laço. É possível combinar
o incremento e a condição, desse jeito:

Exemplo 2.21. Um laço while com uma condição e incremento combinado

.. code-block:: javascript

    var i = -1;
    while (++i < 100) {
        // Este bloco será executado 100 vezes
        console.log('Estou em ' + i);
    }



Perceba que nós estamos iniciando em -1 e usando o incremento prefixo (++i).



O laço do-while
---------------

É quase a mesma coisa do laço while, exceto pelo fato que o corpo do laço é executado pelo menos uma vez antes da condição ser testada.

    do [corpoDoLaço] while ([condição])

Este é um exemplo de código com do-while:

Exemplo 2.22. Um laço do-while

.. code-block:: javascript

    do {
        // Mesmo que a condição seja interpretada como false
        // este corpo do laço ainda será executado uma vez
        alert('Olá!');
    } while (false);



Estes tipos de laços são relativamente raros, pois poucas situações requerem um laço que execute cegamente um bloco de código ao menos uma vez. Independentemente, é bom ficar sabendo disso.



Parando e continuando
---------------------

Geralmente, um loop termina porque a declaração condicional não é interpretada como verdadeira, mas é possível parar um laço dentro do corpo do mesmo utilizando a declaração break.

Exemplo 2.23. Parando um laço

.. code-block:: javascript

    for (var i = 0; i < 10; i++) {
        if (algumaCoisa) {
            break;
        }
    }



Você também pode desejar continuar o laço sem executar o resto do seu corpo. Isso é feito usando a declaração continue.

Exemplo 2.24. Pulando para a próxima iteração de um loop

.. code-block:: javascript

    for (var i = 0; i < 10; i++) {
        if (algumaCoisa) {
            continue;
        }
        // A declaração seguinte somente será executada se
        // a condição 'algumaCoisa' for falsa
        console.log('Cheguei até aqui');
    }



Palavras Reservadas
===================

JavaScript possui um número de “palavras reservadas”, ou palavras que têm significado especial na linguagem. Você deve evitar usar estas palavras em seu código, exceto quando você as usar com seu significado desejado.

    * break
    * case
    * catch
    * continue
    * default
    * delete
    * do
    * else
    * finally
    * for
    * function
    * if
    * in
    * instanceof
    * new
    * return
    * switch
    * this
    * throw
    * try
    * typeof
    * var
    * void
    * while
    * with
    * abstract
    * boolean
    * byte
    * char
    * class
    * const
    * debugger
    * double
    * enum
    * export
    * extends
    * final
    * float
    * goto
    * implements
    * import
    * int
    * interface
    * long
    * native
    * package
    * private
    * protected
    * public
    * short
    * static
    * super
    * synchronized
    * throws
    * transient
    * volatile


Arrays
======

Arrays são listas de valores indexadas por zero. Eles são uma forma útil de armazenar um conjunto de itens relacionados
do mesmo tipo (como strings), apesar que na realidade, um array pode incluir múltiplos tipos de itens, incluindo outros arrays.

Exemplo 2.25. Um array simples

    var meuArray = [ 'Olá', 'Mundo' ];


Exemplo 2.26. Acessando os itens do array pelo índice

.. code-block:: javascript

    var meuArray = [ 'ola', 'mundo', 'foo', 'bar' ];
    console.log(meuArray[3]); // loga 'bar'



Exemplo 2.27. Testando o tamanho de um array

.. code-block:: javascript

    var meuArray = [ 'olá', 'mundo' ];
    console.log(meuArray.length); // loga 2



Exemplo 2.28. Mudando o valor de um item do array

.. code-block:: javascript

    var meuArray = [ 'olá', 'mundo' ];
    meuArray[1] = 'mudado';



Enquanto é possível mudar o valor de um item do array como mostrado no Exemplo 2.28, “Mudando o valor de um item do array”,
geralmente não é recomendado.

Exemplo 2.29. Adicionando elementos em um array

.. code-block:: javascript

    var meuArray = [ 'olá', 'mundo' ];
    meuArray.push('novo');



Exemplo 2.30. Trabalhando com arrays

.. code-block:: javascript

    var meuArray = [ 'h', 'e', 'l', 'l', 'o' ];
    var minhaString = meuArray.join(''); // 'hello'
    var meuSplit = minhaString.split(''); // [ 'h', 'e', 'l', 'l', 'o' ]


Objetos
=======

Objetos contém um ou mais pares chave-valor. A porção chave pode ser qualquer string. A porção valor pode ser qualquer
tipo de valor: um número, uma string, um array, uma função ou até um outro objeto.

.. note::

    Quando um desses valores é uma função, é chamado de método do objeto. Senão, elas são chamadas de propriedades.

Na prática, quase tudo em JavaScript é um objeto — arrays, funções números e até strings - e todos eles possuem
propriedades e métodos.

Exemplo 2.31. Criando um "literal objeto"

.. code-block:: javascript

    var myObject = {
        sayHello : function() {
            console.log('olá');
        },
        myName : 'Rebecca'
    };
    myObject.sayHello(); // loga 'olá'
    console.log(myObject.myName); // loga 'Rebecca'


.. note::

    Ao criar literais objeto, você deve notar que a porção chave de cada par chave-valor pode ser escrito como qualquer
    identificador válido em JavaScript, uma string (entre aspas) ou um número:



.. code-block:: javascript

    var myObject = {
        validIdentifier: 123,
        'some string': 456,
        99999: 789
    };



Literais objeto podem ser extremamente úteis para organização de código; para mais informações leia Using Objects to
Organize Your Code [http://blog.rebeccamurphey.com/2009/10/15/using-objects-toorganize-your-code/] de Rebecca Murphey.



Funções
=======

Funções contêm blocos de código que precisam ser executados repetidamente. As funções podem ter zero ou mais argumentos,
e podem opcionalmente retornar um valor.

Funções podem ser criadas em uma série de formas:

Exemplo 2.32. Declaração de Função

.. code-block:: javascript

    function foo() { /* faz alguma coisa */ }



Exemplo 2.33. Expressão de Função Nomeada

.. code-block:: javascript

    var foo = function() { /* faz alguma coisa */ }



Eu prefiro a expressão de função nomeada para definir o nome da função por algumas questões técnicas
[http://yura.thinkweb2.com/named-function-expressions/]. Você provavelmente verá ambos os métodos usados em código
JavaScript de outros.

Usando funções
--------------

Exemplo 2.34. Uma função simples

.. code-block:: javascript

    var greet = function(person, greeting) {
        var text = greeting + ', ' + person;
        console.log(text);
    };

    greet('Rebecca', 'Olá');



Exemplo 2.35. Uma função que retorna um valor

.. code-block:: javascript

    var greet = function(person, greeting) {
        var text = greeting + ', ' + person;
        return text;
    };
    console.log(greet('Rebecca','Olá'));



Exemplo 2.36. Uma função que retorna outra função

.. code-block:: javascript

    var greet = function(person, greeting) {
        var text = greeting + ', ' + person;
        return function() { console.log(text); };
    };

    var greeting = greet('Rebecca', 'Olá');

    greeting();



Funções anônimas autoexecutáveis
--------------------------------

Um padrão comum em JavaScript é a função anônima autoexecutável. Este padrão cria uma função e a executa imediatamente.
Este padrão é extremamente útil para casos onde você quer evitar poluir o namespace global com seu código --
nenhuma variável declarada dentro da função é visível fora dele.

Exemplo 2.37. Uma função anônima auto-executável

.. code-block:: javascript

    (function(){
        var foo = 'Hello world';
    })();
    console.log(foo); // undefined!



Funções como argumento
----------------------
Em JavaScript, funções são "cidadãos de primeira classe" -- eles podem ser atribuídos a variáveis ou passados para
outras funções como argumentos. Passar funções como argumento é um idioma extremamente comum em jQuery.

Exemplo 2.38. Passando uma função anônima como argumento

.. code-block:: javascript

    var myFn = function(fn) {
        var result = fn();
        console.log(result);
    };

    myFn(function() { return 'olá mundo'; }); // loga 'olá mundo'



Exemplo 2.39. Passando uma função nomeada como argumento

.. code-block:: javascript

    var myFn = function(fn) {
        var result = fn();
        console.log(result);
    };

    var myOtherFn = function() {
        return 'olá mundo';
    };

    myFn(myOtherFn); // loga 'olá mundo'



jQuery básico
=============

$(document).ready()
-------------------

Você não pode manipular a página com segurança até o documento estar "pronto" (ready). O jQuery
detecta o estado de prontidão para você; o código incluido dentro de $(document).ready() somente
irá rodar depois que a página estiver pronta executar o código JavaScript.

Exemplo 3.1. Um bloco $(document).ready()

.. code-block:: javascript

    $(document).ready(function() {
        console.log('pronto!');
    });



Há um atalho para $(document).ready() que você verá algumas vezes; entretando, eu não
recomendo usá-lo se você estiver escrevendo código que pessoas que não têm experiência com jQuery
poderá ver.

Exemplo 3.2. Atalho para $(document).ready()

.. code-block:: javascript

    $(function() {
        console.log('pronto!');
    });



Você ainda pode passar uma função nomeada para $(document).ready() ao invés de passar uma
função anônima.

Exemplo 3.3. Passando uma função nomeada ao invés de uma anônima

.. code-block:: javascript

    function readyFn() {
        // código para executar quando o documento estiver pronto
    }
    $(document).ready(readyFn);



Selecionando elementos
======================

O conceito mais básico do jQuery é "selecionar alguns elementos e fazer alguma coisa com eles." O
jQuery suporta a maioria dos seletores CSS3, assim como alguns seletores não-padrão. Para uma referência
completa de seletores, visite http://api.jquery.com/category/selectors/

A seguir, alguns exemplos de técnicas comuns de seleção.

Exemplo 3.4. Selecionando elementos por ID

.. code-block:: javascript

    $('#myId'); // lembre-se que os IDs devem ser únicos por página

Exemplo 3.5. Selecionando elementos pelo nome da classe

.. code-block:: javascript

    $('div.myClass'); // há um aumento de performance se você especificar o tipo de elemento



Exemplo 3.6. Selecionando elementos por atributo

.. code-block:: javascript

    $('input[name=first_name]'); // cuidado, isto pode ser muito lento

Exemplo 3.7. Selecionando elementos através da composição de seletores CSS

.. code-block:: javascript

    $('#contents ul.people li');

Exemplo 3.8. Pseudo-seletores

.. code-block:: javascript

    $('a.external:first');
    $('tr:odd');
    $('#myForm :input'); // selecione todos os elementos input num formulário
    $('div:visible');
    $('div:gt(2)'); // todos exceto as três primeiras divs
    $('div:animated'); // todas as divs com animação


.. note::

    Quando você usa os pseudos-seletores :visible e :hidden, o jQuery testa a visibilidade
    atual do elemento, não os atributos visibility ou display do CSS - ou seja, ele olha se
    a altura e a largura física do elemento na página são ambas maiores que zero. No entanto, este
    teste não funciona com elementos <tr>; neste caso, o jQuery faz a verificação da propriedade
    display do CSS, e considera um elemento como oculto se sua propriedade display for none.
    Elementos que não forem adicionados no DOM serão sempre considerados ocultos, mesmo que
    o CSS que os afetam torná-los visíveis. (Consulte a seção de Manipulação mais adiante neste
    capítulo para aprender como criar e adicionar elementos ao DOM).



Para referência, aqui está o código que jQuery usa para determinar se um elemento é visível ou
oculto, com os comentários adicionados para maior esclarecimento:

.. code-block:: javascript

    jQuery.expr.filters.hidden = function( elem ) {
        var width = elem.offsetWidth, height = elem.offsetHeight,
        skip = elem.nodeName.toLowerCase() === "tr";
        // o elemento tem 0 de altura e 0 de largura e não é uma <tr>?
        return width === 0 && height === 0 && !skip ?
            // então deve estar escondido
            true :
            // mas se o elemento tiver largura e altura e não for uma <tr>
            width > 0 && height > 0 && !skip ?
                // então deve estar visível
                false :
                // se chegamos aqui, o elemento tem largura
                // e altura, mas também é uma <tr>,
                // então verifica a propriedade display para
                // decidir se ele esta escondido
                jQuery.curCSS(elem, "display") === "none";
    };

    jQuery.expr.filters.visible = function( elem ) {
        return !jQuery.expr.filters.hidden( elem );
    };



Escolhendo seletores
--------------------

Escolher bons seletores é uma forma de melhorar a performance do seu JavaScript. Uma pequena
especificidade - por exemplo, incluir um elemento como div quando selecionar elementos pelo
nome da classe - pode ir por um longo caminho. Geralmente, sempre que você puder dar ao jQuery
alguma dica sobre onde ele pode esperar encontrar o que você estiver procurando, você deve dar.

Por outro lado, muita especificidade pode não ser muito bom. Um seletor como #myTable thead
tr th.special é um desperdício se um seletor como #myTable th.special lhe dará o
que você precisa.

O jQuery oferece muitos seletores baseados em atributo, permitindo que você selecione elementos
baseados no conteúdo de atributos arbitrários usando expressões regulares simplificadas.

.. code-block:: javascript

    // encontre todos elementos <a>s em que o atributo
    // rel termine com "thinger"
    $("a[rel$='thinger']");

Se por um lado estes seletores podem ser bem úteis, eles também podem ser extremamente lerdos
- Uma vez eu escrevi um seletor baseado em atributos que travou minha página por múltiplos
segundos. Sempre que possível, faça suas seleções usando IDs, nomes de classe e nomes de tags.
Quer saber mais? Paul Irish tem uma excelente apresentação sobre melhorar a performance do
JavaScript [http://paulirish.com/perf], com vários slides focando especificamente em performace
de seletores.



Minha seleção contém algum elemento?
------------------------------------

Uma vez que você fez uma seleção, você irá querer saber se há algo para trabalhar com ela. Você talvez
se sinta tentado a fazer algo assim:

.. code-block:: javascript

    if ($('div.foo')) { ... } // Não funciona pra saber se existem elementos em div.foo

Isso não irá funcionar. Quando você faz uma seleção usando $(), um objeto é sempre retornado, e objetos
sempre são tratados como true. Mesmo se sua seleção não tiver nenhum elemento, o código dentro do
if vai executar do mesmo jeito.

Ao invés disso, você precisa testar a propriedade length da seleção, que diz a você quantos elementos
foram selecionados. Se a resposta for 0, a propriedade length será interpretada como falso quando usada
como um valor booleano.

Exemplo 3.9. Testando se uma seleção contém elementos.

.. code-block:: javascript

    if ($('div.foo').length) { ... } // Dessa forma retorna se o elemento div.foo tem elementos



Salvando seleções
-----------------

Toda vez que você faz uma seleção, um monte de código é executado, e o jQuery não faz caching de
seleções para você. Se você fez uma seleção que você talvez precise fazer novamente, você deve salvar a
seleção numa variável ao invés de fazer a seleção várias vezes.

Exemplo 3.10. Armazenando seleções em variáveis

.. code-block:: javascript

    var $divs = $('div');

.. note::

    No Exemplo 3.10, “Armazenando seleções em variáveis”, o nome da variável começa com
    um sinal de dólar. Ao invés de outras linguagens, não há nada especial sobre o sinal de dólar
    em JavaScript -- é apenas outro caracter. Nós o usamos para indicar que a varíavel contém
    um objeto jQuery. Esta prática -- um tipo de Notação Húngara [http://en.wikipedia.org/wiki/
    Hungarian_notation] -- é meramente uma convenção, e não é obrigatória.

Uma vez que você armazenou sua seleção, você pode chamar os métodos do jQuery na variável que você
armazenou, da mesma forma que você faria na seleção original.

.. note::

    Uma seleção somente obtém os elementos que estão na página quando você faz a seleção. Se
    você adicionar elementos na página depois, você terá que repetir a seleção ou então adicioná-la à
    seleção armazenada na variável. Seleções armazenadas não atualizam automaticamente quando
    o DOM muda.



Refinando & Filtrando Seleções
------------------------------

Algumas vezes você tem uma seleção que contém mais do que você quer; neste caso, você talvez queira
refinar sua seleção. O jQuery oferece vários métodos para você obter exatamente o que precisa.

Exemplo 3.11. Refinando seleções

.. code-block:: javascript

    $('div.foo').has('p'); // o elemento div.foo que contém <p>'s
    $('h1').not('.bar'); // elementos h1 que não têm a classe bar
    $('ul li').filter('.current'); // itens de listas não-ordenadas com a classe current
    $('ul li').first(); // somente o primeiro item da lista não ordenada
    $('ul li').eq(5); // o sexto item da lista



Seletores relacionados à formulários
------------------------------------

O jQuery oferece vários pseudo-seletores que lhe ajudam a encontrar elementos nos seus formulários; estes
são especialmente úteis porque pode ser difícil distinguir entre elementos form baseados no seu estado ou
tipo usando seletores CSS padrão.



    :button Seleciona elementos do tipo <button> e elementos com type="button"

    :checkbox Seleciona inputs com type="checkbox"

    :checked Seleciona inputs selecionados

    :disabled Seleciona elementos de formulário desabilitados

    :enabled Seleciona elementos de formulário habilitados

    :file Seleciona inputs com type="file"

    :image Seleciona inputs com type="image"

    :input Seleciona <input>, <textarea>, e elementos <select>

    :password Selecionam inputs com type="password"

    :radio Selecionam inputs com type="radio"

    :reset Selecionam inputs com type="reset"

    :selected Seleciona inputs que estão selecionados

    :submit Seleciona inputs com type="submit"

    :text Seleciona inputs com type="text"



Exemplo 3.12. Usando pseudo-seletores relacionados à formulários

.. code-block:: javascript

    $("#myForm :input'); // obtém todos os elementos que aceitam entrada de dados



Trabalhando com seleções
------------------------

Uma vez que você tem uma seleção, você pode chamar métodos nela. Métodos geralmente vêm em duas
formas diferentes: getters e setters. Getters retornam uma propriedade do primeiro elemento selecionado;
setters ajustam (setam) uma propriedade em todos os elementos selecionados

Encadeamento
++++++++++++

Se você chamar um método numa seleção e este método retornar um objeto jQuery, você pode continuar
a chamar métodos do jQuery sem precisar pausar com um ponto-e-vírgula.

Exemplo 3.13. Encadeamento

.. code-block:: javascript

    $('#content').find('h3').eq(2).html('o novo texto do terceiro h3!');

Se você estiver escrevendo uma cadeia que inclui vários passos, você (e a pessoa que virá depois de você)
talvez ache seu código mais legível se você quebrar o código em várias linhas.

Exemplo 3.14. Formatando código encadeado

.. code-block:: javascript

    $('#content')
        .find('h3')
        .eq(2)
        .html('novo texto do terceiro h3!');

Se você mudar sua seleção no meio de uma cadeia, o jQuery provê o método $.fn.end para você voltar
para sua seleção original.

Exemplo 3.15. Restaurando sua seleção original usando $.fn.end

.. code-block:: javascript

    $('#content')
        .find('h3')
        .eq(2)
        .html('new text for the third h3!')
        .end() // restaura a seleção para todos h3 em #context
        .eq(0)
        .html('novo texto para o primeiro h3!');


.. note::

    Encadeamento é um recurso extraordinariamente poderoso, e muitas bibliotecas adotaram-no
    desde que o jQuery o tornou popular. Entretando, deve ser usado com cuidado. Encadeamentos
    extensos podem deixar o código extremamente difícil de debugar ou modificar. Não há uma regra
    que diz o quão grande uma cadeia deve ser -- mas saiba que é fácil fazer bagunça



Getters & Setters
-----------------

O jQuery “sobrecarrega” seus métodos, então o método usado para setar um valor geralmente tem o mesmo
nome do método usado para obter um valor. [Definition: Quando um método é usado para setar um valor,
ele é chamado de setter]. [Definition: Quando um método é usado para pegar (ou ler) um valor, ele é
chamado de getter]. Os setters afetam todos os elementos na seleção; getters obtêm o valor requisitado
somente do primeiro elemento na seleção.

Exemplo 3.16. O método $.fn.html usado como setter

.. code-block:: javascript

    $('h1').html('olá mundo');

Exemplo 3.17. O método html usado como getter

.. code-block:: javascript

    $('h1').html();

Os setters retornam um objeto jQuery, permitindo que você continue chamando métodos jQuery na sua
seleção; getters retornam o que eles foram pedidos para retornar, o que significa que você não pode
continuar chamando métodos jQuery no valor retornado pelo getter.



CSS, Styling, & Dimensões
=========================

O jQuery possui uma forma bem prática para pegar e setar propriedades CSS dos elementos.

.. note::

    Propriedades CSS que normalmente incluem um hífen, precisam ser acessadas no estilo
    camel case em JavaScript. Por exemplo, a propriedade CSS font-size é expressada como
    fontSize em JavaScript.



Exemplo 3.18. Pegando propriedades CSS

.. code-block:: javascript

    $('h1').css('fontSize'); // retorna uma string, como "19px"

Exemplo 3.19. Setando propriedades CSS

.. code-block:: javascript

    $('h1').css('fontSize', '100px'); // setando uma propriedade individual
    $('h1').css({ 'fontSize' : '100px', 'color' : 'red' }); // setando múltiplas propriedades

Note o estilo do argumento que usamos na segunda linha -- é um objeto que contém múltiplas
propriedades. Este é um jeito comum de passar múltiplos argumentos para uma função, e muitos setters
do jQuery aceitam objetos para setar múltiplos valores de uma só vez.

Usando classes do CSS para estilos
----------------------------------

Como um getter, o método $.fn.css é útil; Entretanto, ele geralmente deve ser evitado como um setter
em código de produção, pois você não quer informação de apresentação no seu JavaScript. Ao invés disso,
escreva regras CSS para classes que descrevam os vários estados visuais, e então mude a classe no elemento
que você quer afetar.

Exemplo 3.20. Trabalhando com classes

.. code-block:: javascript

    var $h1 = $('h1');
    $h1.addClass('big');
    $h1.removeClass('big');
    $h1.toggleClass('big');
    if ($h1.hasClass('big')) { ... }

Classes também podem ser úteis para armazenar informações de estado de um elemento, como indicar se
um elemento está selecionado, por exemplo.

Dimensões
---------

O jQuery oferece uma variedade de métodos para obter e modificar informações sobre dimensões e
posições de um elemento.

O código do Exemplo 3.21, “Métodos básicos de dimensões” é somente uma introdução muito curta sobre
as funcionalidades de dimensões do jQuery; para detalhes completos sobre os métodos de dimensão do
jQuery, visite http://api.jquery.com/category/dimensions/.

Exemplo 3.21. Métodos básicos de dimensões

.. code-block:: javascript

    $('h1').width('50px'); // seta a largura de todos os elementos h1
    $('h1').width(); // obtém a largura do primeiro h1
    $('h1').height('50px'); // seta a altura de todos os elementos h1
    $('h1').height(); // obtém a altura do primeiro h1
    $('h1').position(); // retorna um objeto contendo informações
    // sobre a posição do primeiro h1 relativo a seu pai



Atributos
=========

Atributos de elementos podem conter informações úteis para sua aplicação, então é importante saber como
setá-los e obtê-los.

O método $.fn.attr atua como getter e setter . Assim como os métodos $.fn.css , $.fn.attr
atuando como um setter, podem aceitar tanto uma chave e um valor ou um objeto contendo um ou mais
pares chave/valor.

Exemplo 3.22. Setting attributes

.. code-block:: javascript

    $('a').attr('href', 'todosMeusHrefsSaoOMesmoAgora.html');
    $('a').attr({
        'title' : 'todos os títulos são os mesmos também!',
        'href' : 'algoNovo.html'
    });

Agora, nós quebramos o objeto em múltiplas linhas. Lembre-se, espaços não importam em JavaScript,
então você deve se sentir livre para usa-lo do jeito que quiser para fazer seu código ficar mais legível!
Depois, você pode usar uma ferramenta de minificação para remover espaços desnecessários para seu
código de produção.

Exemplo 3.23. Getting attributes

.. code-block:: javascript

    $('a').attr('href'); // retorna o href do primeiro elemento <a> do documento



Travessia
=========

Uma vez que você tem uma seleção do jQuery, você pode encontrar outros elementos usando sua seleção
como ponto de início.
Para documentação completa dos métodos de travessia do jQuery, visite http://api.jquery.com/category/traversing/

.. note::
    Seja cuidadoso com travessias de longas distâncias nos seus documentos -- travessias complexas
    torna imperativo que a estrutura do seu documento permaneça a mesma, uma dificuldade à
    estabilidade, mesmo se você for o responsável por criar toda a aplicação desde o servidor até
    o cliente. Travessias de um ou dois passos são legais, mas geralmente você irá querer evitar
    travessias que levem você de um container para outro.



Exemplo 3.24. Movendo pelo DOM usando métodos de travessia.

.. code-block:: javascript

    $('h1').next('p');
    $('div:visible').parent();
    $('input[name=first_name]').closest('form');
    $('#myList').children();
    $('li.selected').siblings();

Você também pode iterar sobre uma seleção usando $.fn.each. Este método itera sobre todos os
elementos numa seleção e executar uma função para cada um. A função recebe como argumento um índice
com o elemento atual e com o próprio elemento DOM. Dentro da função, o elemento DOM também está
disponível como this por padrão.

Exemplo 3.25. Iterando sobre uma seleção

.. code-block:: javascript

    $('#myList li').each(function(idx, el) {
        console.log(
            'O elemento ' + idx +
            'tem o seguinte html: ' +
            $(el).html()
        );
    });



Manipulando elementos
=====================

Uma vez que você fez uma seleção, a diversão começa. Você pode mudar, mover, remover e clonar
elementos. Você ainda pode criar novos elementos através de uma sintaxe simples.

Para uma referência completa dos métodos de manipulação do jQuery, visite http://api.jquery.com/category/manipulation/

Obtendo e setando informações sobre elementos
---------------------------------------------

Há um certo número de formas que você pode mudar um elemento existente. Dentre as tarefas mais comuns
que você irá fazer é mudar o inner HTML ou o atributo de um elemento. O jQuery oferece métodos simples
e cross-navegador para estes tipos de manipulação. Você ainda pode obter informações sobre elementos
usando muitos dos mesmos métodos nas suas formas de getter. Nós veremos exemplos destes métodos
durante esta seção, mas especificamente, aqui vão alguns poucos métodos que você pode usar para obter
e setar informação sobre elementos.

.. note::

    Mudar coisas em elementos é trivial, mas lembre-se que a mudança irá afetar todos os elementos
    na seleção, então se você quiser mudar um elemento, esteja certo de especificá-lo em sua seleção
    antes de chamar o método setter

.. note::

    Quando os métodos atuam como getters, eles geralmente só trabalham no primeiro elemento
    da seleção e eles não retornam um objeto jQuery, portanto você não pode encadear métodos
    adicionais a eles. Uma exceção notável é $.fn.text; como mencionado acima, ele obtém o
    texto para todos os elementos da seleção



$.fn.html Obtém ou seta o conteúdo html.

$.fn.text Obtém ou seta os conteúdos de texto; HTML será removido.

$.fn.attr Obtém ou seta o valor do atributo fornecido.

$.fn.width Obtém ou seta a largura em pixels do primeiro elemento na seleção como um inteiro.

$.fn.height Obtém ou seta a altura em pixels do primeiro elemento na seleção.

$.fn.position Obtém um objeto com a informação de posição do primeiro elemento na seleção,

relativo a seu primeiro ancestral (pai). Este é somente um getter.

$.fn.val Obtém ou seta o valor de elementos de formulários.


Exemplo 3.26. Mudando o HTML de um elemento.

.. code-block:: javascript

    $('#myDiv p:first')
        .html('Primeiro parágrafo <strong>novo</strong>!');

Movendo, copiando e removendo elementos
---------------------------------------

Há uma variedade de formas de mover elementos pelo DOM; geralmente, há duas abordagens:

    * Coloque o(s) elemento(s) selecionado(s) relativo à outro elemento
    * Coloque um elemento relativo ao(s) elemento(s) selecionado(s)

Por exemplo, o jQuery fornece $.fn.insertAfter e $.fn.after. O método
$.fn.insertAfter coloca o(s) elemento(s) selecionado(s) depois do elemento que você passou como
argumento; o método $.fn.after coloca o elemento passado como argumento depois do elemento
selecionado.Vários outros métodos seguem este padrão: $.fn.insertBefore e $.fn.before;
$.fn.appendTo e $.fn.append; e $.fn.prependTo e $.fn.prepend.

O método que faz mais sentido pra você dependerá de quais elementos você já selecionou e quais
você precisará armazenar uma referência para os elementos que você está adicionando na página. Se
você precisar armazenar uma referência, você sempre irá querer fazer pela primeira forma -- colocando
os elementos selecionados relativos à outro elemento -- de forma que ele retorne o(s) elemento(s)
que você está colocando. Neste caso, os métodos $.fn.insertAfter, $.fn.insertBefore,
$.fn.appendTo, e $.fn.prependTo serão suas ferramentas para escolha.

Exemplo 3.27. Movendo elementos usando outras formas

.. code-block:: javascript

    // faz o primeiro item da lista se tornar o último
    var $li = $('#myList li:first').appendTo('#myList');
    // outra forma de resolver o mesmo problema
    $('#myList').append($('#myList li:first'));
    // perceba que não tem como acessar o item
    // da lista que movemos, pois ele retorna
    // a própria lista

Clonando elementos
++++++++++++++++++

Quando você usa métodos como $.fn.appendTo, você está movendo o elemento; porém, algumas vezes
você irá querer fazer uma cópia do elemento. Neste caso, você precisará usar $.fn.clone primeiro.

Exemplo 3.28. Fazendo uma cópia de um elemento

.. code-block:: javascript

    // copia o primeiro item da lista para o fim
    $('#myList li:first').clone().appendTo('#myList');

.. note::

    Se você precisa copiar dados e eventos relacionados, esteja certo de passar true como um
    argumento para $.fn.clone.

Removendo elementos
+++++++++++++++++++

Há duas formas de remover elementos da página: $.fn.remove e $.fn.detach. Você irá usar
$.fn.remove quando você quiser remover a seleção permanentemente da página; enquanto o método
retorna os elementos removidos, estes elementos não terão seus eventos e dados associados a ele se você
retorna-los à página.

Se você precisa que os dados e eventos persistam, você irá usar $.fn.detach . Da mesma forma que
$.fn.remove, ele retorna uma seleção, mas também mantém os dados e os eventos associados com a
seleção para que você possa restaurar a seleção para a página no futuro.

.. note::

    O método $.fn.detach é extremamente útil se você estiver fazendo uma manipulação pesada
    à um elemento. Neste caso, é bom aplicar um $.fn.detach no elemento da página, trabalhar
    no seu próprio código, e então restaura-lo à página quando você terminar. Isto evita que você faça
    "toques ao DOM" caros enquanto mantém os dados e eventos do elemento.

Se você quer deixar um elemento na página mas simplesmente quer remover seu conteúdo, você pode usar
$.fn.empty para retirar o innerHTML do elemento.

Criando novos elementos
-----------------------

O jQuery oferece uma forma elegante e trivial para criar novos elementos usando o mesmo método $()
que você usava para seleções.

Exemplo 3.29. Criando novos elementos

.. code-block:: javascript

    $('<p>Este é um novo parágrafo</p>');
    $('<li class="new">novo item de lista</li>');

Exemplo 3.30. Criando um novo elemento com um objeto atributo

.. code-block:: javascript

    $('<a/>', {
        html : 'Este é um link <strong>new</strong>',
        'class' : 'new',
        href : 'foo.html'
    });

Perceba que no objeto de atributos nós incluimos como segundo argumento a propriedade class entre
aspas, enquanto as propriedades html e href não. Geralmente, nomes de propriedades não precisam estar
entre aspas a não ser que elas sejam palavras reservadas (como a class neste caso)

Quando você cria um novo elemento, ele não é adicionado imediatamente à página. Há várias formas de
adicionar um elemento à página uma vez que ele esteja criado.

Exemplo 3.31. Inserindo um novo elemento na página

.. code-block:: javascript

    var $myNewElement = $('<p>Novo elemento</p>');
    $myNewElement.appendTo('#content');
    $myNewElement.insertAfter('ul:last'); // isto irá remover p de #content!
    $('ul').last().after($myNewElement.clone()); // clona o p, portanto temos 2 agora

Estritamente falando, você não precisa armazenar o elemento criado numa variável -- você pode
simplesmente chamar o método para adicioná-lo diretamente depois do $(). Entretanto, a maior parte
do tempo você precisará de uma referência ao elemento que você adicionou para que você não o selecione
depois.

Você ainda pode criar um elemento ao mesmo tempo que você o adiciona à página, mas note que neste
caso você não obtém a referência do novo objeto criado.

Exemplo 3.32. Criando e adicionando um elemento à página ao mesmo tempo

.. code-block:: javascript

    $('ul').append('<li>item de lista</li>');

.. note::

    A sintaxe para adicionar novos elementos à página é tão fácil que é tentador esquecer que há um
    enorme custo de performance por adicionar ao DOM repetidas vezes. Se você está adicionando
    muitos elementos ao mesmo container, você irá concatenar todo html numa única string e então
    adicionar a string ao container ao invés de ir adicionando um elemento de cada vez. Você pode usar
    um array para colocar todas os pedaços juntos e então aplicar um join nele em uma única
    string para adicionar ao container.

.. code-block:: javascript

    var myItems = [], $myList = $('#myList');
    for (var i=0; i<100; i++) {
        myItems.push('<li>item ' + i + '</li>');
    }
    $myList.append(myItems.join(''));



Manipulando atributos
---------------------

Os recursos de manipulação de atributos do jQuery são muitos. Mudanças básicas são simples, mas o
método $.fn.attr também permite manipulações mais complexas.

Exemplo 3.33. Manipulando um único atributo

.. code-block:: javascript

    $('#myDiv a:first').attr('href', 'novoDestino.html');

Exemplo 3.34. Manipulando múltiplos atributos

.. code-block:: javascript

    $('#myDiv a:first').attr({
        href : 'novoDestino.html',
        rel : 'super-special'
    });

Exemplo 3.35. Usando uma função para determinar um novo valor de atributo

.. code-block:: javascript

    $('#myDiv a:first').attr({
        rel : 'super-special',
        href : function() {
            return '/new/' + $(this).attr('href');
        }
    });
    $('#myDiv a:first').attr('href', function() {
        return '/new/' + $(this).attr('href');
    });
