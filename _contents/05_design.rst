************
Design (16h)
************

Psicologia das cores
====================

Whether you’re defusing a ticking time bomb, or trying to design a decent-​​looking site, if you choose the wrong color — 
you’re doomed. Okay, so the wrong color selection for a client’s site might not be the death of you, but it could
curtail your budding career as a web designer. Choosing colors is no simple matter. There are aesthetic, identity, and
usability considerations to take into account. And, to make matters worse, most modern displays can render more than
sixteen million colors. That’s an infinite number of horrible color combinations just waiting to happen!

Se você está desarmar uma bomba-relógio, ou tentando criar um site decente, se você escolher a cor errada - você está
condenado. Ok, então a seleção de cor errada para site de um cliente pode não ser a sua morte, mas poderia reduzir a sua
carreira incipiente como web designer. Escolher cores não é uma questão simples. Existem considerações de identidade,
estética e usabilidade a ter em conta. E, para piorar a situação, a maioria dos monitores modernos pode render mais de
16 milhões de cores. Isso é um número infinito de combinações de cores horríveis apenas esperando para acontecer!

Fortunately, there’s no need to be a swatchbook-​​carrying color consultant to make good color choices. A wealth of
knowledge is available, from touchy-​​feely (as I like to call them) psychological guidelines to tried-​​and-​​true color
theories that will help you make the right choices when it comes to color.

Felizmente, não há necessidade de ser um consultor de cor mostruário transporte para fazer escolhas de cores boas. A
riqueza de conhecimento está disponível, a partir de melosas (como eu gosto de chamá-los) orientações psicológicas para
teorias da cor tentou-e-verdadeiro que ajudarão você a fazer as escolhas certas quando se trata de cor.

Color psychology is a field of study that’s devoted to analyzing the emotional and behavioral effects produced by colors
and color combinations. Ecommerce website owners want to know which color will make their website visitors spend more
money. Home decorators are after a color that will transform a bedroom into a tranquil Zen retreat. Fast-​​food restaurant
owners are dying to know which color combinations will make you want to super-​​size your meal. As you can imagine, color
psychology is big business.

Psicologia das cores é um campo de estudo que se dedica a analisar os efeitos emocionais e comportamentais produzidos
por cores e combinações de cores. Proprietários de sites de comércio eletrônico quer saber qual cor vai fazer seus
visitantes do site gastar mais dinheiro. Decoradores estão atrás de uma cor que irá transformar um quarto em um retiro
tranquilo zen. Os proprietários de restaurantes de fast-food estão morrendo para saber quais combinações de cores vai
fazer você querer super-tamanho da sua refeição. Como você pode imaginar, psicologia das cores é um grande negócio.

Although it’s important to know how your color choices might affect the masses, the idea that there’s a single, unified,
psychological response to specific colors is spurious. Many of the responses that color psychologists accredit to
certain colors are rooted in individual experience. It’s also interesting to note that many cultures have completely
different associations with, and interpretations of, colors. With those caveats in mind, let’s explore some general
psychological associations that the majority of people in Western cultures have in response to specific colors.

Embora seja importante saber como as opções de cores pode afetar as massas, a idéia de que não há uma única resposta
unificada, psicológico a cores específicas é espúrio. Muitas das respostas que os psicólogos cor credenciam a
determinadas cores estão enraizadas na experiência individual. É também interessante notar que muitas culturas têm
associações completamente diferentes, com e interpretações, cores. Com essas advertências em mente, vamos explorar
algumas associações gerais psicológicos que a maioria das pessoas nas culturas ocidentais têm em resposta a cores
específicas.



Associações das cores
---------------------

Describing the emotional connections that people can have with colors can be a very hippy-​​esque topic. If you find that
hard to believe, just head over to your favorite online music store and sample some tracks from Colors by Ken Nordine.
Although most designers will stop short at relying solely on the supposed meanings, characteristics, and personalities
of specific colors, it’s still handy to have an understanding of the emotional attributes of some of the main color
groups.

Descrevendo as conexões emocionais que as pessoas podem ter com as cores podem ser um tema muito hippie-esque. Se você
achar isso difícil de acreditar, apenas a cabeça para a sua loja de música online favorito e amostra de algumas faixas
de cores por Ken Nordine. Embora a maioria dos designers vão fechar-se na baseando-se unicamente sobre os significados
supostamente, características e personalidades de cores específicas, ainda é útil para ter uma compreensão dos atributos
emocionais de alguns dos grupos de cores principais.



Vermelho
++++++++

The color red has a reputation for stimulating adrenaline and blood pressure. Along with those physiological effects,
red is also known to increase human metabolism; it’s an exciting, dramatic, and rich color. Red is also a color of
passion. Nothing says love like painting a wall bright red on Valentine’s Day for your sweetheart, as seen in
Figure 1.1, “Red, the color of affection (two gallons of it!)”. The darker shades of red, such as burgundy and maroon,
have a rich, indulgent feeling about them — in fact, they can be quite hoity-​​toity. Think about these colors when
designing anything for wine enthusiasts or connoisseurs of fine living. The more earthy, brownish shades of red are
associated with the season of autumn and harvest.

A cor vermelha tem a reputação de estimular a adrenalina e pressão arterial. Junto com os efeitos fisiológicos, vermelho
também é conhecido por aumentar o metabolismo humano, é uma cor estimulante, dramático e rico. O vermelho é também a cor
da paixão. Nada diz o amor como pintar uma parede vermelha brilhante no Dia dos Namorados para o seu amor, como visto na
Figura 1.1, "Vermelho, a cor de afeto (dois galões de-lo!)". Os tons mais escuros de vermelho, como bordô e marrom, tem
um sentimento, rico indulgente com eles - na verdade, eles podem ser muito pretensioso. Pense sobre estas cores ao
projetar qualquer coisa para os entusiastas do vinho ou conhecedores do bem viver. Os mais terrosas, tons acastanhados
de vermelho estão associados com a época do outono e colheita.

.. figure:: ../_images/color-valentine.jpg
    :align: center

    Figura 1.1. Vermelho, a cor da afeição (dois galões pra ele!)



Laranja
+++++++

Like red, orange is a very active and energetic color, though it doesn’t evoke the passion that red can. Instead, orange
is thought to promote happiness, and represents sunshine, enthusiasm, and creativity. Orange is a more informal and less
corporate-​​feeling color than red, which is perhaps a reason why the designers behind the location-​​based service Gowalla
chose it for their logo. Because orange doesn’t show up often in nature, it tends to jump out at us when we see it. For
that reason, it’s often used for life jackets, road cones, and hunting vests. Since orange also stimulates metabolism and
appetite, it’s a great color for promoting food and cooking. That’s probably why the picture of a tangerine in
Figure 1.2, “Orange you glad I didn’t say banana?” is making you hungry, even if you don’t like citrus fruits.

Como o vermelho, o laranja é uma cor muito ativo e enérgico, embora não evocam a paixão que o vermelho pode. Em vez
disso, a laranja é pensado para promover a felicidade, e representa sol, entusiasmo e criatividade. A laranja é uma mais
informal e menos cor-sentimento corporativo do que vermelho, que é, talvez, uma razão pela qual os designers por trás do
serviço baseado em localização Gowalla escolheu para seu logotipo. Porque laranja não aparece muitas vezes na natureza,
tende a saltar para fora de nós quando vê-lo. Por esse motivo, é muitas vezes usada para coletes, cones da estrada e
coletes à prova de caça. Desde laranja também estimula o metabolismo e do apetite, é uma cor excelente para a promoção
de alimentos e cozinhar. Isso é provavelmente porque a imagem de uma tangerina na Figura 1.2, "Laranja você feliz Eu não
disse banana?" Está fazendo com fome, mesmo se você não gosta de frutas cítricas.

.. figure:: ../_images/color-orange.jpg
    :align: center

    Figura 1.2. Belas laranjas.



Amarelo
+++++++

Like orange, yellow is an active color, and being highly visible, it’s often used for taxicabs and caution signs. It’s
also associated with happiness and, as Figure 1.3, “Yellow, the color of smileys” illustrates, is the signature color of
smileys. The original orange and lemon-​​lime flavors of the sports energy drink Gatorade are still the best-​​selling of
the brand’s products; this is likely due, in part at least, to the energetic characteristics associated with the colors
orange and yellow.

Como laranja, o amarelo é uma cor ativa, e sendo altamente visível, é muitas vezes utilizado para táxis e sinais de
aviso. Também está associado à felicidade e, como a Figura 1.3, "amarelo, a cor do smileys" ilustra, é a cor da
assinatura de smileys. Os sabores originais laranja e lima-limão da bebida energética Gatorade esportes ainda são a
melhor-venda de produtos da marca, o que é provável, devido, pelo menos em parte, às características energéticas
associadas com as cores laranja e amarelo.

.. figure:: ../_images/color-smileys.jpg
    :align: center

    Figura 1.3. Amarelo, a cor do sorriso



Verde
+++++

Green is most commonly associated with nature. It’s a soothing color that symbolizes growth, freshness, and hope.
There’s little doubt why the color has been so closely tied with environmental protection. Visually, green is much
easier on the eyes, and far less dynamic than yellow, orange, or red. Although many website designs using green appeal
to visitors’ sense of nature, green is a versatile color that can also represent wealth, stability, and education. When
bright green is set against a black background, it really pops — giving the design a techy feel. For me, it brings back
memories of my first computer, a trusty old Apple IIe. This was the inspiration for the MailChimp loading screen I
designed recently, shown in Figure 1.4, “ASCII version of Freddie Von Chimpenheimer IV”.

Verde é mais comumente associado com a natureza. É uma cor calmante que simboliza o crescimento, frescor e esperança.
Há pouca dúvida de por que a cor tem sido tão intimamente ligada com a proteção ambiental. Visualmente, o verde é muito
mais fácil sobre os olhos, e muito menos dinâmico do que o amarelo, laranja ou vermelho. Embora os projetos muitos site
usando apelo verde a sensação visitantes da natureza, o verde é uma cor versátil que também pode representar a riqueza,
estabilidade e educação. Quando verde brilhante é contra um fundo preto, ele realmente aparece - dar ao projeto uma
sensação techy. Para mim, ela traz de volta memórias de meu primeiro computador, um bom e velho Apple IIe. Esta foi a
inspiração para a tela de carregamento MailChimp eu projetei recentemente, mostrado na Figura 1.4, "versão ASCII de
Freddie Von Chimpenheimer IV".

.. figure:: ../_images/color-green.jpg
    :align: center

    Figura 1.4. Versão ASCII de Freddie Von Chimpenheimer IV



Azul
++++

When I was a kid, my favorite color was blue. Not just any blue, but cerulean blue from Crayola crayons. While most kids
are less particular, blue is often cited as the universally favorite color. On the touchy-​​feely level, blue symbolizes
openness, intelligence, and faith, and has been found to have calming effects. On the other hand, blue has also has been
found to reduce appetite. This is probably due in part to the rarity of blue in real food. Aside from blueberries, how
many naturally blue foods can you count? Blue, it would seem, is excluded from nature’s appetite-​​inducing palette;
therefore, it’s less than ideal for promoting food products.

Quando eu era criança, minha cor favorita era azul. Não é qualquer azul, mas azul cerúleo de pastéis de Crayola.
Enquanto a maioria das crianças são menos especial, azul é frequentemente citado como a cor universalmente favorito.
No nível melosas, azul simboliza a abertura, inteligência e fé, e foi encontrado para ter efeitos calmantes. Por outro
lado, o azul também foi encontrada para reduzir o apetite. Este é, provavelmente, devido em parte à raridade de azul na
comida de verdade. Além de mirtilos, quantos alimentos naturalmente azuis você pode contar? Azul, ao que parece, está
excluído da paleta da natureza apetite induzindo, portanto, é menos do que ideal para promoção de produtos alimentares.

In addition, blue is sometimes seen as a symbol of bad luck and trouble. This emotional color connection is evident in
blues music, as well as in the paintings of Picasso’s depression-​​induced “blue period.” It’s not all about unnatural
food colors and melancholy forms of art, though; blue has universal appeal because of its association with the sky and
the sea. For me, the presence of blue in the stacked stones image in Figure 1.5, “Calming stones, sky, and sea” makes me
feel more at ease.

Além disso, o azul é às vezes visto como um símbolo de má sorte e problemas. Esta conexão cor emocional é evidente na
música blues, bem como nas pinturas de depressão induzida por Picasso Não é tudo sobre cores de alimento não-naturais e
formas de melancolia de arte, apesar de "período azul"., Azul tem um apelo universal por causa de sua associação com o
céu eo mar. Para mim, a presença de azul na imagem pedras empilhadas na Figura 1.5, "pedras calmante, céu e mar" faz-me
sentir mais à vontade.

.. figure:: ../_images/color-blue.jpg
    :align: center

    Figura 1.5. Pedras calmas, céu e mar.

This visual connection makes blue an obvious choice for websites associated with airlines, air conditioning, pool
filters, and cruises. Have you ever noticed that blue is the primary color in the logos of IBM, Dell, HP, and Microsoft?
That’s because blue also conveys a sense of stability and clarity of purpose … that is, until you’ve experienced the
blue screen of death!

Essa conexão visual faz uma escolha óbvia para azul sites associados com as companhias aéreas, ar condicionado, filtros
de piscina, e cruzeiros. Você já notou que o azul é a cor primária nos logotipos da IBM, Dell, HP e Microsoft? Isso
porque azul também transmite uma sensação de estabilidade e clareza de propósito ... isto é, até que você tenha
experimentado a tela azul da morte!



Roxo
++++

Historically, the color purple has been associated with royalty and power, as it is on the postage stamp in Figure 1.6,
“Purple coat of arms on a Norwegian postage stamp”. The secret behind purple’s prestigious past has to do with the
difficulty of producing the dye needed to create purple garments. To this day, purple still represents wealth and
extravagance. That extravagance is carried over into nature. Purple is most often connected with flowers, gemstones,
and wine. It balances the stimulation of red and the calming effects of blue. According to Patrick McNeil, author of
The Web Designer’s Idea Book,1 purple is one of the least-​​used colors in web design. He explains that finding good
examples of website designs featuring purple was so hard that he almost had to cut the section from his book. If you’re
trying to create a website design that stands out from the crowd, think about using a rich shade of purple.

Historicamente, a cor púrpura tem sido associada com a realeza e poder, como é no selo postal na Figura 1.6,
"casaco roxo de armas em um selo postal norueguês". O segredo por trás do passado de prestígio roxo tem a ver com a
dificuldade de produzir o corante necessário para criar roupas roxas. Para este dia, o roxo ainda representa riqueza e
extravagância. Essa extravagância é transportada para a natureza. O roxo é mais frequentemente relacionada com flores,
pedras preciosas, e vinho. Ele equilibra a estimulação de vermelho e os efeitos calmantes de azul. De acordo com
Patrick McNeil, autor do livro O Web Designer da Idéia, um roxo é uma das cores menos usadas em web design. Ele explica
que encontrar bons exemplos de projetos site com roxo era tão forte que ele quase teve que cortar a seção de seu livro.
Se você está tentando criar um design de site que se destaca da multidão, pense em usar uma máscara rica de roxo.

.. figure:: ../_images/color-purple.jpg
    :align: center

    Figure 1.6. Casaco roxo de armas em um selo postal norueguês



Branco
++++++

You might think there’s nothing special about the color of the wind turbines in Figure 1.7, “These wind turbines might
be white, but they’re also green”, but the use of white actually helps promote the idea that this is clean power. In
Western cultures, white is considered to be the color of perfection, light, and purity. This is why crisp white sheets
are used in detergent commercials, and why a bride wears a white dress on her wedding day. For an idea of how ingrained
the meaning of white is in our culture, read the poem Design by Robert Frost.2 In it, Frost symbolically contradicts our
associations by using white to represent death and darkness. Interestingly, in Chinese culture, white is a color
traditionally associated with death and mourning. Such cultural distinctions should serve as a reminder to research the
color associations of your target audience, as they may vary greatly from your own perception.

Você pode pensar que não há nada de especial sobre a cor das turbinas eólicas em Figura 1.7, "Estas turbinas eólicas
pode ser branco, mas eles também são verdes", mas o uso do branco, na verdade, ajuda a promover a idéia de que este é
energia limpa. Nas culturas ocidentais, o branco é considerado a cor da perfeição, pureza, luz e. É por isso que lençóis
brancos são usados ​​em comerciais de detergente, e por que uma noiva usa um vestido branco no dia de seu casamento. Para
ter uma ideia de como arraigado o sentido de branco está na nossa cultura, ler o poema de Robert Projeto Frost.2 Nele,
Frost simbolicamente contradiz nossas associações usando branco para representar a morte ea escuridão. Curiosamente, na
cultura chinesa, o branco é a cor tradicionalmente associada à morte e luto. Tais distinções culturais devem servir como
um lembrete para pesquisar as associações de cores de seu público-alvo, pois eles podem variar muito de sua própria
percepção.

In design, white is often overlooked because it’s the default background color. Don’t be afraid to shake it up, though.
Try using a dark background with white text, or put a white background block on an off-​​white canvas to make it pop.
Using colors in unexpected ways can make a bold statement.

No projeto, o branco é muitas vezes esquecido, porque é a cor de fundo padrão. Não tenha medo de sacudir, embora. Tente
usar um fundo escuro com texto branco, ou colocar um bloco de fundo branco em uma tela de off-white para torná-lo pop.
Usando cores de maneiras inesperadas pode fazer uma declaração ousada.

.. figure:: ../_images/color-white.jpg
    :align: center

    Figure 1.7. These wind turbines might be white, but they’re also green



Black
+++++

Although black often suffers from negative connotations such as death and evil, it can also be a color of power,
elegance, and strength, depending on how it’s used. If you’re considering using a particular color and are wondering
what the associations are for that color, just ask yourself, “What are the first three things that come to mind when I
think about this color?” When I think about black, for instance, I think about Johnny Cash, tuxedos, and Batman. When I
think about Johnny Cash, his dark clothing, deep voice, and sorrowful songs give a tangible meaning to the mental
associations I perceive between the man and the color.

.. figure:: ../_images/color-black.jpg
    :align: center

    Figure 1.8. Black, a color that represents power, elegance, and in this case, exorbitance

If you treat all your color choices this way, establishing three word associations for each, chances are you’ll gain a
good idea of how that color is widely perceived among your audience.

Even though color psychology plays a role in the way a visitor may see your site, keep in mind there is no wrong color
to use. While psychological reasoning may help to start your palette, the success of a color scheme depends on the
harmony that exists between all the colors chosen. To achieve this harmony, we’ll need to be mindful of a few other
attributes of color.



Teoria das cores parte 1
========================

Continuing on from the previous article in this series on color, The Psychology of Color, let’s take a closer look at
color temperature, chromatic values, saturation, and the basic theory behind how colors are produced, both in print and
on the web.



Color Temperature
-----------------

One attribute that exists across the entire spectrum is the notion of color temperature. Which color faucet gives you
hot water? What color do you associate with ice? Why? The answers are obvious, and are enforced by both culture and
nature.

.. figure:: ../_images/color-warm.png
    :align: center

Warm colors are the colors from red to yellow, including orange, pink, brown, and burgundy. Due to their association
with the sun and fire, warm colors represent both heat and motion. When placed near a cool color, a warm color will
tend to pop out and dominate dominate.

.. figure:: ../_images/color-cool.png
    :align: center

Cool colors are the colors from green to blue, and can include some shades of violet. Violet is the intermediary between
red and blue, so a cooler violet is, as you probably guessed, one that’s closer to blue, while a reddish violet can feel
warm. Cool colors are calming, and can reduce tension. In a design, cool colors tend to recede, making them great for
backgrounds and larger elements on a page, since they won’t overpower your content.



Chromatic Value
---------------

The measure of the lightness or darkness of a color is known as its chromatic value. Adding white to a color creates a
tint of that color. Likewise, a shade is produced by adding black to a given color. Figure 1, “Chromatic value” to the
left illustrates this distinction.

.. figure:: ../_images/color-value.png
    :align: center

As with colors themselves, the chromatic value of colors you’re using can impact upon the psychological connection users
will have to the content. One use of chromatic value might be to accent the time of day that customers associate with a
company or organization. If you were designing a website that’s all about nightlife or concerts, for instance, you’d
probably want to go with dark shades and limit your use of light tints. Tints tend to be associated with daylight,
springtime, and childhood. Think: sunrise, baby clothes, and Care Bears. These light pastel colors can be used in
professional, sophisticated, grown-​​up ways, too, as anyone who’s ever spent time in a hospital can attest. This is
because tints are soothing colors that provide personality to sterile environments without startling the ill or making
babies cry. Color designers are generally uninspired by colors such as “Hospital Green,” but if you’re working on a
website for a day spa, tints would be a great foundation for your color palette.



Saturation
----------

The saturation or intensity of a color is described as the strength or purity of that color. It’s obvious that intense,
vivid colors stand out. Even though cool colors tend to recede, a vivid blue will draw more attention to itself than a
dull orange. When we add gray (black and white) to a color, it becomes dull and muted. Like an office with beige walls,
or an overcast winter morning, these colors are less exciting or appealing as bright, vivid colors. On the bright side — 
no pun intended — dull colors help to reduce tension, giving compositions a meditative, dreamy mood

The relationship between value and saturation is illustrated in Figure 2, “Value and saturation chart”.

.. figure:: ../_images/color-value-saturation.png
    :align: center

    Fig. 2, “Value and saturation chart”

To take our foundational knowledge of color any further, we’ll need to gain a grounding in some of the more technical
concepts associated with the subject, such as how colors are formed and how they can be categorized.

The colors displayed on your computer screen (that is, the colors we’ll be using in our website designs) are based on an
additive color model. In an additive color model, colors are displayed in percentages of red, green, and blue (RGB)
light. If we turn all three of these colors on full blast, we’ll have white light. If we turn red and green all the way
up, but switch off blue, we have yellow.

If you’ve ever owned a color printer, you might be familiar with the acronym CMYK (cyan, magenta, yellow, and black).
Your ink-​​jet printer, laser printer, and industrial four-​​color printing press all create images using cyan, magenta,
yellow, and black inks or toners. This process uses a subtractive color model; by combining colors in this color model,
we come close to achieving a grayish black. There’s no way of producing black combining just cyan, magenta, and yellow.
This is why they’re always supplemented with black — the K in CMYK. Take a look at Figure 3, “RGB additive color model
(left) and the CMYK subtractive color model (right)” for a better idea of how additive and subtractive color models
work.

.. figure:: ../_images/color-rgb-cmyk.png
    :align: center

    Fig. 3, RGB additive color model (left) and the CMYK subtractive color model (right)


Regardless of whether you’re designing for print or the Web, the lessons of traditional color theory are key to helping
us classify colors and group them together. Recorded studies of color classification date back to the third century BC
and the works of Aristotle. Since then, many other great artists and philosophers have contributed to our knowledge of
how colors work, including Isaac Newton, Johann Wolfgang von Goethe, and Johannes Itten. The works of these individuals,
in the 17TH, 18TH, and 19TH centuries respectively, provide the foundations on which much of our understanding of color
lies. All three theorists explained colors in relation to a color wheel, using red, yellow, and blue as the primary
colors. The color wheel is a simple but effective diagram developed to present the concepts and terminology of color
theory. The traditional artists’ wheel is a circle divided into 12 slices, as Figure 4, “The traditional red, yellow,
and blue artists’ color wheel” indicates. Each slice is either a primary color, a secondary color, or a tertiary color.

.. figure:: ../_images/color-colorwheel.png
    :align: center

    Fig. 4, The traditional red, yellow, and blue artists’ color wheel


Primary colors
The primary colors of the traditional color wheel are red, yellow, and blue. These hues form an equilateral triangle on
the color wheel, and commencing from a primary color, every fourth color represents another primary color.

Secondary colors
By mixing two primary colors, we create secondary colors, indicated here by the small gray triangles. The secondary
colors are orange, green, and purple.

Tertiary colors
There’s a total of six tertiary colors: vermilion (red-​​orange), marigold (yellow-​​orange), chartreuse (yellow-​​green),
aquamarine (blue-​​green), violet (blue-​​purple), and magenta (red-​​purple). As you might already have guessed, mixing a
primary color with an adjacent secondary color forms the tertiary colors.



Teoria das cores parte 2
========================

First impressions are everything. How you look and how you present yourself can determine how you are perceived.
The same goes for our design work. The impression that our work gives depends on a myriad of different factors.
One of the most important factors of any design is color. Color reflects the mood of a design and can invoke emotions,
feelings, and even memories. If you haven’t gone back to the basics of color theory lately, you might find some insights
that you’ve overlooked.

Figuring out which colors work well with others isn’t just a matter of chance. There is actually a science behind which
colors work well together. Different color combinations fit into different categories, and can be broken down easily.
Let’s start with the absolute basics and move on to more advanced color combinations.



Primary Colors
--------------

.. figure:: ../_images/primary.jpg
    :align: center

Colors start out with the basis of all colors, called the Primary Colors. These are red, yellow, and blue. If we are
talking about screen colors, such as for web devices and monitors, red green, and blue (RGB) are the basic colors which
make up all colors found on screen devices.



Secondary Colors
----------------

.. figure:: ../_images/secondary.jpg
    :align: center

If you evenly mix red and yellow, yellow and blue, and blue and red, you create the secondary colors, which are green,
orange and violet. Combining these colors in projects can make for a lot of contrast.



Tertiary Colors
---------------

.. figure:: ../_images/tertiary.jpg
    :align: center

Tertiary colors are made when you take the secondary colors and mix them with the primary colors. These are red-​​violet,
blue-​​violet, blue-​​green, yellow-​​green, red-​​orange, and yellow-​​orange.

So, now that you know how colors are made, you can understand how the color combinations on the color wheel model work.
Understanding the principles of color combinations will help you to choose combinations that work well together, set the
right mood, and create the right amount of contrast within your design work. Next are the basic color combinations
derived from the color wheel.



Complimentary Colors
--------------------

.. figure:: ../_images/complimentary2.jpg
    :align: center

.. figure:: ../_images/complimentary.jpg
    :align: center

Complimentary colors are colors that are opposite each other on the color wheel. Examples would be blue and orange, red
and green, Yellow and purple, etc. Complementary color schemes create a high amount of contrast, but can create a lot of
visual vibration when they are used at full saturation.



Analogous Colors
----------------

.. figure:: ../_images/analogous.jpg
    :align: center

.. figure:: ../_images/analogous1.jpg
    :align: center

Analogous colors are colors that are next to each other on the color wheel. It is a good idea to choose a set of
analogous colors that create a sense of variety. A good example would be blue-​​green, blue, and blue-​​violet or
yellow-​​green, yellow, and yellow-​​orange.



Triads
------

.. figure:: ../_images/triad.jpg
    :align: center

.. figure:: ../_images/triad2.jpg
    :align: center

A triad of colors is a set of colors that are evenly spaced around the color wheel. A triad has a nice variety of
colors, but is also well balanced. In the examples above, blue-​​violet and yellow-​​green create a lot of contrast.



Split Complimentary Colors
--------------------------

.. figure:: ../_images/split_complimentary.jpg
    :align: center

Split complimentary colors take a color and — instead of choosing the color directly across from it on the color wheel —
it takes the two on either side of it. In the example above, we chose yellow. The opposite color on the color wheel is
purple, but instead we choose blue-​​violet and red-​​violet, which creates a lot of contrast and make for some highly
cooperative colors.



Square Colors
-------------

.. figure:: ../_images/square-wheel.jpg
    :align: center

The square color model takes four colors evenly spaced around the color wheel. In the example above, the colors are
blue, orange, red-​​violet, and yellow-​​green. This color scheme is really nice and would work well with one strong color
and muted versions of the other colors.



Tetradic Colors
---------------

.. figure:: ../_images/tetrad.jpg
    :align: center

Tetradic color schemes are built by creating a rectangle on the color wheel. Select two opposites on the color wheel and
then select another color two spaces over and its compliment across the color wheel.



Tints and Shades
----------------

.. figure:: ../_images/tints_and_shades.jpg
    :align: center

A tint of a color is when you take a color, such as blue in the example above, and add white to it. A shade is a hue
that has black added to it. You can create a monochromatic color scheme buy using tints and shades of the same hue.



Warm Colors
-----------

.. figure:: ../_images/warmcolors.jpg
    :align: center

Warm colors create a sense of warmth and heat in an image or a design. When you see warm colors, you think of the sun,
heat, fire, and love (passion). Red is the color of blood, which is warm, and orange and yellow go along with summer.
Adding an orange photo filter to an image instantly makes it look warmer and happier.



Cool Colors
-----------

.. figure:: ../_images/coolcolors.jpg
    :align: center

Cool colors carry connotations of cool climates, winter, death, sadness, ice, night, and water. Cool colors can be
associated with calmness, tranquility, trust, cleanliness. Purple is associated with royalty, because they are supposed
to be reserved.



Significado da cores
====================

Red
----

Red is the color of love and passion. Boxes of candies are red on valentines day. Some are pink, which is a tint of red.
Red is also the color of anger and blood. Red, orange and yellow are all found in fire. Red can also mean danger. Stop
signs are red, which get our attention and tell us to be careful and look before we proceed. Red is dominant, and when
combined with colors such as black, can create a very masculine look. Red commands attention and can set a serious tone.

.. figure:: ../_images/red.jpg
    :align: center



Orange
------

Orange represents warmth, but isn’t aggressive like red is. Orange can portray a fun atmosphere because it is energetic
and creates a sense of warmth without associated connotations of danger and blood, as with the color red. Orange can be
associated with health, such as vitamin C, which is commonly found in oranges.

.. figure:: ../_images/orange.jpg
    :align: center



Yellow
------

Yellow is associated with the sun and warmth. When used with orange, it creates a sense of summer fun. Yellow can be
associated with thirst, and can be found on the walls of many refreshment shops. Yellow can also be associated with
cowardice and fear, which comes from the old expression of someone being “yellow.” When combined with black, it can gain
a lot of attention. A good example outside of design would be a taxi. The combination gets a lot of attention.

.. figure:: ../_images/yellow.jpg
    :align: center



Green
-----

Green is the color of money, so in our culture it is associated with wealth. Since most plants are green, it is also
associated with growth and health. It is used to show that products are natural and healthy, it also connotes profit
and gain. Combined with blue, green further perpetuates health, cleanliness, life, and nature.

.. figure:: ../_images/green.jpg
    :align: center



Blue
----

Depending on the tint and shade of blue, it can represent different feelings, thoughts, and emotions. In imagery, dark
shades of blue can give a sense of sadness. An expression that goes along with this is “singing the blues” when someone
is sad. Light blue is the color of the sky and of water, which can be refreshing, free, and calm. Blue skies are calming
and tranquil. Water washes away dirt and cleans wounds. Blue can represent freshness and renewal, such as when rain
washes away dirt and dust. The calmness of blue promotes relaxation.

.. figure:: ../_images/blue.jpg
    :align: center



Purple
------

Associated from the color of the robes of royalty, purple relates to royalty. Purples with more red can be associated
with romances, intimacy, softness, and comfort. Purple can give a sense of mystique as well as luxury. A good example
would be the wine website shown below.

.. figure:: ../_images/purple.jpg
    :align: center



White
-----

White can be associated with sterility, due to doctors wearing white and most hospitals being white. Because most
artistic depictions of religious figures are completely colorless, white represents “good” and holiness. White can
represent cleanliness, such as clean linens and clean laundry. It can represent softness due to cotton and clouds. It
can reference mental health due to the white coats and uniforms, white walls, etc. White is great for connoting health
and cleanliness, as shown in the optical website shown below.

.. figure:: ../_images/white.jpg
    :align: center



Black
-----

Black is mostly associated with death, especially in the United States. It can represent decay — due to rot — based on
how food breaks down and turns black. Black can represent evil, because it is the opposite of white, which often
represents good. It can represent anxiety due to darkness and the unknown. A lot of black in an image can suggest
depression and despair, as well as loneliness. However, despite all of the negative connotations, when combined with
other colors, such as gold, it can represent luxury. Combined with silver or grey, it can represent sophistication, such
as in the timepiece website shown below.

.. figure:: ../_images/black.jpg
    :align: center



Conclusion
----------

It is essential to understand color as a designer. Everything that you design should take color into serious and
careful consideration. The color choices that you make can create a drastic effect on the mood of your work. The right
combination can gain attention and convey the right message visually, further driving the message into your viewers’
minds. The emotional side of design is extremely lucrative, and if you aren’t carefully considering your color choices,
you should from now on. Your clients will see better results, and your message will have added clarity and strength.
Color makes as much of a connection with people as imagery does.



Tipografia
==========

Design is about visual communication and effective delivery of a carefully-​​crafted message. The message is delivered
through a combination of imagery, color, words, and typography. Understanding typography will help you make your message
strong, compelling, and persuasive. It’s important to choose the right typeface for the right occasion, and to make sure
that it reinforces your design and your message. In order to choose the right typeface, you have to understand the
fundamentals of type, the differences between typefaces, and why you should and shouldn’t use different typefaces in
certain situations.



Anatomy
-------

To understand type, it’s a good idea to understand the parts and pieces that make up a typeface. The variation found in
each of these parts and pieces is what distinguishes typefaces from one another.

.. figure:: ../_images/anatomy.jpg
    :align: center



Definitions of Type Anatomy
+++++++++++++++++++++++++++

* Apex — The portion of letters A, M, and N where two strokes meet to form a peak.
* Stem — Any vertical line found in any letter, such as the ones found in H, B, d, b, F, or any other letter that contains a vertical line.
* Arm — The horizontal stroke that connects to a stem on one end, but not on the other, such as in the letters E, F, and T.
* Spur — The part of some uppercase G’s that sticks out, similar to a chin on a person’s face.
* Crossbar — The thin bar between two stems, such as the ones found in the letters A and H.
* Leg — The downward diagonal stem, such as the one found in the letters K and R.
* Stem — The vertical stroke found in any letter, such as D, B, b, d, H, F, etc.
* Tail — The descender found on the letter Q.
* Spine — The middle diagonal line that connects the rounded portions found on the letter S.
* Bracket — The curved part of a letter between the stem and the serif. If there is not bracket present in the letter, but there is still a serif, then you’re usually viewing a slab serif typeface.
* Eye — The enclosed portion of the lowercase e.
* Ear -  A decorative flourish usually on the upper right side of the bowl on a lowercase g.
* Ascender — The part of a lowercase letter that rises above the x-​​height, such as a b, d, h, etc.
* Shoulder — The rounded part of a letter such as r, m, or n. This name comes from the fact that it resembles a human shoulder.
* Bowl — The curved part of a letter that encloses the circular portion of a letter, such as a B, P, g, D, etc.
* Counter — The closed or partially-​​enclosed circular or curved area of empty space of some letters such as o, and d.
* Finial — The tapered portion of the end of the stroke of a letter such as in the letter c or e.
* Neck/​Link — The thin line that connects the upper and lower bowls of a lowercase g.
* Loop — The rounded portion of a lowercase g.
* Serif — The squared or rectangular end on the stroke of a letter, including the stems, legs, arms, etc.
* Axis — The line that bisects a letter down the middle is referred to as an axis.
* Descender — The portion of a letter that extend below the baseline, found in y, p, or q.
* Terminal — The end of any stroke in a letter that doesn’t end in a serif.
* Cap Height — The height from the baseline of a typeface to the top of the uppercase letters.
* X-​​Height — The height of the lowercase portion of letters, which is typically defined by the height of the lowercase x.
* Baseline — The line on which any letter sits. any letter whose stem goes below this point is called a descender.



Classification
--------------

Typefaces are classified by their different styles and features. The features are determined by a set of rules that are
based on the differences in structure and style.



Serif
+++++

.. figure:: ../_images/serif.jpg
    :align: center

Serif fonts have been used for decades. Their negative space makes them easy to read at small sizes. That is why they
have been favored for body copy in books. Serif fonts also look great in large sizes, although some professional serif
fonts have a display version that is optimized for large type sizes. Good examples of serif typefaces are: Baskerville,
Didot, Caslon, Pigeon, Quaver Serif, Sabon, Bembo, Bodoni, Clarendon, Garamond, Georgia, Goudy, & Meta Serif.


Sans Serif
++++++++++

.. figure:: ../_images/sans-serif.jpg
    :align: center

Sans serif literally means “without serifs,” and they are also used for both body copy and display purposes. Some fonts,
such as Futura are preferred for body copy because of their balance between positive and negative space, making them
easy to read. Other sans serif typefaces, such as League Gothic, are used for display purposes. You wouldn’t use League
Gothic for large amounts of text. Good examples of sans serif fonts are Futura, Arial, Helvetica, League Gothic, Gill
Sans, Optima, Franklin Gothic, Frutiger, Bebas, Franchise, Akzidenz Grotesk, Univers, Meta, and DIN. There are also many
others available, but those are a good start.



Slab Serif
++++++++++

.. figure:: ../_images/slab-serif.jpg
    :align: center

Slab serif fonts have a unique appearance because of their rigid serifs. A slab serif contains no brackets, making their
serifs appear blocky, but this gives the typeface a grounded appearance. The slab serifs make the font appear very
stable, which makes slab serif fonts good for traditional identities. This type of font gives the impression of
stability. Good examples of slab serif fonts are: Rockwell, Museo, Chunk, and Nilland.


Gothic
++++++

.. figure:: ../_images/gothic.jpg
    :align: center

Gothic is meant to be decorative, and it really shows. The decorative flourishes and swashes really show off an old-​​time
craftsmanship. It is meant to emulate the type styles of old. These fonts should be used sparingly, because they can be
difficult to read in large doses. good examples would be Lucida Blackletter, Deutsch Gothic, Perry Gothic, Rothenburg
Decorative, and Urdeutsch.



Script
++++++

.. figure:: ../_images/script.jpg
    :align: center

Script typefaces are meant to be elegant and decorative. Their appearance undeniably states importance and formality.
These typefaces are great for invitations, greeting cards, anniversaries, graduations, and romantic applications. The
tone of these typefaces is serious. Good examples are Scriptina, Pro, Snell Roundhand, and Edwardian Script.



Brush
+++++

.. figure:: ../_images/brush.jpg
    :align: center


Brush fonts are meant to emulate custom hand-​​lettered type. They have thick and thin varying strokes and are very round
and fluid. These typefaces appear as though they were made by an artist with a brush. Some examples are Brush Script MT,
Ballpark, Artbrush, Bello, Reklame Script, and Lobster.



Handwriting
+++++++++++

.. figure:: ../_images/handwriting.jpg
    :align: center

Handwriting fonts are designed to simulate natural handwriting. This gives a design a more personal touch and makes the
design appear friendlier. You will find typefaces like these on organic or personal products where an emotional
connection is needed. Good examples are Journal, Handwriting — Dakota, Grimshaw Hand, Noteworthy, and Windsong.



Ligatures
---------

Ligatures are when two letters are visually combined to optimize reading and viewing. Strokes are combined, such as the
crossbars of two seperate letters, or the crossbar of a “t” with the stem if an “i”. Examples of this are ff, fi, ffi,
fl, fh, tt, ti, etc. You can see examples shown below.

.. figure:: ../_images/Screen-shot-2013-01-10-at-9.06.33-AM.png
    :align: center



Kerning & Tracking
------------------

Kerning is the amount of space between two letters and tracking is the space between a group of letters. Many times the
spacing between two letters needs to be optimized, such as with an “o” and a “v” next to each other, or an uppercase “T”
with most lowercase letters. This applies to many different combinations of letters and depends on the typeface. The
example below shows where some letters could be kerned a little tighter so that the word flows more cohesively.

.. figure:: ../_images/Screen-shot-2013-01-10-at-9.11.59-AM.png
    :align: center

Below, you can see that the stacked type isn’t working well in the first example. The tracking is normal, but the word
“photography” is just too large and there isn’t enough contrast and size difference between the letters. In the bottom
example, the type size of the word was reduced and the tracking was increased dramatically, creating an elegant effect
with more impact, while reducing the dissonant visual competition between the two words.

.. figure:: ../_images/Screen-shot-2013-01-10-at-9.19.21-AM.png
    :align: center



Leading
-------

Leading is the amount of space between each line of text. Finding the optimum amount of space between lines of text
will make large bodies of text easier to read. When you read large amounts of text and the lines are too close together,
you often end up re-​​reading lines by mistake. This makes for an unpleasant read and can be very frustrating. Opening up
the leading of a large body of text will make it easier to read, while making its overall appearance more elegant. As
you can see below, 2pt of leading makes a huge difference in readability. Imagine squinting to read an entire book. Your
eyes would get tired and you might end up with a headache from having to concentrate on not losing your place.

.. figure:: ../_images/Screen-shot-2013-01-10-at-9.33.06-AM.png
    :align: center



Conclusion
----------

Typography is an essential skill for any designer. Our whole career depends on being able to send an effective message
and elicit a powerful, positive response. If someone can’t read the message, then the whole design fails. Following
these principles will ensure that you understand type and how to avoid major mistakes, such as not having enough leading,
or not kerning your type properly. In large bodies of text you may not kern every word, but on large headlines, kerning
is essential. If you follow the guidelines from this article, your designs will be easy and enjoyable to read.



Dicas de cores e tipografia para diversos tipo de projetos
==========================================================



Projetando layouts web
======================



Implementando o que foi definido no projeto
===========================================
