***********
HTML 5 (8h)
***********

Visão geral do HTML5
====================

De acordo com o W3C a Web é baseada em 3 pilares:

* Um esquema de nomes para localização de fontes de informação na Web, esse esquema chama-se URI.
* Um Protocolo de acesso para acessar estas fontes, hoje o HTTP.
* Uma linguagem de Hypertexto, para a fácil navegação entre as fontes de informação: o HTML.

Vamos nos focar no terceiro pilar, o HTML.

Hypertexto
----------

HTML é uma abreviação de Hypertext Markup Language Linguagem de Marcação de Hypertexto. Resumindo em uma frase: o
HTML é uma linguagem para publicação de conteúdo (texto, imagem, vídeo, áudio e etc) na Web.

O HTML é baseado no conceito de Hipertexto. Hipertexto são conjuntos de elementos – ou nós – ligados por conexões.
Estes elementos podem ser palavras, imagens, vídeos, áudio, documentos etc. Estes elementos conectados formam uma
grande rede de informação. Eles não estão conectados linearmente como se fossem textos de um livro, onde um assunto
é ligado ao outro seguidamente. A conexão feita em um hipertexto é algo imprevisto que permite a comunicação de dados,
organizando conhecimentos e guardando informações relacionadas.

Para distribuir informação de uma maneira global, é necessário haver uma linguagem que seja entendida universalmente por
diversos meios de acesso. O HTML se propõe a ser esta linguagem.

Desenvolvido originalmente por Tim Berners-Lee o HTML ganhou popularidade quando o Mosaic browser desenvolvido por
Marc Andreessen na década de 1990 ganhou força. A partir daí, desenvolvedores e fabricantes de browsers utilizaram o
HTML como base, compartilhando as mesmas convenções.



O começo e a interoperabilidade
-------------------------------

Entre 1993 e 1995, o HTML ganhou as versões HTML+, HTML2.0 e HTML3.0, onde foram propostas diversas mudanças para
enriquecer as possibilidades da linguagem. Contudo, até aqui o HTML ainda não era tratado como um padrão. Apenas em
1997, o grupo de trabalho do W3C responsável por manter o padrão do código, trabalhou na versão 3.2 da linguagem,
fazendo com que ela fosse tratada como prática comum. Você pode ver: http://www.w3.org/TR/html401/appendix/changes.html.

Desde o começo o HTML foi criado para ser uma linguagem independente de plataformas, browsers e outros meios de
acesso. Interoperabilidade significa menos custo. Você cria apenas um código HTML e este código pode ser lido por
diversos meios, ao invés de versões diferentes para diversos dispositivos. Dessa forma, evitou-se que a Web fosse
desenvolvida em uma base proprietária, com formatos incompatíveis e limitada.

Por isso o HTML foi desenvolvido para que essa barreira fosse ultrapassada, fazendo com que a informação publicada por
meio deste código fosse acessível por dispositivos e outros meios com características diferentes, não importando o
tamanho da tela, resolução, variação de cor. Dispositivos próprios para deficientes visuais e auditivos ou dispositivos
móveis e portáteis. O HTML deve ser entendido universalmente, dando a possibilidade para a reutilização dessa informação
de acordo com as limitações de cada meio de acesso.



WHAT Working Group
------------------

Enquanto o W3C focava suas atenções para a criação da segunda versão do XHTML, um grupo chamado Web Hypertext
Application Technology Working Group ou WHATWG trabalhava em uma versão do HTML que trazia mais flexibilidade para a
produção de websites e sistemas baseados na web.

O WHATWG (http://www.whatwg.org/) foi fundado por desenvolvedores de empresas como Mozilla, Apple e Opera em 2004. Eles
não estavam felizes com o caminho que a Web tomava e nem com o rumo dado ao XHTML. Por isso, estas organizações se
juntaram para escrever o que seria chamado hoje de HTML5.

Entre outros assuntos que o WHATWG se focava era Web Forms 2.0 que foi incluído no HTML5 e o Web Controls 1.0 que foi
abandonado por enquanto.

A participação no grupo é livre e você pode se inscrever na lista de email para contribuir.

Por volta de 2006, o trabalho do WHATWG passou ser conhecido pelo mundo e principalmente pelo W3C que até então
trabalhavam separadamente que reconheceu todo o trabalho do grupo. Em Outubro de 2006, Tim Berners-Lee anunciou que
trabalharia juntamente com o WHATWG na produção do HTML5 em detrimento do XHTML 2. Contudo o XHTML continuaria sendo
mantido paralelamente de acordo com as mudanças causadas no HTML. O grupo que estava cuidando especificamente do XHTML 2
foi descontinuado em 2009.



O HTML5 e suas mudanças
-----------------------

Quando o HTML4 foi lançado, o W3C alertou os desenvolvedores sobre algumas boas práticas que deveriam ser seguidas ao
produzir códigos client-side. Desde este tempo, assuntos como a separação da estrutura do código com a formatação e
princípios de acessibilidade foram trazidos para discussões e à atenção dos fabricantes e desenvolvedores.

Contudo, o HTML4 ainda não trazia diferencial real para a semântica do código. o HTML4 também não facilitava a
manipulação dos elementos via Javascript ou CSS. Se você quisesse criar um sistema com a possibilidade de Drag’n Drop
de elementos, era necessário criar um grande script, com bugs e que muitas vezes não funcionavam de acordo em todos os
browsers.



O que é o HTML5?
----------------

O HTML5 é a nova versão do HTML4. Enquanto o WHATWG define as regras de marcação que usaremos no HTML5 e no XHTML, eles
também definem APIs que formarão a base da arquitetura web. Essas APIs são conhecidas como DOM Level 0.

Um dos principais objetivos do HTML5 é facilitar a manipulação do elemento possibilitando o desenvolvedor a modificar as
características dos objetos de forma não intrusiva e de maneira que seja transparente para o usuário final.

Ao contrário das versões anteriores, o HTML5 fornece ferramentas para a CSS e o Javascript fazerem seu trabalho da
melhor maneira possível. O HTML5 permite por meio de suas APIs a manipulação das características destes elementos, de
forma que o website ou a aplicação continue leve e funcional.

O HTML5 também cria novas tags e modifica a função de outras. As versões antigas do HTML não continham um padrão
universal para a criação de seções comuns e específicas como rodapé, cabeçalho, sidebar, menus e etc. Não havia um
padrão de nomenclatura de IDs, Classes ou tags. Não havia um método de capturar de maneira automática as informações
localizadas nos rodapés dos websites.

Há outros elementos e atributos que sua função e significado foram modificados e que agora podem ser reutilizados de
forma mais eficaz. Por exemplo, elementos como B ou I que foram descontinuados em versões anteriores do HTML agora
assumem funções diferentes e entregam mais significado para os usuários.

O HTML5 modifica a forma de como escrevemos código e organizamos a informação na página. Seria mais semântica com menos
código. Seria mais interatividade sem a necessidade de instalação de plugins e perda de performance. É a criação de
código interoperável, pronto para futuros dispositivos e que facilita a reutilização da informação de diversas formas.

O WHATWG tem mantido o foco para manter a retrocompatibilidade. Nenhum site deverá ter de ser refeito totalmente para se
adequar aos novos conceitos e regras. O HTML5 está sendo criado para que seja compatível com os browsers recentes,
possibilitando a utilização das novas características imediatamente.



Motores de Renderização
-----------------------

Há uma grande diversidade de dispositivos que acessam a internet. Entre eles, há uma série de tablets, smartphones,
pc’s e etc. Cada um destes meios de acesso utilizam um determinado browser para navegar na web. Não há como os
desenvolvedores manterem um bom nível de compatibilidade com todos estes browsers levando em consideração a
particularidade de cada um. Uma maneira mais segura de manter o código compatível, é nivelar o desenvolvimento pelos
motores de renderização. Cada browser utiliza um motor de renderização que é responsável pelo processamento do código
da página.

Abaixo, segue uma lista dos principais browsers e seus motores:

=======  ========================
Motor    Browser
=======  ========================
Webkit   Safari, Google Chrome
Gecko    Firefox, Mozilla, Camino
Trident  Internet Explorer 4 ao 9
Presto   Opera 7 ao 10
=======  ========================



Estrutura básica, Doctype e Charsets
====================================

A estrutura básica do HTML5 continua sendo a mesma das versões anteriores da linguagem, há apenas uma excessão na
escrita do Doctype. Segue abaixo como a estrutura básica pode ser seguida:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="pt-br">
        <head>
            <meta charset="UTF-8">
            <link rel="stylesheet" type="text/css" href="estilo.css">
            <title></title>
        </head>
        <body>
        </body>
    </html>



O Doctype
---------

O Doctype deve ser a primeira linha de código do documento antes da tag HTML.

.. code-block:: html

    <!DOCTYPE html>



O Doctype indica para o navegador e para outros meios qual a especificação de código utilizar. Em versões anteriores,
era necessário referenciar o DTD diretamente no código do Doctype. Com o HTML5, a referência por qual DTD utilizar é
responsabilidade do Browser.

O Doctype não é uma tag do HTML, mas uma instrução para que o browser tenha informações sobre qual versão de código a
marcação foi escrita.



O elemento HTML
---------------

O código HTML é uma série de elementos em árvore onde alguns elementos são filhos de outros e assim por diante. O
elemento principal dessa grande árvore é sempre a tag HTML.

.. code-block:: html

    <html lang="pt-br">



O atributo LANG é necessário para que os user-agents saibam qual a linguagem principal do documento.

Lembre-se que o atributo LANG não é restrito ao elemento HTML, ele pode ser utilizado em qualquer outro elemento para
indicar o idioma do texto representado.

Para encontrar a listagem de códigos das linguagens, acesse:
http://www.w3.org/International/questions/qa-choosing-language-tags.



HEAD
----

A Tag HEAD é onde fica toda a parte inteligente da página. No HEAD ficam os metadados. Metadados são informações sobre a
página e o conteúdo ali publicado.

Metatag Charset
---------------

No nosso exemplo há uma metatag responsável por chavear qual tabela de caractéres a página está utilizando.

.. code-block:: html

    <meta charset="utf-8">



Nas versões anteriores ao HTML5, essa tag era escrita da forma abaixo:

.. code-block:: html

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">



Essa forma antiga será também suportada no HTML5. Contudo, é melhor que você utilize a nova forma.

A Web é acessada por pessoas do mundo inteiro. Ter um sistema ou um site que limite o acesso e pessoas de outros países
é algo que vai contra a tradição e os ideais da internet. Por isso, foi criado uma tabela que suprisse essas
necessidades, essa tabela se chama Unicode. A tabela Unicode suporta algo em torno de um milhão de caracteres. Ao invés
de cada região ter sua tabela de caracteres, é muito mais sensato haver uma tabela padrão com o maior número de
caracteres possível. Atualmente a maioria dos sistemas e browsers utilizados por usuários suportam plenamente Unicode.
Por isso, fazendo seu sistema Unicode você garante que ele será bem visualizado aqui, na China ou em qualquer outro
lugar do mundo.

O que o Unicode faz é fornecer um único número para cada caractere, não importa a plataforma, nem o programa, nem a
língua.



Tag LINK
--------

Há dois tipos de links no HTML: a tag A, que são links que levam o usuário para outros documentos e a tag LINK, que são
links para fontes externas que serão usadas no documento.

No nosso exemplo há uma tag LINK que importa o CSS para nossa página:

.. code-block:: html

    <link rel="stylesheet" type="text/css" href="estilo.css">



O atributo ``rel="stylesheet"`` indica que aquele link é relativo a importação de um arquivo referente a folhas de estilo.
Há outros valores para o atributo REL, como por exemplo o ALTERNATE:

.. code-block:: html

    <link rel="alternate" type="application/atom+xml" title="feed" href="/feed/">



Neste caso, indicamos aos user-agents que o conteúdo do site poder ser encontrado em um caminho alternativo via Atom
FEED.

No HTML5 há outros links relativos que você pode inserir como o rel="archives" que indica uma referência a uma coleção de
material histórico da página. Por exemplo, a página de histórico de um blog pode ser referenciada nesta tag.



Modelos de conteúdo
===================

Há pequenas regras básicas que nós já conhecemos e que estão no HTML desde o início. Estas regras definem onde os
elementos podem ou não estar. Se eles podem ser filhos ou pais de outros elementos e quais os seus comportamentos.

Dentre todas as categorias de modelos de conteúdo, existem dois tipos de elementos: elementos de linha e de bloco.

Os elementos de linha marcam, na sua maioria das vezes, texto. Alguns exemplos: a, strong, em, img, input, abbr, span.

Os elementos de blocos são como caixas, que dividem o conteúdo nas seções do layout.

Abaixo segue algumas premissas que você precisa relembrar e conhecer:

*   Os elementos de linha podem conter outros elementos de linha, dependendo da categoria que ele se encontra. Por
    exemplo: o elemento a não pode conter o elemento label.

*   Os elementos de linha nunca podem conter elementos de bloco.

*   Elementos de bloco sempre podem conter elementos de linha.

*   Elementos de bloco podem conter elementos de bloco, dependendo da categoria que ele se encontra. Por exemplo, um
    parágrafo não pode conter um DIV. Mas o contrário é possível.

Estes dois grandes grupos podem ser divididos em categorias. Estas categorias dizem qual modelo de conteúdo o elemento
trabalha e como pode ser seu comportamento.



Categorias
----------

Cada elemento no HTML pode ou não fazer parte de um grupo de elementos com características similares. As categorias
estão a seguir. Manteremos os nomes das categorias em inglês para que haja um melhor entendimento:

* Metadata content
* Flow content
    * Sectioning content
    * Heading content
    * Phrasing content Markup
        * Embedded content
    * Interactive content

Abaixo segue como as categorias estão relacionadas de acordo com o WHATWG:

.. figure:: ../_images/categorias.png



Metadata content
----------------

Os elementos que compõe a categoria Metadata são:

* base
* command
* link
* meta
* noscript
* script
* style
* title

Este conteúdo vem antes da apresentação, formando uma relação com o documento e seu conteúdo com outros documentos que
distribuem informação por outros meios.



Flow content
------------

A maioria dos elementos utilizados no body e aplicações são categorizados como Flow Content. São eles:

* a
* abbr
* address
* area (se for um decendente de um elemento de mapa)
* article
* aside
* audio
* b
* bdo
* blockquote
* br
* button
* canvas
* cite
* code
* command
* datalist
* del
* details
* dfn
* div
* dl
* em
* embed
* fieldset
* figure
* footer
* form
* h1
* h2
* h3
* h4
* h5
* h6
* header
* hgroup
* hr
* i
* iframe
* img
* input
* ins
* kbd
* keygen
* label
* link (Se o atributo itemprop for utilizado)
* map
* mark
* math
* menu
* meta (Se o atributo itemprop for utilizado)
* meter
* nav
* noscript
* object
* ol
* output
* p
* pre
* progress
* q
* ruby
* samp
* script
* section
* select
* small
* span
* strong
* style (Se o atributo scoped for utilizado)
* sub
* sup
* svg
* table
* textarea
* time
* ul
* var
* video
* wbr
* Text

Por via de regra, elementos que seu modelo de conteúdo permitem inserir qualquer elemento que se encaixa no Flow Content,
devem ter pelo menos um descendente de texto ou um elemento descendente que faça parte da categoria embedded.



Sectioning content
------------------

Estes elementos definem um grupo de cabeçalhos e rodapés.

* article
* aside
* nav
* section

Basicamente são elementos que juntam grupos de textos no documento.



Heading content
---------------

Os elementos da categoria Heading definem uma seção de cabeçalhos, que podem estar contidos em um elemento na categoria 
Sectioning.

* h1 
* h2 
* h3 
* h4 
* h5 
* h6 
* hgroup



Phrasing content
----------------

Fazem parte desta categoria elementos que marcam o texto do documento, bem como os elementos que marcam este texto 
dentro do elemento de parágrafo.

* a 
* abbr 
* area (se ele for descendente de um elemento de mapa) 
* audio 
* b 
* bdo 
* br 
* button 
* canvas 
* cite 
* code 
* command 
* datalist 
* del (se ele contiver um elemento da categoria de Phrasing) 
* dfn 
* em 
* embed 
* i 
* iframe 
* img 
* input 
* ins (se ele contiver um elemento da categoria de Phrasing) 
* kbd 
* keygen 
* label 
* link (se o atributo itemprop for utilizado) 
* map (se apenas ele contiver um elemento da categoria de Phrasing) 
* mark 
* math
* meta (se o atributo itemprop for utilizado) 
* meter 
* noscript 
* object
* output 
* progress 
* q 
* ruby 
* samp 
* script 
* select 
* small 
* span 
* strong 
* sub 
* sup 
* svg 
* textarea 
* time 
* var 
* video 
* wbr 
* Text



Embedded content
----------------

Na categoria Embedded, há elementos que importam outra fonte de informação para o documento.

* audio 
* canvas 
* embed 
* iframe 
* img 
* math 
* object 
* svg 
* video



Interactive content
-------------------

Interactive Content são elementos que fazem parte da interação de usuário.

* a 
* audio (se o atributo control for utilizado)
* button
* details
* embed
* iframe
* img (se o atributo usemap for utilizado)
* input (se o atributo type não tiver o valor hidden)
* keygen
* label
* menu (se o atributo type tiver o valor toolbar)
* object (se o atributo usemap for utilizado)
* select
* textarea
* video (se o atributo control for utilizado)



Alguns elementos no HTML podem ser ativados por um comportamento. Isso significa que o usuário pode ativá-lo de alguma
forma. O início da sequencia de eventos depende do mecanismo de ativação e normalmente culminam em um evento de click
seguido pelo evento DOMActivate.

O user-agent permite que o usuário ative manualmente o elemento que tem este comportamento utilizando um teclado, mouse,
comando de voz etc.


Novos elementos e atributos
===========================

A função do HTML é indicar que tipo de informação a página está exibindo. Quando lemos um livro, conseguimos entender e
diferenciar um título de um parágrafo. Basta percebermos a quantidade de letra, tamanho da fonte, cor etc. No código
isso é diferente. Robôs de busca e outros user-agents não conseguem diferenciar tais detalhes. Por isso, cabe ao
desenvolvedor marcar a informação para que elas possam ser diferenciadas por diversos dispositivos.

Com as versões anteriores do HTML nós conseguimos marcar diversos elementos do layout, estruturando a página de forma
que as informações ficassem em suas áreas específicas. Conseguiámos diferenciar por exemplo, um parágrafo de um título.
Mas não conseguíamos diferenciar o rodapé do cabeçalho. Essa diferenciação era apenas percebida visualmente pelo layout
pronto ou pela posição dos elementos na estrutura do HTML. Entretanto, não havia maneira de detectar automaticamente
estes elementos já que as tags utilizada para ambos poderiam ser iguais e não havia padrão para nomenclatura de IDs e
Classes.

O HTML5 trouxe uma série de elementos que nos ajudam a definir setores principais no documento HTML. Com a ajuda
destes elementos, podemos por exemplo diferenciar diretamente pelo código HTML5 áreas importantes do site como sidebar,
rodapé e cabeçalho. Conseguimos seccionar a área de conteúdo indicando onde exatamente é o texto do artigo.

Estas mudanças simplificam o trabalho de sistemas como os dos buscadores. Com o HTML5 os buscadores conseguem vasculhar
o código de maneira mais eficaz. Procurando e guardando informações mais exatas e levando menos tempo para estocar essa
informação.

Lista dos novos elementos e atributos incluídos no HTML5
--------------------------------------------------------

:section:   A tag ``section`` define uma nova seção genérica no documento. Por exemplo, a home de um website pode ser
            dividida em diversas seções: introdução ou destaque, novidades, informação de contato e chamadas para
            conteúdo interno.

:nav:       O elemento ``nav`` representa uma seção da página que contém links para outras partes do website. Nem todos
            os grupos de links devem ser elementos nav, apenas aqueles grupos que contém links importantes. Isso pode ser
            aplicado naqueles blocos de links que geralmente são colocados no Rodapé e também para compor o menu
            principal do site.

:article:   O elemento ``article`` representa uma parte da página que poderá ser distribuído e reutilizável em FEEDs por
            exemplo. Isto pode ser um post, artigo, um bloco de comentários de usuários ou apenas um bloco de texto
            comum.

:aside:     O elemento ``aside`` representa um bloco de conteúdo que referência o conteúdo que envolta do elemento aside.
            O aside pode ser representado por conteúdos em sidebars em textos impressos, publicidade ou até mesmo para
            criar um grupo de elementos nav e outras informações separados do conteúdo principal do website.

:hgroup:    Este elemento consiste em um grupo de títulos. Ele serve para agrupar elementos de título de H1 até H6
            quando eles tem múltiplos níveis como título com subtítulos e etc.

:header:    O elemento ``header`` representa um grupo de introdução ou elementos de navegação. O elemento ``header`` pode
            ser utilizado para agrupar índices de conteúdos, campos de busca ou até mesmo logos.

:footer:    O elemento ``footer`` representa literalmente o rodapé da página. Seria o último elemento do último elemento
            antes de fechar a tag HTML. O elemento ``footer`` não precisa aparecer necessariamente no final de uma seção.

:time:      Este elemento serve para marcar parte do texto que exibe um horário ou uma data precisa no calendário
            gregoriano.



Atributos que foram descontinuados porque modificam a formatação do elemento e suas funções são melhores controladas pelo CSS
-----------------------------------------------------------------------------------------------------------------------------

* align como atributo da tag caption, iframe, img, input, object, legend, table, hr, div, h1, h2, h3, h4, h5, h6, p, col, colgroup, tbody, td, tfoot, th, thead e tr.
* alink, link, text e vlink como atributos da tag body. 
* background como atributo da tag body. 
* bgcolor como atributo da tag table, tr, td, th e body. 
* border como atributo da tag table e object. 
* cellpadding e cellspacing como atributos da tag table. 
* char e charoff como atributos da tag col, colgroup, tbody, td, tfoot, th, thead e tr. 
* clear como atributo da tag br. 
* compact como atributo da tag dl, menu, ol e ul. 
* frame como atributo da tag table. 
* frameborder como atributo da tag iframe. 
* height como atributo da tag td e th. 
* hspace e vspace como atributos da tag img e object. 
* marginheight e marginwidth como atributos da tag iframe. 
* noshade como atributo da tag hr. 
* nowrap como atributo da tag td e th. 
* rules como atributo da tag table. 
* scrolling como atributo da tag iframe. 
* size como atributo da tag hr. 
* type como atributo da tag li, ol e ul. 
* valign como atributo da tag col, colgroup, tbody, td, tfoot, th, thead e tr. 
* width como atributo da tag hr, table, td, th, col, colgroup e pre.



Alguns atributos do HTML4 que não são mais permitidos no HTML5. Se eles tiverem algum impacto negativo na compatibilidade de algum user-agent eles serão discutidos
-------------------------------------------------------------------------------------------------------------------------------------------------------------------

* rev e charset como atributos da tag link e a.
* shape e coords como atributos da tag a.
* longdesc como atributo da tag img and iframe.
* target como atributo da tag link.
* nohref como atributo da tag area.
* profile como atributo da tag head.
* version como atributo da tag html.
* name como atributo da tag img (use id instead).
* scheme como atributo da tag meta.
* archive, classid, codebase, codetype, declare e standby como atributos da tag object.
* valuetype e type como atributos da tag param.
* axis e abbr como atributos da tag td e th.
* scope como atributo da tag td.



Atributos
---------

Alguns elementos ganharam novos atributos:

* O atributo autofocus pode ser especificado nos elementos input (exceto quando há atributo hidden atribuído), textarea, select e button.
* A tag a passa a suportar o atributo media como a tag link. 
* A tag form ganha um atributo chamado novalidate. Quando aplicado o formulário pode ser enviado sem validação de dados. 
* O elemento ol ganhou um atributo chamado reversed. Quando ele é aplicado os indicadores da lista são colocados na ordem inversa, isto é, da forma descendente. 
* O elemento fieldset agora permite o atributo disabled. Quando aplicado, todos os filhos de fieldset são desativados. 
* O novo atributo placeholder pode ser colocado em inputs e textareas. 
* O elemento area agora suporta os atributos hreflang e rel como os elementos a e link 
* O elemento base agora suporta o atributo target assim como o elemento a. O atributo target também não está mais descontinuado nos elementos a e area porque são úteis para aplicações web.



Atributos que foram descontinuados
----------------------------------

* O atributo border utilizado na tag img. 
* O atributo language na tag script. 
* O atributo name na tag a. Porque os desenvolvedores utilizam ID em vez de name. 
* O atributo summary na tag table.

O W3C mantém um documento atualizado constantemente nesta página: http://www.w3.org/TR/2010/WD-html5-diff-20100624/



Novos Tipos de campos
=====================

Novos valores para o atributo type
----------------------------------

O elemento input aceita os seguintes novos valores para o atributo type:

.. note::

    Até o momento o Opera 10 é o único navegador Desktop que fez um bom trabalho implementando os novos recursos de
    formulário do HTML5. Se você instalá-lo, poderá testar quase tudo deste e dos próximos dois capítulos.



tel
++++

Telefone. Não há máscara de formatação ou validação, propositalmente, visto não haver no mundo um padrão bem definido
para números de telefones. É claro que você pode usar a nova API de validação de formulários (descrita no capítulo 8)
para isso. Os agentes de usuário podem permitir a integração com sua agenda de contatos, o que é particularmente útil
em telefones celulares.



search
++++++

Um campo de busca. A aparência e comportamento do campo pode mudar ligeiramente dependendo do agente de usuário, para
parecer com os demais campos de busca do sistema.



email
+++++

E-mail, com formatação e validação. O agente de usuário pode inclusive promover a integração com sua agenda de contatos.



url
++++

Um endereço web, também com formatação e validação.



Datas e horas
+++++++++++++

O campo de formulário pode conter qualquer um desses valores no atributo type:

.. note::

    O tipo de campo datetime-local trata automaticamente as diferenças de fusos horários, submetendo ao servidor e
    recebendo dele valores GMT. Com isso você pode, com facilidade, construir um sistema que será usado em diferentes
    fusos horários e permitir que cada usuário lide com os valores em seu próprio fuso horário.



* datetime
* date
* month
* week
* time
* datetime-local

Todos devem ser validados e formatados pelo agente de usuário, que pode inclusive mostrar um calendário, um seletor de
horário ou outro auxílio ao preenchimento que estiver disponível no sistema do usuário.

O atributo adicional step define, para os validadores e auxílios ao preenchimento, a diferença mínima entre dois horários.
O valor de step é em segundos, e o valor padrão é 60. Assim, se você usar step="300" o usuário poderá fornecer como
horários 7:00, 7:05 e 7:10, mas não 7:02 ou 7:08.



number
++++++

Veja um exemplo do tipo number com seus atributos opcionais:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>Number type</title>
    </head>

    <body>
        <input name="valuex" type="number" value="12.4" step="0.2" min="0" max="20" />
    </body>
    </html>



.. figure:: ../_images/input-number.png



range
+++++

Vamos modificar, no exemplo acima, apenas o valor de type, mudando de “number" para “range":

.. code-block:: html

    <!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>Range type</title>
    </head>

    <body>
        <input name="valuex" type="range" value="12.4" step="0.2" min="0" max="20" />
    </body>
    </html>



.. figure:: ../_images/input-range.png



color
+++++

O campo com type="color" é um seletor de cor. O agente de usuário pode mostrar um controle de seleção de cor ou outro
auxílio que estiver disponível. O valor será uma cor no formato #ff6600.



Tipos de dados e validadores
============================

Formulários vitaminados
-----------------------

Conforme você deve ter percebido no último capítulo, o HTML5 avançou bastante nos recursos de formulários, facilitando
muito a vida de quem precisa desenvolver aplicações web baseadas em formulários. Neste capítulo vamos avançar um pouco
mais nesse assunto e, você vai ver, a coisa vai ficar ainda melhor.



autofocus
+++++++++

Ao incluir em um campo de formulário o atributo autofocus, assim:

.. code-block:: html

    <input name="login" autofocus >



O foco será colocado neste campo automaticamente ao carregar a página. Diferente das soluções em Javascript, o foco
estará no campo tão logo ele seja criado, e não apenas ao final do carregamento da página. Isso evita o problema, muito
comum quando você muda o foco com Javascript, de o usuário já estar em outro campo, digitando, quando o foco é mudado.



Placeholder text
++++++++++++++++

Você já deve ter visto um “placeholder". Tradicionalmente, vínhamos fazendo isso:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>Placeholder, the old style</title>
    </head>

    <body>
        <input name="q" value="Search here" onfocus="if(this.value==’Search here’)this.value=’’">
    </body>
    </html>



HTML5 nos permite fazer isso de maneira muito mais elegante:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <title>Placeholder, HTML5 way</title>
    </head>

    <body>
        <input name="q" placeholder="Search here">
    </body>
    </html>



required
++++++++

Para tornar um campo de formulário obrigatório (seu valor precisa ser preenchido) basta, em HTML5, incluir o atributo
required:

.. code-block:: html

    <input name="login" required>



maxlength
+++++++++

Você já conhecia o atributo maxlength, que limita a quantidade de caracteres em um campo de formulário. Uma grande
lacuna dos formulário HTML foi corrigida. Em HTML5, o elemento textarea também pode ter maxlength!



Validação de formulários
------------------------

Uma das tarefas mais enfadonhas de se fazer em Javascript é validar formulários. Infelizmente, é também uma das mais
comuns. HTML5 facilita muito nossa vida ao validar formulários, tornando automática boa parte do processo. Em muitos
casos, todo ele. Você já viu que pode tornar seus campos “espertos" com os novos valores para o atributo type, que já
incluem validação para datas, emails, URLs e números. Vamos um pouco além.



pattern
+++++++

O atributo pattern nos permite definir expressões regulares de validação, sem Javascript. Veja um exemplo de como
validar CEP:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="pt-BR">
    <head>
        <meta charset="UTF-8" />
        <title>O atributo pattern</title>
    </head>

    <body>
        <form>
            <label for="CEP">CEP:
                <input name="CEP" id="CEP" required pattern="\d{5}-?\d{3}" />
            </label>
            <input type="submit" value="Enviar" />
        </form>
    </body>
    </html>



novalidate e formnovalidate
+++++++++++++++++++++++++++

Podem haver situações em que você precisa que um formulário não seja validado. Nestes casos, basta incluir no elemento
form o atributo novalidate.

Outra situação comum é querer que o formulário não seja validado dependendo da ação de submit. Nesse caso, você pode
usar no botão de submit o atributo formnovalidate. Veja um exemplo:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="pt-BR">
    <head>
        <meta charset="UTF-8" />
        <title>Salvando rascunho</title>
        <style>label{display:block}</style>
    </head>

    <body>
        <form>
            <label>nome: <input name="nome" required></label>
            <label>email: <input name="email" type="email" required></label>
            <label>mensagem: <textarea name="mensagem" required></textarea></label>
            <input type="submit" name="action" value="Salvar rascunho" formnovalidate>
            <input type="submit" name="action" value="Enviar">
        </form>
    </body>
    </html>



Custom validators
+++++++++++++++++

É claro que as validações padrão, embora atendam a maioria dos casos, não são suficientes para todas as situações.
Muitas vezes você vai querer escrever sua própria função de validação Javascript. Há alguns detalhes na especificação do
HTML5 que vão ajudá-lo com isso:

1.  O novo evento oninput é disparado quando algo é modificado no valor de um campo de formulário. Diferente de onchange,
    que é disparado ao final da edição, oninput é disparado ao editar. É diferente também de onkeyup e onkeypress,
    porque vai capturar qualquer modificação no valor do campo, feita com mouse, teclado ou outra interface qualquer.

2.  O método setCustomValidity pode ser invocado por você. Ele recebe uma string. Se a string for vazia, o campo será
    marcado como válido. Caso contrário, será marcado como inválido.

Com isso, você pode inserir suas validações no campo de formulário e deixar o navegador fazer o resto. Não é mais
preciso capturar o evento submit e tratá-lo. Veja, por exemplo, este formulário com validação de CPF:

.. code-block:: html

    <!DOCTYPE html>
    <html lang="pt-BR">
    <head>
        <meta charset="UTF-8" />
        <title>Custom validator</title>
        <!-- O arquivo cpf.js contém a função validaCPF, que recebe uma string e retorna true ou false. -->
        <script src="cpf.js"></script>
        <script>
            function vCPF(i){
                i.setCustomValidity(validaCPF(i.value)?'':'CPF inválido!')
            }
        </script>
    </head>

    <body>
        <form>
            <label>CPF: <input name="cpf" oninput="vCPF(this)" /></label>
            <input type="submit" value="Enviar" />
        </form>
    </body>
    </html>


