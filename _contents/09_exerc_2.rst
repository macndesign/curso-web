***********************
2º Exercício de fixação
***********************

Projeto
=======

Um grupo de estudantes cearenses da linguagem de programação Python, vendo o crescimento acelerado da linguagem no
mercado, tiveram a idéia de fazer um encotro regional de desenvolvedores Python a "Python Nordeste 2013", que terá sede
em Fortaleza - CE. Como são apenas programadores, eles não tem muita noção do processo criativo de um layout para a
página do evento, então resolveram lhe contratar para desenvolver um site com alguns requisitos básicos do evento.



Requisitos
==========

A página deve ser toda, apenas em uma única página e deve conter:

    * Logo da Python Nordeste 2013
    * Descrição com data e cidade sede do evento
    * Menu principal com os itens: Evento, Palestras, Inscrições e Contato (Com link para sua respectiva seção)
    * Local para o texto sobre o evento com link para o menu
    * Local para a listagem de palestras com link para o menu
    * Local para texto informando como o usuário deverá proceder para se inscrever no evento com link para o menu
    * Local para inserir o formulário de contato com link para o menu
    * Endereço no rodapé do site
    * Tema Nordestino com estilo baseado em xilogravura



Antes de tudo
=============

Antes de começar a desenvolver o projeto, crie a hierarquia de pastas onde os arquivos deverão estar.

    * Crie uma pasta chamada pyne2013/
    * Dentro de pyne2013/ crie uma pasta chamada public/, essa pasta terá os arquivos estáticos do projeto
    * Dentro da pasta public/, crie as pastas: css/, fonts/, img/ e js/, respectivamente essas pasta terão os arquivos de
      folha de estilo (css), fontes utilizadas no projeto (fonts), imagens do site (img) e arquivos de javascript (js)



Estrutura básica
================

Comno no exercício 1, vamos criar a estrutura básica de um documento html5, salvando como index.html dentro da pasta pyne2013

.. code-block:: html

    <!DOCTYPE html>
    <html>
    <head>
        <title>Python Nordeste 2013</title>
    </head>
    <body>

    </body>
    </html>



Metas
=====

Insira as tags ``<meta>`` necessárias antes da tag ``<tittle>``

.. code-block:: html

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta http-equiv="X-Frame-Options" content="deny">
    <meta name="author" content="Mário <macndesign@gmail.com>">
    <meta name="description" content="Primeira conferência Python que reune os PUGs (Python User Groups) da região Nordeste do Brasil">
    <meta name="keywords" content="palestras,python,nordeste">
    <meta name="viewport" content="width=device-width">

.. note::

    A tag ``<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">`` força o navegador a renderizar com webkit.
    A tag ``<meta http-equiv="X-Frame-Options" content="deny">`` faz com quê outras pessoa não utilizem o conteúdo do
    site como ``<iframe>`` de outra página.
    A tag ``<meta name="viewport" content="width=device-width">`` define a largura da tela para a largura do dispositivo
    quando se usa media queries da forma correta.



Recursos externos para preparação do desenvolvimento do site
============================================================

Para que nosso site seja melhor renderizado nos mais diversos navegadores, temos que pegar alguns recursos desenvolvido
por terceiros, como:

    * Pege os arquivos ``normalize.css`` e ``main.css`` e insira dentro da pasta css/ que está dentro de public/. Esses
      arquivos respectivamente servirão para normalizar as tags html para uma melhor renderização das mesmas e classes de ajuda
      para tarefas rotineiras
    * Agora insira na pasta fonts/ que fica dentro de public/ as fontes true-tape que usaremos em nosso projeto
    * Coloque as imagens de nosso site na pasta img/ que fica dentro de public/
    * E insira os scripts flexie.min.js (melhora o funcionamento de box flexíveis nos mais diversos navegadores), html5.js
      (esse é o mesmo html5shiv que ajuda na renderização de novas tags html, inclusive a recente tag ``<main>`` e o jQuery
      para possibilitar o funcionamento do script flexie



Importando os recursos em nossa página
======================================

Abaixo da tag ``<title>`` insira os arquivos abaixo.

.. code-block:: html

    <link rel="shortcut icon" href="public/img/favicon.ico" />
    <link rel="stylesheet" href="public/css/normalize.css">
    <link rel="stylesheet" href="public/css/main.css">
    <link rel="stylesheet" href="public/css/style.css">
    <!--[if IE]><script src="public/js/html5.js"></script><![endif]-->
    <script type="text/javascript" src="public/js/jquery.min.js"></script>
    <script type="text/javascript" src="public/js/flexie.min.js"></script>

.. note::

    Ainda não falamos desse primeiro import, que é o favicon, esse arquivo é responsável por inserir a logo do site
    na barra de título do navegador ou na aba que o site estiver sendo executado.


Teste em navegador menor que IE 7 e não tem o google chrome frame instalado
===========================================================================

Inserir logo após a abertura da tag ``<body>``

.. code-block:: html

    <!--[if lt IE 7]>
        <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->



Cabeçalho principal
===================

Inserir logo após o teste de browser

.. code-block:: html

    <header role="banner" class="clearfix">
        <!-- Define uma região que possui, principalmente, conteúdo de orientação do site e não conteúdo específico da página. -->
        <hgroup>
            <h1 class="ir">Python Nordeste 2013</h1>
            <h2><time datetime="2013-05-24T08:00Z">24</time> e <time datetime="2013-05-25T08:00Z">25 de Maio</time><span id="cidade-uf">Fortaleza - CE</span></h2>
        </hgroup>
    </header>



Menu principal do site
======================

Inserir logo após o fechamento da tag ``<header>``

.. code-block:: html

    <nav role="navigation" id="menu" class="clearfix">
        <!-- Define uma coleção de elementos de navegação (geralmente links) para navegar no documento ou em documentos relacionados. -->
        <a href="#evento">Evento</a>
        <a href="#palestras">Palestras</a>
        <a href="#inscricoes">Inscrições</a>
        <a href="#contato">Contato</a>
    </nav>



Tag do conteúdo principal
=========================

Usando a recente tag ``<main>`` introduzida a pouco tempo no html5, insira logo após o fechamento da tag ``<nav>``

.. code-block:: html

    <main class="clearfix">
        <section role="main">
        <!-- Define o conteúdo principal do documento. -->
        </section>
    </main>



Um artigo para cara item do menu
================================

Inserir dentro da tag ``<section role="main">``, que é a seção do conteúdo principal.

.. code-block:: html

    <article role="article" id="evento"></article>
    <article role="article" id="palestras"></article>
    <article role="article" id="inscricoes"></article>
    <article role="article" id="contato"></article>

.. note::

    A role article define uma seção de uma página que consiste em uma composição que forma uma parte independente do documento.



Conteúdo da seção sobre o evento
================================

Insira o título do evento dentro da tag ``<article role="article" id="evento">``

.. code-block:: html

    <h3>O Evento <a href="#menu" class="go-top">↑ menu</a></h3>

E o conteúdo do texto sobre o evento logo após o título da seção evento

.. code-block:: html

    <div class="article-content rotacionar-direita">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac orci eu erat pharetra suscipit.</p>
        <p>Curabitur laoreet, odio et eleifend euismod, metus nulla mollis nulla, elementum ullamcorper dolor lorem vitae ipsum.</p>
    </div>



Conteúdo da seção palestras
===========================

Insira o título do evento dentro da tag ``<article role="article" id="palestras">``

.. code-block:: html

    <h3>O Evento <a href="#menu" class="go-top">↑ menu</a></h3>

E a listagem de palestras logo após o título da seção palestras

.. code-block:: html

    <div class="article-content">
        <section class="dias">
            <article class="dia">
                <h4>Dia 24</h4>
                <dl>
                    <dt>Abertura <time datetime="2013-05-24T08:00Z">8:00h</time></dt>
                    <dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac orci eu erat pharetra suscipit.</dd>
                    <dt>Palestra Django - Henrique Bastos <time datetime="2013-05-24T09:00Z">9:00h</time></dt>
                    <dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac orci eu erat pharetra suscipit.</dd>
                </dl>
            </article>
            <article class="dia">
                <h4>Dia 25</h4>
                <dl>
                    <dt>Abertura <time datetime="2013-05-25T08:00Z">8:00h</time></dt>
                    <dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac orci eu erat pharetra suscipit.</dd>
                    <dt>Palestra Python - Luciano Ramalho <time datetime="2013-05-25T09:00Z">9:00h</time></dt>
                    <dd>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras ac orci eu erat pharetra suscipit.</dd>
                </dl>
            </article>
        </section>
    </div>

.. note::

    Veja o uso das tags ``<dl>`` definition list, ``<dt>`` definition title e ``<dd>`` definition data.



Conteúdo da seção do texto sobre as inscrições
==============================================

Insira o título do evento dentro da tag ``<article role="article" id="inscricoes">``

.. code-block:: html

    <h3>Inscrições <a href="#menu" class="go-top">↑ menu</a></h3>

E o conteúdo do texto informando como o usuário deverá proceder para se inscrever no evento logo após o título da seção inscrições

.. code-block:: html

    <div class="article-content rotacionar-direita">
        <p>Texto informando como o usuário deverá proceder para se inscrever no evento.</p>
    </div>



Conteúdo e formulário da seção de contato
=========================================

Insira o título do evento dentro da tag ``<article role="article" id="contato">``

.. code-block:: html

    <h3>Inscrições <a href="#menu" class="go-top">↑ menu</a></h3>

Insira o conteúdo do formulário logo após o título da seção de contato

.. code-block:: html

    <div class="article-content rotacionar-esquerda">
        <form action="." method="post">
            <label><span class="rotulo">Nome: </span><input type="text" placeholder="Nome" maxlength="75" required></label>
            <label><span class="rotulo">Email: </span><input type="email" placeholder="Email" maxlength="120" required></label>
            <label><span class="rotulo">Mensagem: </span><textarea placeholder="Mensagem" maxlength="255" rows="5" required></textarea></label>
            <div class="form-actions"><input type="submit" value="Enviar"></div>
        </form>
    </div>



Rodapé da página com localização da Python Nordeste 2013
========================================================

Insira logo após a tag ``<main>``

.. code-block:: html

    <footer role="contentinfo" class="clearfix">
    <!-- footer: role=”contentinfo”. Define uma região que contém informações sobre o documento. -->
        <address>Fortaleza - CE</address>
    </footer>



Aplicando a folha de estilo no site
===================================

Agora vamos dar um "tapa" visual dessa página que está toda em preto e branco com CSS.


Definindo as fontes do site
===========================

.. code-block:: css

    @font-face {
        font-family: "Xilosa";
        src: url(../fonts/xilosa.ttf);
    }
    @font-face {
        font-family: "Eldes Cordel 1";
        src: url(../fonts/eldes_cordel_1_lite.ttf);
    }


Aplicando estilos globais
=========================

.. code-block:: css

    html {
        height: 100%;
        width: 100%;
    }
    body {
        font-family: "Georgia", Arial, sans-serif;
        height: 100%;
        width: 100%;
        background: #ffff99; /* Old browsers */
        background: -moz-radial-gradient(center, ellipse cover,  #ffff99 0%, #f7ce00 100%); /* FF3.6+ */
        background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#ffff99), color-stop(100%,#f7ce00)); /* Chrome,Safari4+ */
        background: -webkit-radial-gradient(center, ellipse cover,  #ffff99 0%,#f7ce00 100%); /* Chrome10+,Safari5.1+ */
        background: -o-radial-gradient(center, ellipse cover,  #ffff99 0%,#f7ce00 100%); /* Opera 12+ */
        background: -ms-radial-gradient(center, ellipse cover,  #ffff99 0%,#f7ce00 100%); /* IE10+ */
        background: radial-gradient(ellipse at center,  #ffff99 0%,#f7ce00 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffff99', endColorstr='#f7ce00',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
        background-attachment: fixed;
    }



Definindo a largura dos elementos
=================================

.. code-block:: css

    header[role=banner], nav#menu, section[role=main], footer[role=contentinfo] {
        display: block;
        width: 992px;
        margin: 0 auto;
    }

    section[role=main] {
        width: 896px;
        padding: 0 48px;
        position: relative;
        top: -50px;
    }



Definindo estilos para o cabeçalho principal do site
====================================================

.. code-block:: css

    header[role=banner] {
        height: 187px;
        margin-top: 30px;
    }
    header[role=banner] hgroup h1 {
        float: left;
        width: 476px;
        height: 187px;
        background-image: url("../img/logo.png");
        margin: 0;
    }
    header[role=banner] hgroup h2 {
        float: right;
        font-family: "Eldes Cordel 1", Arial, sans-serif;
        margin-top: 45px;
        font-size: 30px;
        margin-right: 50px;
        text-align: right;
    }
    header[role=banner] hgroup h2 span#cidade-uf {
        display: block;
        font-size: 20px;
        position: relative;
    }



Formatando o menu principal
===========================

.. code-block:: css

    nav#menu {
        background: url("../img/menu.png") no-repeat top center;
        text-align: center;
        height: 66px;
        padding-top: 15px;
        position: relative;
        top: -20px;
        clear: both;
    }
    nav#menu a {
        font-family: "Xilosa", Arial, sans-serif;
        color: #fff;
        text-decoration: none;
        font-size: 28px;
        padding-right: 15px;
    }



Definindo estilos para os elementos que fazem parte de cada tag article
=======================================================================

.. code-block:: css

    article[role=article] h3 {
        font-family: "Xilosa", Arial, sans-serif;
        font-size: 80px;
        margin: 0;
        padding-top: 15px;
    }
    article[role=article] div.article-content {
        padding: 1px 20px;
        border: solid 12px #000;
        background: #ffffff; /* Old browsers */
        background: -moz-radial-gradient(center, ellipse cover,  #ffffff 0%, #e8e6da 100%); /* FF3.6+ */
        background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,#ffffff), color-stop(100%,#e8e6da)); /* Chrome,Safari4+ */
        background: -webkit-radial-gradient(center, ellipse cover,  #ffffff 0%,#e8e6da 100%); /* Chrome10+,Safari5.1+ */
        background: -o-radial-gradient(center, ellipse cover,  #ffffff 0%,#e8e6da 100%); /* Opera 12+ */
        background: -ms-radial-gradient(center, ellipse cover,  #ffffff 0%,#e8e6da 100%); /* IE10+ */
        background: radial-gradient(ellipse at center,  #ffffff 0%,#e8e6da 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#e8e6da',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
    }
    article[role=article] h3 a.go-top:first-letter {
        font-size: 20px;
    }
    article[role=article] h3 a.go-top {
        font-size: 15px;
        float: right;
        text-decoration: none;
        color: #b79900;
        margin-top: 30px;
    }



Definindo estilos específicos para a grid de programação da PyNE 2013
=====================================================================

.. code-block:: css

    section.dias {
        display: -webkit-flex;
        display: -ms-flexbox;
        display: flex;
    }
    section.dias article.dia {
        -webkit-flex: 1;
        -ms-flexbox: 1;
        flex: 1;
        padding: 0;
    }
    section.dias article.dia h4 {
        background: #000;
        font-family: "Xilosa", Arial, sans-serif;
        font-size: 25px;
        color: #fff;
        padding: 5px 15px;

        /* chrome hack */
        display: block;
        -webkit-margin-before: 20px;
        -webkit-margin-after: 0;
        font-weight: bold;
    }
    section.dias article.dia dl {
        /* chrome hack */
        display: block;
        -webkit-margin-before: 20px;
        -webkit-margin-after: 0;
    }
    section.dias article.dia:first-child dl {
        border-right: dotted 1px #b79900;
    }
    section.dias article.dia dl dt {
        font-family: "Xilosa", Arial, sans-serif;
        font-size: 20px;
    }
    section.dias article.dia dl dd {
        font-size: 0.8em;
        margin: 10px 0 13px 40px 13px;
    }
    section.dias article.dia dl dt {
        padding: 5px 13px;
    }
    section.dias article.dia dl dt time {
        float: right;
        color: #b79900;
    }



Definindo estilos específicos para o formulário de contato
==========================================================

.. code-block:: css

    article#contato > div {
        padding: 10px;
    }
    article#contato div form label {
        display: block;
        padding-bottom: 10px;
    }
    article#contato div form label span.rotulo {
        display: none;
    }
    article#contato div form label input[type=text],
    article#contato div form label input[type=email],
    article#contato div form label textarea,
    article#contato div form div input[type=submit] {
        font-family: "Xilosa", Arial, sans-serif;
        font-size: 16px;
        border: solid 4px #000;
        padding: 3px;
        width: 400px;
    }
    article#contato div form div input[type=submit] {
        font-size: 22px;
        background: #000;
        color: #fff;
        width: 413px;
    }

Definindo estilos específicos para o rodapé do site
===================================================

.. code-block:: css

    footer {
        background: url("../img/footer.png") no-repeat top center;
        height: 216px;
    }
    footer address {
        font-family: "Eldes Cordel 1", Arial, sans-serif;
        font-size: 20px;
        margin-top: 180px;
        text-align: center;
        color: #fff;
    }



Aplicando efeito de inclinação suave para a direita e para a esquerda
=====================================================================

.. code-block:: css

    .rotacionar-direita {
        transform:rotate(2deg);
        -ms-transform:rotate(2deg);
        -moz-transform:rotate(2deg);
        -webkit-transform:rotate(2deg);
        -o-transform:rotate(2deg);
    }
    .rotacionar-esquerda {
        transform:rotate(-2deg);
        -ms-transform:rotate(-2deg);
        -moz-transform:rotate(-2deg);
        -webkit-transform:rotate(-2deg);
        -o-transform:rotate(-2deg);
    }
